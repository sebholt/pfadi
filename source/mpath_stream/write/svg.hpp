/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <sev/lag/vector3.hpp>
#include <snc/mpath/curve_evaluator_buffer_d.hpp>
#include <snc/mpath_stream/statistics_d.hpp>
#include <snc/mpath_stream/write.hpp>
#include <array>
#include <fstream>
#include <string>

namespace mpath_stream::write
{

class SVG : public snc::mpath_stream::Write
{
  // Public types
  public:
  // Public methods
  public:
  SVG ();

  ~SVG ();

  const std::string &
  file_name () const
  {
    return _file_name;
  }

  void
  set_file_name ( std::string file_name_n );

  void
  set_stats ( const snc::mpath_stream::Statistics_D3 & stats_n );

  // --- Abstract interface

  bool
  open () override;

  bool
  is_open () override;

  bool
  write ( const snc::mpath::Element & elem_n ) override;

  void
  close () override;

  private:
  // -- Utility

  void
  svg_begin ();

  void
  svg_end ();

  void
  svg_path_begin ();

  void
  svg_path_end ();

  void
  handle_element ( const snc::mpath::Element & elem_n );

  void
  handle_elem_curve ( const snc::mpath::Element & curve_n );

  void
  handle_curve_linear ();

  void
  handle_curve_cubic ();

  void
  handle_curve_circle ();

  void
  handle_curve_helix ();

  void
  point_adjust ( sev::lag::Vector3d & vpos_n );

  private:
  // -- Attributes
  std::ofstream _ofstream;

  sev::lag::Vector3d _vpos_cur;
  double _len_scale = 1.0;
  bool _is_open = false;
  bool _first_elem = false;
  bool _first_curve_elem = false;
  bool _curve_started = false;
  snc::mpath_stream::Statistics_D3 _mpath_stats;
  std::string _file_name;
  snc::mpath::Curve_Evaluator_Buffer_D3 _eval_buffer;
};

} // namespace mpath_stream::write
