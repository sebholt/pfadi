/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#include "svg.hpp"
#include <sev/assert.hpp>
#include <sev/math/numbers.hpp>
#include <snc/mpath/curve/evaluator_cubic_d.hpp>
#include <snc/mpath/curve/evaluator_linear_d.hpp>
#include <snc/mpath/d3/curve/evaluator_circle.hpp>
#include <snc/mpath/d3/curve/evaluator_helix.hpp>
#include <snc/mpath/d3/types.hpp>
#include <snc/mpath/elem/curve.hpp>
#include <snc/mpath/element.hpp>

namespace mpath_stream::write
{

SVG::SVG () {}

SVG::~SVG ()
{
  DEBUG_ASSERT ( !_is_open );
}

void
SVG::set_file_name ( std::string file_name_n )
{
  _file_name = std::move ( file_name_n );
}

void
SVG::set_stats ( const snc::mpath_stream::Statistics_D3 & stats_n )
{
  _mpath_stats = stats_n;
}

bool
SVG::open ()
{
  if ( !_is_open ) {
    _is_open = true;
    _first_elem = true;
    _first_curve_elem = true;
    _curve_started = false;

    _vpos_cur.fill ( 0.0 );
  }
  return _is_open;
}

bool
SVG::is_open ()
{
  return _is_open;
}

void
SVG::close ()
{
  if ( _is_open ) {
    if ( _ofstream.is_open () ) {
      svg_end ();
    }
    _is_open = false;
  }
}

bool
SVG::write ( const snc::mpath::Element & elem_n )
{
  if ( !_is_open ) {
    return false;
  }

  if ( _first_elem ) {
    _first_elem = false;

    // Open file
    _ofstream.open ( _file_name, std::ios::binary );
    // Begin SVG
    if ( _ofstream.is_open () ) {
      svg_begin ();
    }
  }
  if ( _ofstream.is_open () ) {
    if ( snc::mpath::d3::elem::type_is_curve ( elem_n.elem_type () ) ) {
      // Curve element
      if ( !_curve_started ) {
        _curve_started = true;
        _first_curve_elem = true;
        svg_path_begin ();
      }
      {
        auto & ecurve =
            static_cast< const snc::mpath::elem::Curve_D< 3 > & > ( elem_n );
        _eval_buffer.new_from_curve ( _vpos_cur, ecurve );
        _eval_buffer.evaluator ().calc_end_pos ( _vpos_cur );
        handle_elem_curve ( ecurve );
      }
      _first_curve_elem = false;
    } else if ( elem_n.elem_type () ==
                (std::uint_fast32_t)snc::mpath::elem::Type::SPEED ) {
      // Ignore
    } else {
      if ( _curve_started ) {
        _curve_started = false;
        svg_path_end ();
      }
    }
    return true;
  }

  return false;
}

void
SVG::handle_elem_curve ( const snc::mpath::Element & curve_n )
{
  switch ( curve_n.elem_type () ) {
  case snc::mpath::d3::elem::Type::CURVE_LINEAR:
    handle_curve_linear ();
    break;
  case snc::mpath::d3::elem::Type::CURVE_CUBIC:
    handle_curve_cubic ();
    break;
  case snc::mpath::d3::elem::Type::CURVE_CIRCLE:
    handle_curve_circle ();
    break;
  case snc::mpath::d3::elem::Type::CURVE_HELIX:
    handle_curve_helix ();
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }
}

void
SVG::svg_begin ()
{
  std::size_t x_axis = 0;
  std::size_t y_axis = 1;

  double axis_overshoot = 10.0;

  auto & bbox = _mpath_stats.curve_bbox;
  double x_min = std::min ( 0.0, bbox[ 0 ][ x_axis ] ) - axis_overshoot;
  double x_max = std::max ( 0.0, bbox[ 1 ][ x_axis ] ) + axis_overshoot;
  double y_min = std::min ( 0.0, bbox[ 0 ][ y_axis ] ) - axis_overshoot;
  double y_max = std::max ( 0.0, bbox[ 1 ][ y_axis ] ) + axis_overshoot;

  double width = ( x_max - x_min );
  double height = ( y_max - y_min );

  _ofstream << "<svg \n";
  _ofstream << " width=\"" << width << "mm"
            << "\" \n";
  _ofstream << " height=\"" << height << "mm"
            << "\" \n";
  _ofstream << " viewBox=\"";
  _ofstream << x_min << " ";
  _ofstream << -y_max << " ";
  _ofstream << width << " ";
  _ofstream << height << "\" \n";
  _ofstream << " xmlns=\"http://www.w3.org/2000/svg\" \n";
  _ofstream << " version=\"1.1\" >\n";

  // -- X-Axis
  _ofstream << "<path";
  _ofstream << " d=\"";
  _ofstream << "M" << x_min << " " << 0.0;
  _ofstream << " L" << x_max << " " << 0.0;
  _ofstream << "\"\n";
  _ofstream << " fill=\"none\"";
  _ofstream << " stroke=\"#990000\"";
  _ofstream << " stroke-opacity=\"1\"";
  _ofstream << " stroke-width=\"0.25\"";
  _ofstream << " stroke-linecap=\"butt\"";
  _ofstream << " stroke-linejoin=\"round\"";
  _ofstream << "/>\n";

  // -- Y-Axis
  _ofstream << "<path";
  _ofstream << " d=\"";
  _ofstream << "M" << 0.0 << " " << -y_max;
  _ofstream << " L" << 0.0 << " " << -y_min;
  _ofstream << "\"\n";
  _ofstream << " fill=\"none\"";
  _ofstream << " stroke=\"#006600\"";
  _ofstream << " stroke-opacity=\"1\"";
  _ofstream << " stroke-width=\"0.25\"";
  _ofstream << " stroke-linecap=\"butt\"";
  _ofstream << " stroke-linejoin=\"round\"";
  _ofstream << "/>\n";
}

void
SVG::svg_end ()
{
  if ( _curve_started ) {
    _curve_started = false;
    svg_path_end ();
  }
  _ofstream << "</svg>\n";
}

void
SVG::svg_path_begin ()
{
  _ofstream << "<path";
  _ofstream << " d=\"";
}

void
SVG::svg_path_end ()
{
  _ofstream << "\"\n";
  _ofstream << " fill=\"none\"";
  _ofstream << " stroke=\"black\"";
  _ofstream << " stroke-width=\"0.25\"";
  _ofstream << " stroke-linejoin=\"round\"";
  _ofstream << "/>\n";
}

void
SVG::point_adjust ( sev::lag::Vector3d & vpos_n )
{
  vpos_n *= _len_scale;
  vpos_n[ 1 ] = -vpos_n[ 1 ];
}

void
SVG::handle_curve_linear ()
{
  sev::lag::Vector3d pos_end;
  _eval_buffer.evaluator ().calc_end_pos ( pos_end );
  point_adjust ( pos_end );

  if ( _first_curve_elem ) {
    // Move to
    _ofstream << "M";
  } else {
    // Line to
    _ofstream << " L";
  }
  _ofstream << pos_end[ 0 ] << " " << pos_end[ 1 ];
}

void
SVG::handle_curve_cubic ()
{
  auto & ceval = static_cast< snc::mpath::curve::Evaluator_Cubic_D< 3 > & > (
      _eval_buffer.evaluator () );

  sev::lag::Vector3d pt1 ( ceval.points ()[ 0 ] );
  sev::lag::Vector3d pt2 ( ceval.points ()[ 1 ] );
  sev::lag::Vector3d pt3 ( ceval.points ()[ 2 ] );
  point_adjust ( pt1 );
  point_adjust ( pt2 );
  point_adjust ( pt3 );

  _ofstream << " C" << pt1[ 0 ] << "," << pt1[ 1 ];
  _ofstream << " " << pt2[ 0 ] << "," << pt2[ 1 ];
  _ofstream << " " << pt3[ 0 ] << "," << pt3[ 1 ];
}

void
SVG::handle_curve_circle ()
{
  typedef snc::mpath::d3::curve::Evaluator_Circle EType;
  EType & ceval ( static_cast< EType & > ( _eval_buffer.evaluator () ) );

  const double srad = ( ceval.radius () * _len_scale );
  const double axis_rotation = 0.0;

  double angle_done = 0.0;
  while ( true ) {
    double angle_todo =
        ( std::fabs ( ceval.angle_radians () ) - std::fabs ( angle_done ) );
    if ( angle_todo < 1.0e-6 ) {
      break;
    }
    double angle = angle_todo;
    sev::math::assign_smaller< double > ( angle, sev::lag::pi_half_d );
    if ( ceval.angle_radians () < 0.0 ) {
      angle = -angle;
    }

    sev::lag::Vector3d pos_end;
    {
      sev::lag::Vector3d pos_begin;
      ceval.calc_pos ( pos_begin, angle_done / ceval.angle_radians () );

      snc::mpath::d3::elem::Curve_Circle tcirc;
      tcirc.set_pos_center ( ceval.pos_center () );
      tcirc.set_angle_radians ( angle );

      snc::mpath::d3::curve::Evaluator_Circle ecirc ( pos_begin, tcirc );
      ecirc.calc_end_pos ( pos_end );
    }
    point_adjust ( pos_end );

    _ofstream << " A" << srad << "," << srad;
    _ofstream << " " << axis_rotation;
    _ofstream << " 0,";
    if ( angle >= 0.0 ) {
      // Inverted due to y-axis inversion
      _ofstream << "0";
    } else {
      _ofstream << "1";
    }
    _ofstream << " " << pos_end[ 0 ] << "," << pos_end[ 1 ];

    angle_done += angle;
  }
}

void
SVG::handle_curve_helix ()
{
  auto & ceval = static_cast< snc::mpath::d3::curve::Evaluator_Helix & > (
      _eval_buffer.evaluator () );

  const double srad = ( ceval.radius () * _len_scale );
  const double axis_rotation = 0.0;

  double angle_done = 0.0;
  while ( true ) {
    double angle_todo =
        ( std::fabs ( ceval.angle_radians () ) - std::fabs ( angle_done ) );
    if ( angle_todo < 1.0e-6 ) {
      break;
    }
    double angle = angle_todo;
    sev::math::assign_smaller< double > ( angle, sev::lag::pi_half_d );
    if ( ceval.angle_radians () < 0.0 ) {
      angle = -angle;
    }

    sev::lag::Vector3d pos_end;
    {
      sev::lag::Vector3d pos_begin;
      ceval.calc_pos ( pos_begin, angle_done / ceval.angle_radians () );

      snc::mpath::d3::elem::Curve_Circle tcirc;
      tcirc.set_pos_center ( ceval.pos_center () );
      tcirc.set_angle_radians ( angle );

      snc::mpath::d3::curve::Evaluator_Circle ecirc ( pos_begin, tcirc );
      ecirc.calc_end_pos ( pos_end );
    }
    point_adjust ( pos_end );

    _ofstream << " A" << srad << "," << srad;
    _ofstream << " " << axis_rotation;
    _ofstream << " 0,";
    if ( angle >= 0.0 ) {
      // Inverted due to y-axis inversion
      _ofstream << "0";
    } else {
      _ofstream << "1";
    }
    _ofstream << " " << pos_end[ 0 ] << "," << pos_end[ 1 ];

    angle_done += angle;
  }
}

} // namespace mpath_stream::write
