/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <sev/lag/vector3.hpp>
#include <snc/mpath/d3/types.hpp>
#include <snc/mpath/speed_database.hpp>
#include <snc/mpath_stream/read.hpp>
#include <variant>

namespace mpath_stream::read
{

class Go_To : public snc::mpath_stream::Read
{
  public:
  // -- Types

  enum class Stage
  {
    NONE,
    SPEED,
    LEG_1,
    LEG_2
  };

  // Public methods
  public:
  Go_To ();

  void
  reset ();

  void
  setup ( const sev::lag::Vector3d & pos_begin_n,
          const sev::lag::Vector3d & pos_end_n );

  snc::mpath::Speed_Regime
  speed_regime () const
  {
    return _speed_regime;
  }

  void
  set_speed_regime ( snc::mpath::Speed_Regime id_n );

  const sev::lag::Vector3d &
  pos_begin () const
  {
    return _pos_begin;
  }

  const sev::lag::Vector3d &
  pos_end () const
  {
    return _pos_end;
  }

  // --- Abstract interface

  bool
  open () override;

  void
  close () override;

  const snc::mpath::Element *
  read () override;

  private:
  // -- Attributes
  Stage _stage = Stage::NONE;

  sev::lag::Vector3d _pos_begin;
  sev::lag::Vector3d _pos_end;
  sev::lag::Vector3d _pos_middle;
  snc::mpath::Speed_Regime _speed_regime = snc::mpath::Speed_Regime::RAPID;

  std::variant< snc::mpath::elem::Speed,
                snc::mpath::d3::elem::Curve_Linear,
                snc::mpath::elem::Stream_End >
      _element_variant;
};

} // namespace mpath_stream::read
