/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#include "speed_insert.hpp"
#include <sev/assert.hpp>
#include <sev/math/numbers.hpp>
#include <sev/utility.hpp>
#include <snc/device/statics/axis.hpp>
#include <snc/device/statics/statics.hpp>
#include <snc/mpath/curve/evaluator_cubic_d.hpp>
#include <snc/mpath/curve/evaluator_linear_d.hpp>
#include <snc/mpath/d3/curve/evaluator_circle.hpp>
#include <snc/mpath/d3/curve/evaluator_helix.hpp>
#include <snc/mpath/d3/types.hpp>
#include <algorithm>

namespace mpath_stream::read
{

Speed_Insert3::Speed_Insert3 ()
: _pos_current ( sev::lag::init::zero )
{
}

void
Speed_Insert3::setup ( snc::mpath_stream::Read * src_stream_n,
                       const sev::lag::Vector3d & pos_current_n,
                       const snc::device::statics::Statics & device_statics_n,
                       const snc::mpath::Speed_Database & speed_db_n )
{
  _source_stream = src_stream_n;
  _elem_speed = nullptr;
  _elem_source = nullptr;

  _pos_current = pos_current_n;
  _curve_speed_latest = 0.0;
  _curve_speed = 0.0;

  for ( std::size_t ii = 0; ii != 3; ++ii ) {
    const auto & saxis = *device_statics_n.axes ().get ( ii );
    _curve_speeds_min[ ii ] = saxis.geo ().speed_min ();
    _curve_speeds_max[ ii ] = saxis.geo ().speed_max ();
  }

  _curve_speed_min = *std::max_element ( _curve_speeds_min.begin (),
                                         _curve_speeds_min.end () );
  _curve_speed_planar_min =
      std::max ( _curve_speeds_min[ 0 ], _curve_speeds_min[ 1 ] );
  _curve_speed_normal_min = _curve_speeds_min[ 2 ];

  _curve_speed = _curve_speed_min;
  _curve_speed_planar = _curve_speed_planar_min;
  _curve_speed_normal = _curve_speed_normal_min;

  _speed_db = speed_db_n;
}

bool
Speed_Insert3::open ()
{
  if ( is_open () ) {
    return true;
  }
  if ( _source_stream == nullptr ) {
    return false;
  }

  _source_stream->open ();
  set_state ( _source_stream->state () );
  return is_open ();
}

void
Speed_Insert3::close ()
{
  if ( !is_open () ) {
    return;
  }

  _source_stream->close ();
  _source_stream = nullptr;
  _elem_speed = nullptr;
  _elem_source = nullptr;
  set_state_closed ();
}

const snc::mpath::Element *
Speed_Insert3::read ()
{
  DEBUG_ASSERT ( is_open () );

  if ( ( _elem_speed == nullptr ) && ( _elem_source == nullptr ) ) {
    feed_from_source ();
  }

  // Emit computed speed or source element
  if ( _elem_speed != nullptr ) {
    auto * res = _elem_speed;
    _elem_speed = nullptr;
    return res;
  }
  if ( _elem_source != nullptr ) {
    auto * res = _elem_source;
    _elem_source = nullptr;
    // After emitting the latest source stream element, update the stream state.
    set_state ( _source_stream->state () );
    return res;
  }

  // Finalize stream
  set_state_finished ();
  return &_elem_variant.emplace< snc::mpath::elem::Stream_End > ();
}

void
Speed_Insert3::feed_from_source ()
{
  while ( _source_stream->is_good () ) {
    auto * elem_src = _source_stream->read ();
    DEBUG_ASSERT ( elem_src != nullptr );

    if ( elem_src->elem_type () == snc::mpath::elem::Type::SPEED_DB_VALUE ) {
      set_speed_db_value ( *elem_src );
      // Skip this element
      continue;
    }

    if ( elem_src->elem_type () == snc::mpath::elem::Type::SPEED ) {
      set_speeds ( *elem_src );
      // Skip this element
      continue;
    }

    // Emit an additional speed element on demand
    if ( snc::mpath::d3::elem::type_is_curve ( elem_src->elem_type () ) ) {
      double speed = acquire_curve_speed ( *elem_src );
      if ( sev::change ( _curve_speed_latest, speed ) ) {
        auto & elem = _elem_variant.emplace< snc::mpath::elem::Speed > ();
        elem.speed ().set_value ( speed );
        _elem_speed = &elem;
      }
    }

    // Emit the source element
    _elem_source = elem_src;
    return;
  }
}

double
Speed_Insert3::acquire_curve_speed ( const snc::mpath::Element & elem_n )
{
  const sev::lag::Vector3d pos_begin = _pos_current;
  _eval_buffer.new_from_curve (
      pos_begin,
      static_cast< const snc::mpath::elem::Curve_D< 3 > & > ( elem_n ) );
  _eval_buffer.evaluator ().calc_end_pos ( _pos_current );

  // Default speed value
  double speed = _curve_speed;

  switch ( elem_n.elem_type () ) {
  case snc::mpath::d3::elem::Type::CURVE_LINEAR: {
    sev::lag::Vector3d vdir = _pos_current;
    vdir -= pos_begin;
    const double mag ( sev::lag::magnitude ( vdir ) );
    if ( mag > 1.0e-6 ) {
      vdir /= mag;
      sev::math::assign_smaller< double > (
          speed, acquire_curve_speed_dir_max ( vdir ) );
    }
  } break;

  case snc::mpath::d3::elem::Type::CURVE_CUBIC: {
    sev::lag::Vector3d vpos_prev{ sev::lag::init::zero };
    const std::size_t num_checks = 8;
    for ( std::size_t ii = 0; ii != num_checks; ++ii ) {
      sev::lag::Vector3d vpos;
      _eval_buffer.evaluator ().calc_pos ( vpos,
                                           double ( ii ) / ( num_checks - 1 ) );
      if ( ii != 0 ) {
        sev::lag::Vector3d vdir = vpos;
        vdir -= vpos_prev;
        const double mag = sev::lag::magnitude ( vdir );
        if ( mag > 1.0e-6 ) {
          vdir /= mag;
          sev::math::assign_smaller< double > (
              speed, acquire_curve_speed_dir_max ( vdir ) );
        }
      }
      vpos_prev = vpos;
    }
  } break;

  case snc::mpath::d3::elem::Type::CURVE_CIRCLE: {
    sev::lag::Vector3d vdir;
    vdir[ 0 ] = 1.0;
    vdir[ 1 ] = 0.0;
    vdir[ 2 ] = 0.0;
    sev::math::assign_smaller< double > (
        speed, acquire_curve_speed_dir_max ( vdir ) );
  } break;

  case snc::mpath::d3::elem::Type::CURVE_HELIX: {
    auto & ceval = static_cast< snc::mpath::d3::curve::Evaluator_Helix & > (
        _eval_buffer.evaluator () );
    sev::lag::Vector3d vdir;
    vdir[ 0 ] = ceval.radius () * ceval.angle_radians ();
    vdir[ 1 ] = 0.0;
    vdir[ 2 ] = ceval.height ();
    const double mag = sev::lag::magnitude ( vdir );
    if ( mag > 1.0e-6 ) {
      vdir /= mag;
      sev::math::assign_smaller< double > (
          speed, acquire_curve_speed_dir_max ( vdir ) );
    }
  } break;

  default:
    DEBUG_ASSERT ( false );
    break;
  }

  return speed;
}

double
Speed_Insert3::acquire_curve_speed_dir_max ( const sev::lag::Vector3d & dir_n )
{
  double speed = _curve_speed;

  const double dproj_min = 1.0e-6;
  // Calculate the maximum speed with respect
  // to the maximum normal speed
  if ( dir_n[ 2 ] < 0.0 ) {
    double dproj = std::fabs ( dir_n[ 2 ] );
    if ( dproj > dproj_min ) {
      double vmax ( _curve_speed_normal / dproj );
      sev::math::assign_smaller< double > ( speed, vmax );
    }
  }
  // Calculate the maximum speed with respect
  // to the maximum planar speed
  {
    double dproj =
        ( sev::lag::square ( dir_n[ 0 ] ) + sev::lag::square ( dir_n[ 1 ] ) );
    dproj = sev::lag::square_root ( dproj );
    if ( dproj > dproj_min ) {
      double vmax ( _curve_speed_planar / dproj );
      sev::math::assign_smaller< double > ( speed, vmax );
    }
  }

  return speed;
}

void
Speed_Insert3::set_speed_db_value ( const snc::mpath::Element & elem_n )
{
  auto & celem =
      static_cast< const snc::mpath::elem::Speed_Db_Value & > ( elem_n );
  _speed_db.set_speed ( celem.speed_id (), celem.speed_value () );
  update_speeds ();
}

void
Speed_Insert3::set_speeds ( const snc::mpath::Element & elem_n )
{
  auto & celem = static_cast< const snc::mpath::elem::Speed & > ( elem_n );
  _speed = celem.speed ();
  update_speeds ();
}

void
Speed_Insert3::update_speeds ()
{
  // Acquire curve speed
  if ( _speed.is_symbolic () ) {
    switch ( _speed.regime () ) {
    case snc::mpath::Speed_Regime::RAPID:
      _curve_speed = _speed_db.speed ( snc::mpath::Speed_Db_Id::RAPID );
      _curve_speed_normal = _curve_speed;
      _curve_speed_planar = _curve_speed;
      break;
    default:
      // Default to material speed
      _curve_speed = _speed_db.speed ( snc::mpath::Speed_Db_Id::MATERIAL );
      _curve_speed_normal =
          _speed_db.speed ( snc::mpath::Speed_Db_Id::MATERIAL_NORMAL );
      _curve_speed_planar =
          _speed_db.speed ( snc::mpath::Speed_Db_Id::MATERIAL_PLANAR );
      break;
    }
  } else {
    _curve_speed = _speed.value ();
    _curve_speed_normal = _curve_speed;
    _curve_speed_planar = _curve_speed;
  }

  sev::math::assign_larger< double > ( _curve_speed, _curve_speed_min );
  sev::math::assign_larger< double > ( _curve_speed_normal,
                                       _curve_speed_normal_min );
  sev::math::assign_larger< double > ( _curve_speed_planar,
                                       _curve_speed_planar_min );
}

} // namespace mpath_stream::read
