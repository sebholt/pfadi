/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#include "go_to.hpp"
#include <sev/assert.hpp>
#include <snc/mpath/d3/types.hpp>
#include <snc/mpath/element.hpp>
#include <snc/mpath/element_buffer_d.hpp>

namespace mpath_stream::read
{

Go_To::Go_To ()
: _pos_begin ( sev::lag::init::zero )
, _pos_end ( sev::lag::init::zero )
, _pos_middle ( sev::lag::init::zero )
{
}

void
Go_To::setup ( const sev::lag::Vector3d & pos_begin_n,
               const sev::lag::Vector3d & pos_end_n )
{
  _pos_begin = pos_begin_n;
  _pos_end = pos_end_n;
}

void
Go_To::set_speed_regime ( snc::mpath::Speed_Regime regime_n )
{
  _speed_regime = regime_n;
}

bool
Go_To::open ()
{
  if ( is_open () ) {
    return true;
  }

  if ( _pos_begin != _pos_end ) {
    if ( pos_begin ()[ 2 ] > pos_end ()[ 2 ] ) {
      // Move horizontally first
      _pos_middle[ 0 ] = pos_end ()[ 0 ];
      _pos_middle[ 1 ] = pos_end ()[ 1 ];
      // Keep z from the begin
      _pos_middle[ 2 ] = pos_begin ()[ 2 ];
    } else {
      // Move vertically first
      // Keep xy from the begin
      _pos_middle[ 0 ] = pos_begin ()[ 0 ];
      _pos_middle[ 1 ] = pos_begin ()[ 1 ];
      _pos_middle[ 2 ] = pos_end ()[ 2 ];
    }
    _stage = Stage::SPEED;
  } else {
    _stage = Stage::NONE;
  }
  set_state_good ();
  return is_open ();
}

void
Go_To::close ()
{
  _stage = Stage::NONE;
  set_state_closed ();
}

const snc::mpath::Element *
Go_To::read ()
{
  DEBUG_ASSERT ( is_open () );

  do {
    switch ( _stage ) {
    case Stage::NONE:
      break;

    case Stage::SPEED:
      _stage = Stage::LEG_1;
      if ( speed_regime () < snc::mpath::Speed_Regime::LIST_END ) {
        auto & elem = _element_variant.emplace< snc::mpath::elem::Speed > ();
        elem.speed ().set_regime ( speed_regime () );
        return &elem;
      }
      break;

    case Stage::LEG_1:
      _stage = Stage::LEG_2;
      if ( pos_begin () != _pos_middle ) {
        return &_element_variant.emplace< snc::mpath::d3::elem::Curve_Linear > (
            _pos_middle );
      }
      break;

    case Stage::LEG_2:
      _stage = Stage::NONE;
      if ( _pos_end != _pos_middle ) {
        return &_element_variant.emplace< snc::mpath::d3::elem::Curve_Linear > (
            _pos_end );
      }
      break;
    }
  } while ( _stage != Stage::NONE );

  // Finalize stream
  set_state_finished ();
  return &_element_variant.emplace< snc::mpath::elem::Stream_End > ();
}

} // namespace mpath_stream::read
