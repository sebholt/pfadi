/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <sev/lag/vector3.hpp>
#include <snc/device/statics/statics.hpp>
#include <snc/mpath/curve_evaluator_buffer_d.hpp>
#include <snc/mpath/elem/speed.hpp>
#include <snc/mpath/elem/stream_end.hpp>
#include <snc/mpath/speed_database.hpp>
#include <snc/mpath_stream/read.hpp>
#include <variant>

namespace mpath_stream::read
{

/// @brief Replaces speed ids with actual speed values
///
class Speed_Insert3 : public snc::mpath_stream::Read
{
  public:
  // -- Construction

  Speed_Insert3 ();

  void
  setup ( snc::mpath_stream::Read * src_stream_n,
          const sev::lag::Vector3d & pos_current_n,
          const snc::device::statics::Statics & device_statics_n,
          const snc::mpath::Speed_Database & speed_db_n );

  // --- Stream interface

  bool
  open () override;

  void
  close () override;

  const snc::mpath::Element *
  read () override;

  private:
  // -- Utility

  void
  feed_from_source ();

  double
  acquire_curve_speed ( const snc::mpath::Element & elem_n );

  double
  acquire_curve_speed_dir_max ( const sev::lag::Vector3d & dir_n );

  void
  set_speed_db_value ( const snc::mpath::Element & elem_n );

  void
  set_speeds ( const snc::mpath::Element & elem_n );

  void
  update_speeds ();

  private:
  // -- Attributes
  snc::mpath_stream::Read * _source_stream = nullptr;

  const snc::mpath::Element * _elem_speed = nullptr;
  const snc::mpath::Element * _elem_source = nullptr;

  sev::lag::Vector3d _pos_current;
  double _curve_speed_latest = 0.0;

  double _curve_speed = 0.0;
  double _curve_speed_planar = 0.0;
  double _curve_speed_normal = 0.0;

  sev::lag::Vector3d _curve_speeds_min;
  sev::lag::Vector3d _curve_speeds_max;

  double _curve_speed_min = 0.0;
  double _curve_speed_planar_min = 0.0;
  double _curve_speed_normal_min = 0.0;
  snc::mpath::Speed _speed;

  snc::mpath::Speed_Database _speed_db;

  std::variant< snc::mpath::elem::Speed, snc::mpath::elem::Stream_End >
      _elem_variant;
  snc::mpath::Curve_Evaluator_Buffer_D3 _eval_buffer;
};

} // namespace mpath_stream::read
