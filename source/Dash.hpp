/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <snc/device/qt/Info.hpp>
#include <snc/device/statics/statics.hpp>
#include <snc/svp/dash/BasicDash.hpp>
#include <snc/svs/ctl/align_axis/dash/Panel.hpp>
#include <snc/svs/ctl/align_multi/dash/Panel.hpp>
#include <snc/svs/ctl/autostart/dash/Panel.hpp>
#include <snc/svs/ctl/axis_move/dash/Panel.hpp>
#include <snc/svs/ctl/axis_position/dash/Panel.hpp>
#include <snc/svs/ctl/axis_speed/dash/Panel.hpp>
#include <snc/svs/ctl/controls/dash/Panel.hpp>
#include <snc/svs/ctl/lookout/dash/Panel.hpp>
#include <snc/svs/ctl/machine_res/dash/Panel.hpp>
#include <snc/svs/fac/axis_align/dash/Panel.hpp>
#include <snc/svs/fac/axis_feed/dash/Panel.hpp>
#include <snc/svs/fac/axis_move/dash/axes_n.hpp>
#include <snc/svs/fac/controls/dash/Panel.hpp>
#include <snc/svs/fac/machine_startup/dash/Panel.hpp>
#include <snc/svs/fac/mpath/dash/Panel_N.hpp>
#include <snc/svs/fac/serial_device/dash/Panel.hpp>
#include <snc/svs/fac/tracker/dash/Panel.hpp>
#include <svs/mpath_loader/Panel.hpp>
#include <svs/mpath_painter/Panel.hpp>
#include <svs/mpath_transform/Panel.hpp>
#include <svs/program/dash/Panel.hpp>

namespace dash
{

class AxisCommon : public QObject
{
  Q_OBJECT

  Q_PROPERTY ( int index READ index CONSTANT )
  Q_PROPERTY ( QObject * info READ info NOTIFY infoChanged )

  public:
  // -- Construction

  AxisCommon ( snc::svp::dash::Dash & dash_n,
               int index_n,
               snc::device::qt::Info * info_n );

  Q_SLOT
  void
  updateAxisInfo ();

  // -- Index

  int
  index () const
  {
    return _index;
  }

  // -- State

  snc::device::qt::axes::Axis *
  info ()
  {
    return _info.get ();
  }

  Q_SIGNAL
  void
  infoChanged ();

  private:
  // -- Attributes
  int _index = 0;
  snc::device::qt::Info * _machineInfo = nullptr;
  std::shared_ptr< snc::device::qt::axes::Axis > _info;
  QObject * _statics = nullptr;
};

class AxisLinear : public AxisCommon
{
  Q_OBJECT

  Q_PROPERTY ( QObject * alignPilot READ alignPilot CONSTANT )
  Q_PROPERTY ( QObject * align READ align CONSTANT )
  Q_PROPERTY ( QObject * move READ move CONSTANT )
  Q_PROPERTY ( QObject * position READ position CONSTANT )
  Q_PROPERTY ( QObject * speed READ speed CONSTANT )

  public:
  // -- Construction

  AxisLinear ( snc::svp::dash::Dash & dash_n,
               int index_n,
               snc::device::qt::Info * info_n )
  : AxisCommon ( dash_n, index_n, info_n )
  , _align_pilot ( dash_n, index () )
  , _align ( dash_n, index () )
  , _move_pilot ( dash_n, index () )
  , _move ( dash_n, index () )
  , _position ( dash_n, index () )
  , _speed ( dash_n, index () )
  {
  }

  // -- Accessors

  snc::svs::fac::axis_align::dash::Panel *
  alignPilot ()
  {
    return &_align_pilot;
  }

  snc::svs::ctl::align_axis::dash::Panel *
  align ()
  {
    return &_align;
  }

  snc::svs::ctl::axis_move::dash::Panel *
  move ()
  {
    return &_move;
  }

  snc::svs::ctl::axis_position::dash::Panel *
  position ()
  {
    return &_position;
  }

  snc::svs::ctl::axis_speed::dash::Panel *
  speed ()
  {
    return &_speed;
  }

  private:
  // -- Attributes
  snc::svs::fac::axis_align::dash::Panel _align_pilot;
  snc::svs::ctl::align_axis::dash::Panel _align;

  snc::svs::fac::axis_move::dash::Panel _move_pilot;
  snc::svs::ctl::axis_move::dash::Panel _move;
  snc::svs::ctl::axis_position::dash::Panel _position;
  snc::svs::ctl::axis_speed::dash::Panel _speed;
};

} // namespace dash

class Dash : public snc::svp::dash::BasicDash
{
  Q_OBJECT

  // -- Properties
  Q_PROPERTY ( QObject * tracker READ tracker CONSTANT )
  Q_PROPERTY ( QObject * machine_startup READ machine_startup CONSTANT )
  Q_PROPERTY ( QObject * machine_res READ machine_res CONSTANT )
  Q_PROPERTY ( QObject * controls READ controls CONSTANT )
  Q_PROPERTY ( QObject * axisX READ axisX CONSTANT )
  Q_PROPERTY ( QObject * axisY READ axisY CONSTANT )
  Q_PROPERTY ( QObject * axisZ READ axisZ CONSTANT )
  Q_PROPERTY ( QObject * alignMulti READ alignMulti CONSTANT )
  Q_PROPERTY ( QObject * program READ program CONSTANT )

  // -- Types
  private:
  using Super = snc::svp::dash::BasicDash;

  public:
  // -- Construction

  Dash ( const sev::logt::Reference & log_parent_n,
         sev::thread::Tracker * thread_tracker_n,
         QObject * qparent_n = nullptr );

  ~Dash ();

  // -- Accessors

  snc::svs::fac::tracker::dash::Panel *
  tracker ()
  {
    return &_tracker;
  }

  snc::svs::fac::machine_startup::dash::Panel *
  machine_startup ()
  {
    return &_pilot_machine_startup;
  }

  snc::svs::ctl::machine_res::dash::Panel *
  machine_res ()
  {
    return &_pilot_machine_res;
  }

  snc::svs::ctl::controls::dash::Panel *
  controls ()
  {
    return &_controls;
  }

  // - Axes

  dash::AxisLinear *
  axisX ()
  {
    return &_axis_x;
  }

  dash::AxisLinear *
  axisY ()
  {
    return &_axis_y;
  }

  dash::AxisLinear *
  axisZ ()
  {
    return &_axis_z;
  }

  Q_INVOKABLE
  QObject *
  getAxis ( int index_n )
  {
    if ( _axis_x.index () == index_n ) {
      return &_axis_x;
    }
    if ( _axis_y.index () == index_n ) {
      return &_axis_y;
    }
    if ( _axis_z.index () == index_n ) {
      return &_axis_z;
    }
    return nullptr;
  }

  // -- Program

  snc::svs::ctl::align_multi::dash::Panel *
  alignMulti ()
  {
    return &_align_multi;
  }

  svs::program::dash::Panel *
  program ()
  {
    return &_program;
  }

  protected:
  // -- Session interface

  void
  session_begin () override;

  private:
  // -- Attributes
  snc::svs::ctl::lookout::dash::Panel _lookout;
  snc::svs::ctl::autostart::dash::Panel _autostart;

  snc::svs::fac::serial_device::dash::Panel _serial_device;
  snc::svs::fac::tracker::dash::Panel _tracker;

  snc::svs::fac::machine_startup::dash::Panel _pilot_machine_startup;
  snc::svs::ctl::machine_res::dash::Panel _pilot_machine_res;
  snc::svs::fac::controls::dash::Panel _pilot_controls;
  snc::svs::fac::mpath::dash::Panel_N< 3 > _pilot_mpath;

  snc::svs::ctl::controls::dash::Panel _controls;

  dash::AxisLinear _axis_x;
  dash::AxisLinear _axis_y;
  dash::AxisLinear _axis_z;

  snc::svs::ctl::align_multi::dash::Panel _align_multi;

  svs::mpath_loader::Panel _mpath_loader;
  svs::mpath_painter::Panel _mpath_painter;
  svs::mpath_transform::Panel _mpath_transform;
  svs::program::dash::Panel _program;
};
