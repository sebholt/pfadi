import QtQuick 2.11
import QtQuick.Controls 2.4

Label {
  id: entry
  property string entryName
  signal clicked()

  text: entryName

  Component.onCompleted: {
    mouseArea.clicked.connect(clicked)
  }

  MouseArea {
    id: mouseArea
    anchors.fill: parent
    cursorShape: Qt.PointingHandCursor
  }
}
