import QtQml 2.11
import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4
import SncQ 1.0

RowLayout {
  id: root
  spacing: 32

  property var axes: ListModel {}

  // Axes
  PanelH {
    title: qsTr("Axes")
    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
    Layout.fillHeight: true

    GridLayout {
      columns: 1 + root.axes.count
      columnSpacing: 24

      Layout.alignment: Qt.AlignLeft | Qt.AlignTop
      Layout.fillHeight: false

      HeadLabelV { text: "" }
      Repeater {
        model: root.axes
        HeadLabelH { text: axisName }
      }

      HeadLabelV { text: qsTr("Aligned") }
      Repeater {
        model: root.axes
        StatusLight {
          active: dash.getAxis(axisIndex).info.state.aligned
          color: "green"
        }
      }

      HeadLabelV { text: qsTr("Process") }
      Repeater {
        model: root.axes
        StackLayout {
          id: progressLabelLayout
          property var axis: dash.getAxis(axisIndex)
          Layout.alignment: Qt.AlignLeft | Qt.AlignTop
          Layout.fillWidth: false
          Layout.fillHeight: false

          function updateCurrentIndex() {
            if ( axis.alignPilot.sequenceApproach ) {
              currentIndex = 0
            } else if ( axis.alignPilot.sequenceWithdraw ) {
              currentIndex = 1
            } else if ( axis.alignPilot.sequenceBreak ) {
              currentIndex = 2
            } else {
              currentIndex = 3
            }
          }

          Component.onCompleted: updateCurrentIndex()
          Connections {
            target: axis.alignPilot
            function onSequenceChanged() {
              updateCurrentIndex()
            }
          }

          Label { text: qsTr("Approaching") }
          Label { text: qsTr("Withdrawing") }
          Label { text: qsTr("Breaking") }
          Label { text: qsTr("Idle") }
        }
      }

      HeadLabelV { text: qsTr("Speed") }
      Repeater {
        model: root.axes
        NumberLabel {
          property var axis: dash.getAxis(axisIndex)
          Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
          precision: 3
          value: axis.alignPilot.speedTarget
          valueMin: -axis.info.speedMax
          valueMax: axis.info.speedMax
        }
      }

    }
  }

  // Sequence
  PanelH {
    title: qsTr("Program")
    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
    Layout.fillHeight: true

    ComboBox {
      id: progSelect
      model: ["Serial", "Parallel", "Set", "Axes"]
      wheelEnabled: true
      enabled: dash.alignMulti.startable
      onCurrentIndexChanged: {
        progStack.currentIndex = currentIndex
      }
    }

    Component {
      id: progSerial
      ColumnLayout {
        Layout.fillHeight: false
        Button {
          text: "Start"
          enabled: dash.alignMulti.startable
          onClicked: dash.alignMulti.alignSerial()
        }
        Button {
          text: "Stop"
          enabled: dash.alignMulti.stoppable
          onClicked: dash.alignMulti.alignAbort()
        }
      }
    }
    Component {
      id: progParallel
      ColumnLayout {
        Button {
          text: "Start"
          enabled: dash.alignMulti.startable
          onClicked: dash.alignMulti.alignParallel()
        }
        Button {
          text: "Stop"
          enabled: dash.alignMulti.stoppable
          onClicked: dash.alignMulti.alignAbort()
        }
      }
    }
    Component {
      id: progSet
      ColumnLayout {
        Button {
          text: "Set aligned"
          enabled: dash.alignMulti.startable
          onClicked: dash.alignMulti.alignSet()
        }
      }
    }
    Component {
      id: progAxes
      GridLayout {
        flow: GridLayout.TopToBottom
        rows: root.axes.count
        columnSpacing: 16

        Repeater {
          model: root.axes
          HeadLabelV { text: axisName }
        }
        Repeater {
          model: root.axes
          StackLayout {
            property var axis: dash.getAxis(axisIndex)
            currentIndex: axis.align.stoppable ? 1 : 0
            Button {
              text: qsTr("Start")
              enabled: axis.align.startable
              Layout.fillWidth: true
              onClicked: axis.align.alignBegin()
            }
            Button {
              text: qsTr("Stop")
              enabled: axis.align.stoppable
              Layout.fillWidth: true
              onClicked: axis.align.alignAbort()
            }
          }
        }
      }
    }

    StackLayout {
      id: progStack
      Loader { sourceComponent: progSerial; Layout.fillHeight: false }
      Loader { sourceComponent: progParallel; Layout.fillHeight: false }
      Loader { sourceComponent: progSet; Layout.fillHeight: false }
      Loader { sourceComponent: progAxes; Layout.fillHeight: false }
    }
  }

}
