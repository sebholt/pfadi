import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4
import Qt.labs.folderlistmodel 2.1
import SncQ 1.0

ColumnLayout {
  id: base
  property var rootFolder: "file:///home"
  property var folder: "file:///home"

  spacing: 8

  signal fileSelected ( url fileUrl )

  RowLayout {
    spacing: 8

    Layout.fillHeight: true
    Layout.alignment: Qt.AlignLeft | Qt.AlignTop

    HeadLabelH { text: qsTr("Directory:") }
    Label { text: base.folder }
  }

  RowLayout {
    spacing: 6

    ColumnLayout {
      Layout.fillWidth: false

      HeadLabelV { id: dirsHead; text: qsTr("Directories") }
      Rectangle {
        color: dirsHead.color
        Layout.fillWidth: true
        implicitHeight: 1
      }

      ListView {
        id: dirsView

        implicitWidth: sampleDirItem.implicitWidth
        implicitHeight: sampleDirItem.implicitHeight*5

        Layout.fillWidth: true
        Layout.fillHeight: true
        Layout.alignment: Qt.AlignLeft | Qt.AlignTop

        function selectSubDir(dirName) {
          if ( dirName == ".") {
            return
          }
          if ( dirName == "..") {
            base.folder = dirsModel.parentFolder
          } else {
            base.folder = base.folder + "/" + dirName
          }
        }

        FolderListModel {
          id: dirsModel

          showFiles:false
          rootFolder: base.rootFolder
          folder: base.folder
          showDotAndDotDot: true
        }

        FileBrowserEntry {
          id: sampleDirItem
          entryName: "Some long directory name"
          visible: false
        }

        model: dirsModel
        delegate: FileBrowserEntry {
          entryName: fileName
          width: dirsView.width
          onClicked: dirsView.selectSubDir(entryName)
        }
        clip: true
      }
    }

    ColumnLayout {
      Layout.fillWidth: true
  
      HeadLabelV { id: filesHead; text: qsTr("Files") }
      Rectangle {
        color: filesHead.color
        Layout.fillWidth: true
        implicitHeight: 1
      }

      ListView {
        id: filesView

        implicitWidth: sampleDirItem.implicitWidth
        implicitHeight: sampleFileItem.implicitHeight*5

        Layout.fillWidth: true
        Layout.fillHeight: true
        Layout.alignment: Qt.AlignLeft | Qt.AlignTop

        function selectFile(fileName) {
          var fileAbs = base.folder + "/" + fileName
          base.fileSelected(fileAbs)
        }

        FolderListModel {
          id: filesModel

          showDirs:false
          nameFilters: [ "*.mpml", "*.MPML", "*.gcode", "*.GCODE" ]
          rootFolder: base.rootFolder
          folder: base.folder
        }

        FileBrowserEntry {
          id: sampleFileItem
          entryName: "Some considerable long file name"
          visible: false
        }

        model: filesModel
        delegate: FileBrowserEntry {
          entryName: fileName
          width: filesView.width
          onClicked: filesView.selectFile(entryName)
        }
        clip: true
      }
    }
  }
}

