import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4
import SncQ 1.0

ColumnLayout {
  id: root

  property var axes: ListModel {
    ListElement {
      axisName: "x"
      axisIndex: 0
    }
    ListElement {
      axisName: "y"
      axisIndex: 1
    }
    ListElement {
      axisName: "z"
      axisIndex: 2
    }
  }

  // Transform inputs
  Component {
    id: emptyItem
    Item { implicitWidth: 1; implicitHeight: 1 }
  }

  ButtonGroup {
    id: xyGroup
  }
  Component {
    id: xyButton
    RadioButton {
      property string bbx
      property string bby
      ButtonGroup.group: xyGroup
    }
  }

  Component {
    id: bboxSelect
    GridLayout {
      columns: 3

      Loader {
        sourceComponent: xyButton
        onLoaded: { item.bbx="min"; item.bby="max"; }
      }
      Loader {
        sourceComponent: xyButton
        onLoaded: { item.bbx="mid"; item.bby="max"; }
      }
      Loader {
        sourceComponent: xyButton
        onLoaded: { item.bbx="max"; item.bby="max"; }
      }

      Loader {
        sourceComponent: xyButton
        onLoaded: { item.bbx="min"; item.bby="mid"; }
      }
      Loader {
        sourceComponent: xyButton
        onLoaded: { item.bbx="mid"; item.bby="mid"; }
      }
      Loader {
        sourceComponent: xyButton
        onLoaded: { item.bbx="max"; item.bby="mid"; }
      }

      Loader {
        sourceComponent: xyButton
        onLoaded: { item.bbx="min"; item.bby="min"; }
      }
      Loader {
        sourceComponent: xyButton
        onLoaded: { item.bbx="mid"; item.bby="min"; }
      }
      Loader {
        sourceComponent: xyButton
        onLoaded: { item.bbx="max"; item.bby="min"; }
      }
    }
  }
  RowLayout {
    spacing: 32

    Section {
      title: qsTr("Use current x,y as path")
      Layout.alignment: Qt.AlignHCenter | Qt.AlignTop

      GridLayout {
        flow: GridLayout.TopToBottom
        rows: 2
        columnSpacing: 16

        Label {
          text: qsTr("basis")
          Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
        }
        Loader {
          sourceComponent: xyButton
          Layout.alignment: Qt.AlignTop | Qt.AlignVCenter
          onLoaded: {
            item.text = qsTr("x=0.0\ny=0.0")
            item.bbx="base";
            item.bby="base";
            item.checked=true;
          }
        }

        Label {
          text: qsTr("bounding box position")
          Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
        }
        ColumnLayout {
          Layout.alignment: Qt.AlignHCenter | Qt.AlignTop

          GridLayout {
            columns: 3

            Label {
              text: qsTr("y")
              Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            }
            Loader { sourceComponent: bboxSelect }
            Label {
              text: qsTr("Max")
              Layout.alignment: Qt.AlignRight | Qt.AlignTop
            }

            Label {
              text: qsTr("Min")
            }
            Label {
              text: qsTr("x")
              Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            }
            Loader { sourceComponent: emptyItem }
          }
        }
      }
    }

    Section {
      title: qsTr("Use current z as path")
      Layout.alignment: Qt.AlignHCenter | Qt.AlignTop

      GridLayout {
        flow: GridLayout.TopToBottom
        rows: 2
        columnSpacing: 16

        Label {
          text: qsTr("basis")
          Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
        }
        RadioButton {
          text: qsTr("z=0.0")
          checked: true
          Layout.alignment: Qt.AlignTop | Qt.AlignVCenter
        }
      }
    }

    Section {
      id: offset
      title: qsTr("Offset")
      Layout.alignment: Qt.AlignHCenter | Qt.AlignTop

      function inputValue(axisIndex) {
        for (var ii=0; ii < offsetInputs.count; ii++) {
          var input = offsetInputs.itemAt(ii)
          if (input.axis == axisIndex) {
            return input.valueLatest
          }
        }
        return 0.0
      }

      GridLayout {
        flow: GridLayout.TopToBottom
        rows: root.axes.count

        columnSpacing: 16
        Layout.fillWidth: false

        Repeater {
          model: root.axes
          HeadLabelV {
            text: axisName
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
          }
        }

        Repeater {
          id: offsetInputs
          model: root.axes
          NumberInput {
            property int axis: axisIndex
            value: 0.0
            valueMax: 10*1000*1000
            valueMin: -valueMax
            precision: 3
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.fillWidth: true
          }
        }
      }
    }

    Section {
      id: sectionTake
      title: qsTr("From current position take")
      Layout.alignment: Qt.AlignHCenter | Qt.AlignTop

      function transform_axis(axis, mode) {
        var offsetValue = offset.inputValue(axis)
        switch (mode) {
        case "base":
          dash.program.setTransformBase(axis, offsetValue)
          break
        case "min":
          dash.program.setTransformMin(axis, offsetValue)
          break
        case "mid":
          dash.program.setTransformMid(axis, offsetValue)
          break
        case "max":
          dash.program.setTransformMax(axis, offsetValue)
          break
        default:
          break
        }
      }

      function transform_x() {
        var btn = xyGroup.checkedButton
        if (btn) {
          transform_axis ( 0, btn.bbx )
        }
      }

      function transform_y() {
        var btn = xyGroup.checkedButton
        if (btn) {
          transform_axis ( 1, btn.bby )
        }
      }

      function transform_z() {
        transform_axis ( 2, "base" )
      }

      function transform_xy() {
        transform_x()
        transform_y()
      }

      function transform_xyz() {
        transform_x()
        transform_y()
        transform_z()
      }

      RowLayout {
        spacing: 16
        ColumnLayout {
          Layout.alignment: Qt.AlignLeft | Qt.AlignTop
          Button {
            text: qsTr("x")
            onClicked: sectionTake.transform_x()
          }
          Button {
            text: qsTr("y")
            onClicked: sectionTake.transform_y()
          }
          Button {
            text: qsTr("z")
            onClicked: sectionTake.transform_z()
          }
        }
        ColumnLayout {
          Layout.alignment: Qt.AlignLeft | Qt.AlignTop
          Button {
            text: qsTr("x,y")
            onClicked: sectionTake.transform_xy();
          }
          Button {
            text: qsTr("x,y,z")
            onClicked: sectionTake.transform_xyz();
          }
        }
      }
    }
  }

  // Transform display
  RowLayout {
    spacing: 32

    Section {
      title: qsTr("Transform base [mm]")
      Layout.alignment: Qt.AlignLeft | Qt.AlignTop

      GridLayout {
        flow: GridLayout.TopToBottom
        rows: 1 + root.axes.count

        columnSpacing: 16
        Layout.fillWidth: false

        HeadLabelH {
          text: qsTr("Axis")
          Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        }
        Repeater {
          model: root.axes
          HeadLabelV {
            text: axisName
            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
          }
        }

        HeadLabelH {
          text: qsTr("Position")
          Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
        }
        Repeater {
          model: root.axes
          NumberLabel {
            value: dash.program.transformBase.getOrCreate(axisIndex).value
            valueMax: 10*1000*1000
            precision: 3
            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
            Layout.fillWidth: true
          }
        }
      }
    }

    Section {
      title: qsTr("Transformed boundings [mm]")
      Layout.alignment: Qt.AlignLeft | Qt.AlignTop
      SequenceMPathBoundings {
        showAxes: false
        axes: root.axes
        pathStats: dash.program.pathStatsTransformed
      }
    }

    Section {
      title: qsTr("Transformed edge distances [mm]")
      Layout.alignment: Qt.AlignLeft | Qt.AlignTop
      Layout.fillWidth: false

      GridLayout {
        id: edges
        property var distBottom: dash.program.edgeDistBottom
        property var distTop: dash.program.edgeDistTop

        flow: GridLayout.TopToBottom
        rows: 1 + root.axes.count

        columnSpacing: 16
        Layout.alignment: Qt.AlignLeft | Qt.AlignTop
        Layout.fillWidth: false

        HeadLabelH {
          text: qsTr("Bottom")
          Layout.alignment: Qt.AlignRight | Qt.AlignTop
        }
        Repeater {
          model: root.axes
          NumberLabel {
            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
            Layout.fillWidth: true

            value: edges.distBottom.getOrCreate(axisIndex).value
            valueMax: 10*1000*1000
            precision: 3
          }
        }

        HeadLabelH {
          text: qsTr("Top")
          Layout.alignment: Qt.AlignRight | Qt.AlignTop
        }
        Repeater {
          model: root.axes
          NumberLabel {
            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
            Layout.fillWidth: false

            value: edges.distTop.getOrCreate(axisIndex).value
            valueMax: 10*1000*1000
            precision: 3
          }
        }
      }
    }

  }
}
