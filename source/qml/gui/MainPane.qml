import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4

Pane {
  id: root

  property int panelHSpace: 12
  property int panelVSpace: 12

  contentWidth: layRoot.implicitWidth
  contentHeight: layRoot.implicitHeight
  padding: 12

  RowLayout {
    id: layRoot
    spacing: panelHSpace
    anchors.fill: parent

    ColumnLayout {
      spacing: panelVSpace
      Layout.alignment: Qt.AlignLeft | Qt.AlignTop
      Layout.fillHeight: true

      // -- State
      StatePanel {
        Layout.fillWidth: true
        Layout.fillHeight: false
      }

      // -- Control
      ControlPanel {
        Layout.fillWidth: true
        Layout.fillHeight: true
      }

      // -- Sequence
      SequencePanel {
        Layout.fillWidth: true
        Layout.fillHeight: true
      }
    }

    // -- Device
    ColumnLayout {
      Layout.alignment: Qt.AlignLeft | Qt.AlignTop
      PanelDevice {
        Layout.alignment: Qt.AlignLeft | Qt.AlignTop
        Layout.fillWidth: true
        Layout.fillHeight: true
      }
    }
  }
}