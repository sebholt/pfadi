import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4
import SncQ 1.0

Section {
  id: sectionChecks
  title: qsTr("Start checks")

  property list<QtObject> checks: [
    QtObject {
      property string name: qsTr("File selected")
      property bool valid: true
      property bool value: dash.program.pathFileOpened
    },
    QtObject {
      property string name: qsTr("Path valid")
      property bool valid: dash.program.pathFileOpened
      property bool value: dash.program.pathFileValid
    },
    QtObject {
      property string name: qsTr("Path inside machine area")
      property var valid: dash.program.pathFileOpened
      property bool value: dash.program.pathFitsWorkspace
    },
    QtObject {
      property string name: qsTr("Controller available")
      property var valid: dash.program.pathFileOpened
      property bool value: dash.program.controllerAvailable
    }
  ]

  GridLayout {
    flow: GridLayout.TopToBottom
    rows: sectionChecks.checks.length
    columnSpacing: 12

    Repeater {
      model: sectionChecks.checks
      Label {
        text: name
        Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
      }
    }

    Repeater {
      model: sectionChecks.checks
      StatusLight {
        active: valid
        color: value ? "green" : "darkOrange"
        Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
      }
    }
  }
}
