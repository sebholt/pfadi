import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4

Panel {
  title: qsTr("Sequence")

  RowLayout {
    spacing: 16
    Layout.fillWidth: true
    
    ColumnLayout {
      spacing: 0
      Layout.alignment: Qt.AlignLeft | Qt.AlignTop
      Layout.fillWidth: false
      Button {
        text: "Source"
        checkable: true
        checked: true
        autoExclusive: true
        Layout.fillWidth: true
        onPressed: stackPages.currentIndex = 0
      }
      Button {
        text: "Transform"
        checkable: true
        autoExclusive: true
        Layout.fillWidth: true
        onPressed: stackPages.currentIndex = 1
      }
      Button {
        text: "Speeds"
        checkable: true
        autoExclusive: true
        Layout.fillWidth: true
        onPressed: stackPages.currentIndex = 2
      }
      Button {
        text: "Progress"
        checkable: true
        autoExclusive: true
        Layout.fillWidth: true
        onPressed: stackPages.currentIndex = 3
      }
    }

    StackLayout {
      id: stackPages
      Layout.alignment: Qt.AlignLeft | Qt.AlignTop
      Layout.fillWidth: true

      SequenceSource {}
      SequenceTransform {}
      SequenceSpeeds {}
      SequenceProgress {}
    }
  }
}