import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4
import QtQuick.Extras 1.4
import SncQ 1.0

Panel {
  title: qsTr("Device state")

  property var leftAlign: Qt.AlignLeft | Qt.AlignBottom
  property var rightAlign: Qt.AlignRight | Qt.AlignBottom

  RowLayout {
    spacing: 16
    Layout.alignment: leftAlign
    Label {
      id: connectionLabel
      text: qsTr("Connected"); font.bold: true;
    }
    StatusLight {
      color: "green"
      active: dash.machine_startup.connected
    }
  }

  Section {
    title: qsTr("Serial bitrates in bits/s")

    GridLayout {
      columns: 2
      columnSpacing: 24

      Label { text: qsTr("In") }
      NumberLabel {
        value: dash.tracker.info.state.serialBitrateIn;
        valueMax: 10 * 1000 * 1000;
        Layout.alignment: rightAlign
      }
      Label { text: qsTr("Out") }
      NumberLabel {
        value: dash.tracker.info.state.serialBitrateOut;
        valueMax: 10 * 1000 * 1000;
        Layout.alignment: rightAlign
      }
      Label { text: qsTr("Total") }
      NumberLabel {
        value: dash.tracker.info.state.serialBitrateTotal;
        valueMax: 10 * 1000 * 1000;
        Layout.alignment: rightAlign
      }
    }
  }

  Section {
    title: qsTr("Latency in ms")

    NumberLabel {
      value: dash.tracker.info.state.latency;
      valueMax: 1000 * 10;
      Layout.alignment: rightAlign
    }
  }

  Section {
    title: qsTr("Threads")

    Layout.fillWidth: true
    Layout.fillHeight: true

    ThreadStats {
      Layout.fillWidth: true
      Layout.fillHeight: true
    }
  }
}
