import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4
import SncQ 1.0

ColumnLayout {
  GridLayout {
    flow: GridLayout.TopToBottom
    rows: 5
    columnSpacing: 32

    HeadLabelV {
      text: qsTr("Type")
      Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
    }
    Repeater {
      model: dash.program.speedsAccepted
      Label {
        text: name
        Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
      }
    }

    HeadLabelV {
      text: qsTr("Accepted")
      Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
    }
    Repeater {
      model: dash.program.speedsAccepted
      NumberLabel {
        precision: 3
        value: speed
        valueMin: 0.0
        valueMax: dash.tracker.info.statics.speedMax
        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
      }
    }

    HeadLabelV {
      text: qsTr("Requested")
      Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
    }
    Repeater {
      model: dash.program.speedsRequested
      NumberInput {
        precision: 3
        value: speed
        valueMin: 0.0
        valueMax: 10000.0
        onValueRequestChanged: {
          speed = valueRequest
        }
      }
    }

    Label {}
    Repeater {
      model: dash.program.speedsAccepted
      Label { text: qsTr("mm/s") }
    }
  }
}
