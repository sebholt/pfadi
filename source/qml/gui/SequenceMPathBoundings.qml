import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4
import SncQ 1.0
import SncQ 1.0

GridLayout {
  property bool showAxes: true
  property var pathStats: MPathStatistics {}
  property var axes: ListModel {
    ListElement {
      axisName: "x"
      axisIndex: 0
    }
    ListElement {
      axisName: "y"
      axisIndex: 1
    }
    ListElement {
      axisName: "z"
      axisIndex: 2
    }
  }

  flow: GridLayout.TopToBottom
  rows: 1 + axes.count
  columnSpacing: 16

  HeadLabelH {
    text: qsTr("Axis")
    visible: showAxes
    Layout.alignment: Qt.AlignRight | Qt.AlignTop
  }
  Repeater {
    model: axes
    HeadLabelV {
      text: axisName
      visible: showAxes
      Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
    }
  }

  HeadLabelH {
    text: qsTr("Min")
    Layout.alignment: Qt.AlignRight | Qt.AlignTop
  }
  Repeater {
    model: axes.count
    NumberLabel {
      value: pathStats.axes.getOrCreate(axes.get(index).axisIndex).min
      valueMax: 10*1000
      precision: 3
      Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
    }
  }

  HeadLabelH {
    text: qsTr("Max")
    Layout.alignment: Qt.AlignRight | Qt.AlignTop
  }
  Repeater {
    model: axes.count
    NumberLabel {
      value: pathStats.axes.getOrCreate(axes.get(index).axisIndex).max
      valueMax: 10*1000
      precision: 3
      Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
    }
  }

  HeadLabelH {
    text: qsTr("Length")
    Layout.alignment: Qt.AlignRight | Qt.AlignTop
  }
  Repeater {
    model: axes.count
    NumberLabel {
      value: pathStats.axes.getOrCreate(axes.get(index).axisIndex).length
      valueMax: 10*1000
      precision: 3
      Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
    }
  }
}
