import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4
import QtQuick.Window 2.2
import SncQ 1.0

Window {
  id: window
  title: "Pfadi"

  minimumHeight: index.implicitHeight
  minimumWidth: index.implicitWidth

  Component.onCompleted: {
    geomSettings.restore();
  }

  onClosing: {
    if( dash.sessionRunning ) {
      dash.abort()
      close.accepted = false
    }
  }

  WindowGeometrySettings {
    id: geomSettings
    win: window
    settings: dash.settings
  }

  MainPane {
    id: index
    anchors.fill: parent
  }

  Shortcut {
    sequence: StandardKey.Quit
    onActivated: window.close()
  }
  Shortcut {
    sequences: [StandardKey.FullScreen, "Ctrl+F", "F11"]
    onActivated: {
      if(window.visibility == Window.FullScreen) {
        window.showNormal()
      } else {
        window.showFullScreen()
      }
    }
  }
  Shortcut {
    sequences: [StandardKey.Cancel, "Pause"]
    onActivated: dash.actor.breakHard()
  }
}
