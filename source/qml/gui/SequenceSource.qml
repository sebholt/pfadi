import QtQml 2.11
import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4
import QtQuick.Dialogs 1.3
import QtQuick.Extras 1.4
import Qt.labs.settings 1.0
import SncQ 1.0

RowLayout {
  id: root
  spacing: 6

  function open_file_selection() {
    var folder = dash.settings.get("file_browser.directory")
    if ( folder ) {
      fileBrowser.folder = folder
    }

    stackLayout.currentIndex = 1
  }

  function close_file_selection() {
    stackLayout.currentIndex = 0
  }


  StackLayout {
    id: stackLayout
    GridLayout {
      columns: 2
      columnSpacing: 12

      Layout.alignment: Qt.AlignLeft | Qt.AlignTop

      HeadLabelV {
        id: fileLabel
        text: qsTr("File:")
        Layout.alignment: Qt.AlignLeft | Qt.AlignTop
      }
      Label {
        id: fileName

        property string filePath: dash.program.pathFilePath
        property bool filePathEmpty: ( filePath.length === 0 )
        property string clickText: qsTr("Click to select...")
        Layout.minimumWidth: fileLabel.implicitWidth
        Layout.preferredWidth: fileLabel.implicitWidth
        Layout.fillWidth: true
        elide: Text.ElideLeft
        text: filePathEmpty ? clickText : filePath

        MouseArea {
          id: fileNameMouseArea
          hoverEnabled: true
          cursorShape: Qt.PointingHandCursor
          anchors.fill: fileName
          onClicked: open_file_selection()
        }
      }

      HeadLabelV {
        text: qsTr("File statistics:")
        Layout.alignment: Qt.AlignLeft | Qt.AlignTop
      }
      GridLayout {
        columns: 2
        columnSpacing: 16

        HeadLabelV {
          text: qsTr("Size in bytes")
          Layout.alignment: Qt.AlignRight | Qt.AlignTop
        }
        HeadLabelV {
          text: qsTr("Valid")
          Layout.alignment: Qt.AlignLeft | Qt.AlignTop
        }

        NumberLabel {
          id: fileSizeLabel
          value: dash.program.pathFileSize
          valueMax: 10*1000*1000
          precision: 0
        }
        StatusLight {
          color: dash.program.pathFileValid ? "green" : "red"
          active: dash.program.pathFileOpened
          Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
        }
      }

      HeadLabelV {
        text: qsTr("Path length:")
        Layout.alignment: Qt.AlignLeft | Qt.AlignTop
      }
      RowLayout {
        NumberLabel {
          value: dash.program.pathStats.length
          precision: 2
        }
        Label { text: "mm" }
      }

      HeadLabelV {
        text: qsTr("Path boundings:")
        Layout.alignment: Qt.AlignLeft | Qt.AlignTop
      }
      SequenceMPathBoundings {
        pathStats: dash.program.pathStats
      }
    }

    ColumnLayout {
      FileBrowser {
        id: fileBrowser
        onFolderChanged: {
          dash.settings.set("file_browser.directory", folder)
        }
        onFileSelected: {
          dash.program.pathFile = fileUrl
          stackLayout.currentIndex = 0
        }
      }
      Button {
        text: "Cancel"
        visible: stackLayout.currentIndex == 1
        Layout.alignment: Qt.AlignLeft | Qt.AlignTop

        onClicked: close_file_selection()
      }    
    }
  }
}
