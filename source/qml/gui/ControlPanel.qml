import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4

Panel {
  id: root
  title: qsTr("Control")

  property var axes: ListModel {
    ListElement {
      axisName: "x"
      axisIndex: 0
    }
    ListElement {
      axisName: "y"
      axisIndex: 1
    }
    ListElement {
      axisName: "z"
      axisIndex: 2
    }
  }

  RowLayout {
    spacing: 16
    Layout.fillWidth: true
    
    ColumnLayout {
      spacing: 0
      Layout.alignment: Qt.AlignLeft | Qt.AlignTop
      Layout.fillWidth: false
      Button {
        text: "Align"
        checkable: true
        checked: true
        autoExclusive: true
        Layout.fillWidth: true
        onPressed: stackPages.currentIndex = 0
      }
      Button {
        text: "Move axes"
        checkable: true
        autoExclusive: true
        Layout.fillWidth: true
        onPressed: stackPages.currentIndex = 1
      }
      Button {
        text: "Position axes"
        checkable: true
        autoExclusive: true
        Layout.fillWidth: true
        onPressed: stackPages.currentIndex = 2
      }
      Button {
        text: "Controls"
        checkable: true
        autoExclusive: true
        Layout.fillWidth: true
        onPressed: stackPages.currentIndex = 3
      }
    }

    StackLayout {
      id: stackPages
      Layout.alignment: Qt.AlignLeft | Qt.AlignTop
      Layout.fillWidth: true

      ControlAlignAxes {
        axes: root.axes
      }
      ControlMoveAxes {
        axes: root.axes
      }
      ControlPositionAxes {
        axes: root.axes
      }
      ControlControls {

      }
    }
  }
}