import QtQml 2.11
import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4
import SncQ 1.0

PanelH {
  id: panel
  property var axis

  title: axis.info.name

  RowLayout {
    spacing: 0
    Layout.fillWidth: true

    GridLayout {
      columns: 2
      rowSpacing: 12
      columnSpacing: 36

      Layout.fillWidth: false

      RowLayout {
        Layout.fillWidth: true
        HeadLabelH { text: qsTr("Step") }
        Item { Layout.fillWidth: true }
        HeadLabelH { text: "mm" }
      }
      RowLayout {
        Layout.fillWidth: true
        HeadLabelH { text: "Speed" }
      }

      AxisStepButtons {
        axis: panel.axis
        valueTextPrecision: 3
        values: [axis.info.lengthPerStep, 0.01, 0.1, 1.0, 10.0]
        buttonComponent: Component { AxisStepButton{} }
        Layout.alignment: Qt.AlignLeft | Qt.AlignTop
      }
      AxisSpeedSliders {
        axis: panel.axis
        Layout.alignment: Qt.AlignLeft | Qt.AlignTop
      }
    }

    Item {
      Layout.fillWidth: true
    }
  }
}
