import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4

// -- Axes
ColumnLayout {

  // Controls bool
  GridLayout {
    flow: GridLayout.TopToBottom
    rows: dash.controls.i1.count

    Repeater {
      model: dash.controls.i1
      Label {
        text: listItem.name
      }
    }

    Repeater {
      model: dash.controls.i1
      Button {
        text: qsTr("On")
        enabled: listItem.available
        onPressed: listItem.setOn()
      }
    }

    Repeater {
      model:  dash.controls.i1
      Button {
        text: qsTr("Off")
        enabled: listItem.available
        onPressed: listItem.setOff()
      }
    }

    Repeater {
      model:  dash.controls.i1
      Button {
        text: qsTr("Toggle")
        enabled: listItem.available
        onPressed: listItem.toggle()
      }
    }
  }

}
