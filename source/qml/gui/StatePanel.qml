import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4
import QtQuick.Extras 1.4
import SncQ 1.0

// -- Sensors / Controls
RowLayout {
  spacing: panelHSpace

  Layout.alignment: Qt.AlignLeft | Qt.AlignTop
  Layout.fillWidth: true

  Panel {
    title: qsTr("Axes")

    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
    Layout.fillWidth: false
    Layout.minimumWidth: implicitWidth

    PanelStateAxes { 
      model: [dash.axisX, dash.axisY, dash.axisZ]
    }
  }
  Panel {
    title: qsTr("Sensors")

    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
    Layout.fillWidth: false
    Layout.minimumWidth: implicitWidth

    PanelStateSensorsI1 {
      model: dash.tracker.info.sensors.i1
    }
  }
  Panel {
    title: qsTr("Controls")
    
    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
    Layout.fillWidth: false
    Layout.minimumWidth: implicitWidth

    PanelStateControlsI1 {
      model: dash.tracker.info.controls.i1
    }
  }
  Item {
    Layout.fillWidth: true
  }
  Item {
    Layout.fillWidth: true
    Layout.fillHeight: true
    RoundButton {
      width: Math.min ( parent.width, parent.height )
      height: Math.min ( parent.width, parent.height )
      anchors.right: parent.right
      anchors.top: parent.top
      text: qsTr("Stop!")
      enabled: dash.actor.breakHardAvailable
      onClicked: dash.actor.breakHard()
    }
  }
}
