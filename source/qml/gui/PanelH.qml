import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4

Pane {
  id: panel

  default property alias childrenList: layContent.data
  property var title: "Panel title"

  contentWidth: contentRoot.implicitWidth
  contentHeight: contentRoot.implicitHeight
  padding: 0

  RowLayout {
    id: contentRoot
    spacing: 0

    anchors.fill: parent

    // -- Title
    Label {
      id: titleLabel
      text: panel.title
      font.bold: true
      font.pointSize: 12
      Layout.alignment: Qt.AlignLeft | Qt.AlignTop
      Layout.rightMargin: 8
      Layout.fillWidth: false
    }
    Rectangle {
      implicitHeight: 1
      implicitWidth: 1
      color: titleLabel.color
      Layout.alignment: Qt.AlignLeft | Qt.AlignTop
      Layout.fillHeight: true
      Layout.fillWidth: false
      Layout.rightMargin: 12
    }

    // -- Content
    ColumnLayout {
      id: layContent
      spacing: 16
      Layout.rightMargin: 12
      Layout.alignment: Qt.AlignLeft | Qt.AlignTop
    }
  }
}
