import QtQml 2.11
import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4
import SncQ 1.0

PanelH {
  id: panel
  property var axis

  title: axis.info.name

  RowLayout {
    spacing: 0
    Layout.fillWidth: true

    ColumnLayout {
      Layout.fillWidth: false
      Layout.fillHeight: true

      Repeater {
        model: 5
        Button {
          property int posIndex: (4 - index)
          property real posValue: posIndex / 4.0
          text: posIndex + "/4"
          enabled: lengthInput.enabled
          onClicked: {
            axis.position.goToLength(axis.info.length * posValue);
          }
        }
      }

      RowLayout {
        Layout.topMargin: 8
        Button {
          text: qsTr("Go to")
          enabled: axis && axis.position.available
          onClicked: {
            axis.position.goToLength(lengthInput.valueLatest);
          }
        }
        NumberInput {
          id: lengthInput

          Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
          Layout.fillWidth: false

          value: 0.0
          valueMax: axis ? axis.info.length : 0.0
          valueMin: 0.0
          precision: 3
          enabled: axis && axis.position.available
        }
      }
    }

    Item {
      Layout.fillWidth: true
    }
  }
}
