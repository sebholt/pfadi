import QtQml 2.11
import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4
import QtQuick.Extras 1.4
import SncQ 1.0
import rtc 1.0

ColumnLayout {
  id: root
  
  function timeString(time) {
    return time.toLocaleString(locale, "hh:mm:ss")
  }

  function durationString(time) {
    return time.toLocaleString(locale, "hh:mm:ss:zzz")
  }

  function statusString(status) {
    switch (status) {
      case SequenceSection.NONE:
        return qsTr("-")
      case SequenceSection.SCHEDULED:
        return qsTr("Scheduled")
      case SequenceSection.SKIPPED:
        return qsTr("Skipped")
      case SequenceSection.PREPARING:
        return qsTr("Preparing")
      case SequenceSection.RUNNING:
        return qsTr("Running")
      case SequenceSection.FINISHED:
        return qsTr("Finished")
      case SequenceSection.ABORTING:
        return qsTr("Aborting")
      case SequenceSection.ABORTED:
        return qsTr("Aborted")
    }
    return ""
  }

  GridLayout {
    columnSpacing: 16
    flow: GridLayout.TopToBottom
    rows: 4

    HeadLabelH { text: qsTr("Section") }
    Repeater {
      model: dash.program.sequence
      delegate: Label { text: sectionObject.name }
    }

    StackLayout {
      Layout.fillWidth: false
      Layout.fillHeight: false

      HeadLabelH { text: qsTr("Status") }
      Label { text: statusString(SequenceSection.SCHEDULED) }
      Label { text: statusString(SequenceSection.SKIPPED) }
      Label { text: statusString(SequenceSection.PREPARING) }
      Label { text: statusString(SequenceSection.RUNNING) }
      Label { text: statusString(SequenceSection.FINISHED) }
      Label { text: statusString(SequenceSection.ABORTING) }
      Label { text: statusString(SequenceSection.ABORTED) }
    }
    Repeater {
      model: dash.program.sequence
      delegate: Label { text: statusString(sectionObject.status) }
    }

    HeadLabelH { text: qsTr("Begin") }
    Repeater {
      model: dash.program.sequence
      delegate: Label {
        text: {
          if ( !sectionObject.started ) {
            return "-"
          }
          return timeString ( sectionObject.beginTime )
        }
      }
    }

    HeadLabelH { text: qsTr("End") }
    Repeater {
      model: dash.program.sequence
      delegate: Label {
        text: {
          if ( !sectionObject.finished ) {
            return "-"
          }
          return timeString ( sectionObject.endTime )
        }
      }
    }

    HeadLabelH { text: qsTr("Duration") }
    Repeater {
      model: dash.program.sequence
      delegate: Label {
        text: {
          if ( !sectionObject.started ) {
            return "-"
          }
          return durationString ( sectionObject.duration )
        }
      }
    }

  }
}
