import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4
import QtQuick.Extras 1.4
import SncQ 1.0

RowLayout {
  spacing: 24
  
  SequenceProgressStatus {
    enabled: dash.program.routing
    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
    Layout.fillWidth: true
  }

  Item {
    Layout.fillWidth: true
    Layout.fillHeight: true
  }

  SequenceProgressChecks {
    visible: !dash.program.routing
    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
    Layout.fillWidth: false
  }

  ColumnLayout {
    spacing: 16

    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
    Layout.fillWidth: false

    Button {
      text: qsTr("Start")
      visible: !dash.program.routing
      enabled: !dash.program.routing && dash.program.startable
      implicitHeight: implicitWidth
      Layout.alignment: Qt.AlignRight | Qt.AlignTop
      onClicked: dash.program.routeBegin()
    }

    Button {
      text: qsTr("Abort")
      visible: dash.program.routing
      implicitHeight: implicitWidth
      Layout.alignment: Qt.AlignRight | Qt.AlignTop
      onClicked: dash.program.routeAbort()
    }

  }
}

