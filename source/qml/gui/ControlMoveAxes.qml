import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4

// -- Axes
RowLayout {
  id: root

  property var axes: ListModel {}

  spacing: panelHSpace

  Layout.alignment: Qt.AlignLeft | Qt.AlignTop
  Layout.fillHeight: true

  // Axes
  Repeater {
    model: root.axes
    ControlMoveAxis {
      axis: dash.getAxis(axisIndex)
      Layout.alignment: Qt.AlignLeft | Qt.AlignTop
      Layout.fillWidth: true
      Layout.fillHeight: true
      Layout.minimumWidth: implicitWidth
    }
  }
}
