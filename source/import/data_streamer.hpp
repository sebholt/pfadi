/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <sev/logt/context.hpp>
#include <snc/mpath_stream/write.hpp>

namespace import
{

/// @brief Abstract base class for parsers that convert raw data into MPath
///        elements.
///
class Data_Streamer
{
  public:
  // -- Types
  using Stream = snc::mpath_stream::Write;

  // -- Construction

  Data_Streamer ( sev::logt::Context log_n );

  virtual ~Data_Streamer ();

  // -- State

  bool
  good ()
  {
    return _good;
  }

  const std::string &
  error_message () const
  {
    return _error_message;
  }

  // -- Interface

  void
  begin ();

  virtual void
  feed ( const char * data_n, std::size_t size_n, Stream * stream_n ) = 0;

  virtual void
  feed_end ( Stream * stream_n ) = 0;

  virtual void
  end ();

  protected:
  // -- Utility

  const sev::logt::Context &
  log () const
  {
    return _log;
  }

  void
  feed_end_good ();

  void
  feed_end_error ( std::string error_n );

  private:
  /// @brief Logging context
  sev::logt::Context _log;
  std::string _error_message;
  bool _good = false;
};

} // namespace import
