/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <import/config.hpp>
#include <import/gcode/codes/g.hpp>
#include <import/gcode/codes/m.hpp>
#include <import/gcode/elements.hpp>
#include <sev/lag/vector3.hpp>
#include <sev/logt/context.hpp>
#include <sev/mem/ring_static.hpp>
#include <snc/mpath/curve_evaluator_buffer_d.hpp>
#include <snc/mpath/d3/types.hpp>
#include <snc/mpath/element_buffer_d.hpp>
#include <snc/mpath/speed_database.hpp>
#include <array>
#include <string>
#include <tuple>
#include <utility>

namespace import::gcode
{

/// @brief Maps mpml elements into mpath elements
///
class Mapper
{
  public:
  // -- Types

  /// @brief Public state token
  enum class Token
  {
    NONE,    /// Feed a gcode element
    ERROR,   /// @brief Error locked
    ELEMENT, /// @brief MPath element ready
    DONE     /// @brief No more elements
  };

  // -- Construction

  Mapper ( const sev::logt::Reference & log_parent_n );

  // -- MPath configuration

  const import::Config &
  config () const
  {
    return _config;
  }

  // -- Mapping state

  bool
  is_mapping () const
  {
    return _active;
  }

  /// @brief Current mpath element
  const snc::mpath::Element *
  element () const
  {
    return &( _mpath_rbuffer.front ().elem () );
  }

  /// @brief Signalizes that there was an error
  bool
  error () const
  {
    return !_error_message.empty ();
  }

  /// @brief Error text
  const std::string &
  error_message () const
  {
    return _error_message;
  }

  // -- Mapping control

  void
  begin ( const import::Config & config_n );

  /// @brief Call when no more elemnts are available
  void
  reset ();

  void
  feed ( const Element & elem_n );

  void
  feed_end ();

  Token
  read ();

  void
  element_pop ()
  {
    _mpath_rbuffer.pop_front_not_empty ();
  }

  private:
  // -- Utility types

  struct Vec_Reg
  {
    Vec_Reg () { reset (); }

    void
    reset ()
    {
      vec.fill ( 0.0 );
      valid.fill ( false );
    }

    bool
    all_valid () const
    {
      for ( bool val : valid ) {
        if ( !val ) {
          return false;
        }
      }
      return true;
    }

    sev::lag::Vector3d vec;
    std::array< bool, 3 > valid;
  };

  // -- Utility methods
  template < class CODES >
  void
  init_codes ( CODES & codes_n );

  void
  feed_comment ( const elem::Comment & elem_n );

  void
  feed_code ( const elem::Code & elem_n );

  template < typename CODES >
  void
  feed_code_action ( const elem::Code & elem_n, CODES & codes_n );

  void
  feed_code_parameter ( const elem::Code & elem_n );

  void
  flush_action_code ();

  void
  raise_error ( std::string_view error_n );

  template < class T, typename... Args >
  T &
  mpath_emplace ( Args &&... args_n );

  void
  mpath_emplace_speed ( const snc::mpath::Speed & speed_n );

  void
  mpath_emplace_speed ( const codes::Parameter & par_n,
                        snc::mpath::Speed_Regime speed_regime_n );

  void
  set_pos ( Vec_Reg & vreg_n,
            std::size_t axis_n,
            const codes::Parameter & par_n );

  double
  get_speed ( double value_n );

  template < class C >
  void
  code_begin ();

  template < class C >
  void
  code_flush ();

  void
  code_flush_linear ( const codes::G_Linear & gc_n,
                      snc::mpath::Speed_Regime speed_default_n );

  void
  code_flush_arc ( const codes::G_Arc & gc_n, bool cw_n );

  private:
  sev::logt::Context _log;

  bool _active = false;
  bool _element_feed = false;
  import::Config _config;
  std::string _error_message;

  /// @brief mpath element construction buffer
  sev::mem::Ring_Static< snc::mpath::Element_Buffer_D< 3 >, 4 > _mpath_rbuffer;

  struct Curve
  {
    Curve () { reset (); }

    void
    reset ()
    {
      inside = false;
      speed_emitted = false;
      speed.reset ();
      pos.reset ();
    }

    // @brief Flags if the last element was a curve element
    bool inside = false;
    bool speed_emitted = false;
    snc::mpath::Speed speed;
    /// @brief Current position tracker
    Vec_Reg pos;
  } _curve;

  codes::Codes< codes::G00,
                codes::G01,
                codes::G02,
                codes::G03,
                codes::G17,
                codes::G21,
                codes::G40,
                codes::G43,
                codes::G49,
                codes::G54,
                codes::G90,
                codes::G80 >
      _g_codes;

  codes::Codes< codes::M02, codes::M03, codes::M05, codes::M06, codes::M09 >
      _m_codes;

  codes::Code_Ref _action_code;
};

} // namespace import::gcode
