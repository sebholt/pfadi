/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <import/gcode/codes/abstract.hpp>

namespace import::gcode::codes
{

class G_Code : public Action_Code
{
  public:
  // -- Construction

  G_Code ( double number_n )
  : Action_Code ( 'G', number_n )
  {
  }
};

template < std::size_t NUMBER >
class G_Code_T : public G_Code
{
  public:
  // -- Construction

  G_Code_T ()
  : G_Code ( NUMBER )
  {
  }
};

template < std::size_t NUM_PAR >
class G_Code_Pars : public Action_Code_Pars< NUM_PAR >
{
  public:
  // -- Construction

  template < typename... Args >
  G_Code_Pars ( double number_n, Args... args_n )
  : Action_Code_Pars< NUM_PAR > (
        'G', number_n, std::forward< Args > ( args_n )... )
  {
  }
};

class G_Linear : public G_Code_Pars< 4 >
{
  public:
  G_Linear ( double number_n )
  : G_Code_Pars< 4 > ( number_n, 'X', 'Y', 'Z', 'F' )
  {
  }

  const Parameter &
  X () const
  {
    return par ( 0 );
  }

  const Parameter &
  Y () const
  {
    return par ( 1 );
  }

  const Parameter &
  Z () const
  {
    return par ( 2 );
  }

  const Parameter &
  F () const
  {
    return par ( 3 );
  }
};

class G_Arc : public G_Code_Pars< 8 >
{
  public:
  G_Arc ( double number_n )
  : G_Code_Pars< 8 > ( number_n, 'X', 'Y', 'Z', 'I', 'J', 'K', 'R', 'F' )
  {
  }

  const Parameter &
  X () const
  {
    return par ( 0 );
  }

  const Parameter &
  Y () const
  {
    return par ( 1 );
  }

  const Parameter &
  Z () const
  {
    return par ( 2 );
  }

  const Parameter &
  I () const
  {
    return par ( 3 );
  }

  const Parameter &
  J () const
  {
    return par ( 4 );
  }

  const Parameter &
  K () const
  {
    return par ( 5 );
  }

  const Parameter &
  R () const
  {
    return par ( 6 );
  }

  const Parameter &
  F () const
  {
    return par ( 7 );
  }
};

class G00 : public G_Linear
{
  public:
  G00 ()
  : G_Linear ( 0 )
  {
  }
};

class G01 : public G_Linear
{
  public:
  G01 ()
  : G_Linear ( 1 )
  {
  }
};

class G02 : public G_Arc
{
  public:
  G02 ()
  : G_Arc ( 2 )
  {
  }
};

class G03 : public G_Arc
{
  public:
  G03 ()
  : G_Arc ( 3 )
  {
  }
};

class G17 : public G_Code_T< 17 >
{
};

class G21 : public G_Code_T< 21 >
{
};

class G40 : public G_Code_T< 40 >
{
};

class G43 : public G_Code_Pars< 1 >
{
  public:
  G43 ()
  : G_Code_Pars< 1 > ( 43, 'H' )
  {
  }
};

class G49 : public G_Code_T< 49 >
{
};

class G54 : public G_Code_T< 54 >
{
};

class G80 : public G_Code_T< 80 >
{
};

class G90 : public G_Code_T< 90 >
{
};

} // namespace import::gcode::codes
