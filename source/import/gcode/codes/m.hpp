/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <import/gcode/codes/abstract.hpp>

namespace import::gcode::codes
{

class M_Code : public Action_Code
{
  public:
  // -- Construction

  M_Code ( double number_n )
  : Action_Code ( 'M', number_n )
  {
  }
};

template < std::size_t NUMBER >
class M_Code_T : public G_Code
{
  public:
  // -- Construction

  M_Code_T ()
  : G_Code ( NUMBER )
  {
  }
};

template < std::size_t NUM_PAR >
class M_Code_Pars : public Action_Code_Pars< NUM_PAR >
{
  public:
  // -- Construction

  template < typename... Args >
  M_Code_Pars ( double number_n, Args... args_n )
  : Action_Code_Pars< NUM_PAR > (
        'M', number_n, std::forward< Args > ( args_n )... )
  {
  }
};

class M02 : public M_Code_T< 2 >
{
};

class M03 : public M_Code_Pars< 1 >
{
  public:
  M03 ()
  : M_Code_Pars< 1 > ( 3, 'S' )
  {
  }

  Parameter &
  S ()
  {
    return par ( 0 );
  }
};

class M05 : public M_Code_T< 5 >
{
};

class M06 : public M_Code_Pars< 1 >
{
  public:
  M06 ()
  : M_Code_Pars< 1 > ( 6, 'T' )
  {
  }

  Parameter &
  T ()
  {
    return par ( 0 );
  }
};

class M09 : public M_Code_T< 9 >
{
};

} // namespace import::gcode::codes
