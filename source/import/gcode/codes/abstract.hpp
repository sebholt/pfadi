/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <sev/mem/view.hpp>
#include <array>
#include <functional>

namespace import::gcode::codes
{

/// @brief Parameter code
class Parameter
{
  public:
  // -- Construction and reset

  constexpr Parameter ( char letter_n )
  : _letter ( letter_n )
  {
  }

  // -- Identification

  constexpr char
  letter () const
  {
    return _letter;
  }

  // -- Reset

  void
  reset ()
  {
    _valid = false;
    _value = 0.0;
  }

  // -- Valid

  bool
  valid () const
  {
    return _valid;
  }

  // -- Value

  double
  value () const
  {
    return _value;
  }

  void
  set_value ( double val_n )
  {
    _valid = true;
    _value = val_n;
  }

  private:
  const char _letter;
  bool _valid = false;
  double _value = 0.0;
};

class Action_Code
{
  protected:
  // -- Construction

  Action_Code ( char letter_n, double number_n )
  : _letter ( letter_n )
  , _number ( number_n )
  {
  }

  virtual ~Action_Code () = default;

  public:
  // -- Identification

  char
  letter () const
  {
    return _letter;
  }

  double
  number () const
  {
    return _number;
  }

  // -- Begin function

  const std::function< void () > &
  begin () const
  {
    return _begin;
  }

  void
  set_begin ( std::function< void () > func_n )
  {
    _begin = std::move ( func_n );
  }

  // -- Flush function

  const std::function< void () > &
  flush () const
  {
    return _flush;
  }

  void
  set_flush ( std::function< void () > func_n )
  {
    _flush = std::move ( func_n );
  }

  // -- Abstract interface

  virtual void
  reset ()
  {
  }

  virtual Parameter *
  find_par ( char letter_n [[maybe_unused]] )
  {
    return nullptr;
  }

  private:
  const char _letter = 0;
  const double _number = -1.0;
  std::function< void () > _begin;
  std::function< void () > _flush;
};

template < std::size_t NUM_PAR >
class Action_Code_Pars : public Action_Code
{
  public:
  // -- Construction

  template < typename... Args >
  Action_Code_Pars ( char letter_n, double number_n, Args... args_n )
  : Action_Code ( letter_n, number_n )
  , _pars{ { std::forward< Args > ( args_n )... } }
  {
  }

  // -- Parameters

  constexpr Parameter &
  par ( std::size_t index_n )
  {
    return _pars[ index_n ];
  }

  constexpr const Parameter &
  par ( std::size_t index_n ) const
  {
    return _pars[ index_n ];
  }

  // -- Abstract interface

  void
  reset () override
  {
    Action_Code::reset ();
    for ( Parameter & par : _pars ) {
      par.reset ();
    }
  }

  Parameter *
  find_par ( char letter_n ) override
  {
    for ( Parameter & par : _pars ) {
      if ( par.letter () == letter_n ) {
        return &par;
      }
    }
    return nullptr;
  }

  private:
  std::array< Parameter, NUM_PAR > _pars;
};

struct Code_Ref
{
  bool
  valid () const
  {
    return ( code != nullptr );
  }

  void
  reset ()
  {
    number = 0.0;
    code = nullptr;
  }

  double number = 0.0;
  codes::Action_Code * code = nullptr;
};

template < typename... Args >
class Codes
{
  public:
  // -- Types
  using Tuple = std::tuple< Args... >;
  using List = std::array< Code_Ref, std::tuple_size< Tuple >::value >;

  // -- Construction

  Codes ()
  {
    // Write list pointers
    std::apply (
        [ this ] ( auto &&... code_n ) {
          std::size_t ii = 0;
          ( ( list[ ii++ ].code = &code_n ), ... );
        },
        tuple );

    // Write list numbers
    for ( auto & chandle : list ) {
      chandle.number = chandle.code->number ();
    }
  }

  template < typename T >
  T &
  get ()
  {
    return std::get< T > ( tuple );
  }

  template < typename T >
  const T &
  get () const
  {
    return std::get< const T > ( tuple );
  }

  Tuple tuple;
  List list;
};

} // namespace import::gcode::codes
