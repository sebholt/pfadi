/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <import/gcode/elements.hpp>
#include <sev/lag/vector3.hpp>
#include <sev/logt/context.hpp>
#include <cstdint>
#include <string>

namespace import::gcode
{

/// @brief Reads a MPML file
///
class Parser
{
  public:
  // -- Types
  /// @brief Public state token
  enum class Token
  {
    NONE,    /// @brief No token found, feed data
    ELEMENT, /// @brief Comment string
    ERROR,   /// @brief Parse error
    DONE     /// @brief Parsing end
  };

  struct PRes
  {
    bool consumed = false;
    Token token = Token::NONE;
  };

  // -- Construction

  Parser ( const sev::logt::Reference & log_parent_n );

  // -- Parse state

  /// @brief true between begin() and reset()
  bool
  is_parsing () const
  {
    return ( _pfunc != nullptr );
  }

  const Element *
  element () const
  {
    return _element;
  }

  /// @brief Signalizes that there was an error
  bool
  error () const
  {
    return !_error_message.empty ();
  }

  /// @brief Error text
  const std::string &
  error_message () const
  {
    return _error_message;
  }

  /// @brief Line number of the last event
  std::uint_fast64_t
  line () const
  {
    return _line;
  }

  /// @brief Column number of the last event
  std::uint_fast64_t
  column () const
  {
    return _column;
  }

  // -- Parse control

  void
  begin ();

  void
  reset ();

  void
  feed ( const char * data_n, std::size_t size_n );

  void
  feed_end ();

  Token
  read ();

  private:
  Token
  parse_data ();

  Token
  parse_end ();

  // -- Utility

  using PFunc = PRes ( * ) ( Parser & p_n, char c_n );

  static void
  pu_col ( Parser & p_n );

  static void
  pu_cr ( Parser & p_n );

  static void
  pu_nl ( Parser & p_n );

  static PRes
  pu_err ( Parser & p_n, std::string message_n );

  static PRes
  p_err ( Parser & p_n, char c_n );

  static PRes
  p_root ( Parser & p_n, char c_n );

  static PRes
  p_skip_line ( Parser & p_n, char c_n );

  static PRes
  p_comment_line ( Parser & p_n, char c_n );

  static PRes
  pend_comment_line ( Parser & p_n );

  static PRes
  p_comment_parantheses ( Parser & p_n, char c_n );

  static PRes
  p_code_value ( Parser & p_n, char c_n );

  static PRes
  pend_code_value ( Parser & p_n );

  private:
  // -- Attributes
  /// @brief Logging context
  sev::logt::Context _log;

  bool _data_feed = false;
  const char * _data_cur = nullptr;
  const char * _data_end = nullptr;

  PFunc _pfunc = nullptr;

  std::uint_fast64_t _line = 0;
  std::uint_fast64_t _column = 0;

  std::string _code_buffer;
  std::string _comment_buffer;
  std::string _error_message;

  Element _elem_base;
  elem::Code _elem_code;
  elem::Comment _elem_comment;
  Element * _element = &_elem_base;
};

} // namespace import::gcode
