/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#include "streamer.hpp"

namespace import::gcode
{

Streamer::Streamer ( const sev::logt::Reference & log_parent_n )
: Super ( { log_parent_n, "GCode-Streamer" } )
, _parser ( log () )
, _mapper ( log () )
{
}

void
Streamer::begin ( const import::Config & config_n )
{
  Super::begin ();
  _parser.begin ();
  _mapper.begin ( config_n );
}

void
Streamer::end ()
{
  _parser.reset ();
  _mapper.reset ();
  Super::end ();
}

void
Streamer::feed ( const char * data_n, std::size_t size_n, Stream * stream_n )
{
  log ().cat ( sev::logt::FL_DEBUG, "feed: ", size_n, " bytes" );
  _parser.feed ( data_n, size_n );
  while ( parse ( stream_n ) ) {
  };
}

void
Streamer::feed_end ( Stream * stream_n )
{
  _parser.feed_end ();
  while ( parse ( stream_n ) ) {
  };
}

bool
Streamer::parse ( Stream * stream_n )
{
  using Token = Parser::Token;
  switch ( _parser.read () ) {
  case Token::NONE:
    // More data needed
    return false;

  case Token::ERROR:
    // Register error
    feed_end_error (
        sev::string::cat ( "GCode parse error: ", _parser.error_message () ) );
    return false;

  case Token::ELEMENT:
    // Feed mapper from parser
    _mapper.feed ( *_parser.element () );
    return map ( stream_n );

  case Token::DONE:
    _mapper.feed_end ();
    return map ( stream_n );
  }

  return false;
}

bool
Streamer::map ( Stream * stream_n )
{
  while ( true ) {
    switch ( _mapper.read () ) {
    case Mapper::Token::NONE:
      // More elements needed
      return true;

    case Mapper::Token::ERROR:
      feed_end_error ( sev::string::cat ( "GCode mapper error: ",
                                          _mapper.error_message () ) );
      return false;

    case Mapper::Token::ELEMENT:
      // Write element to stream and read again
      stream_n->write ( *_mapper.element () );
      _mapper.element_pop ();
      break;

    case Mapper::Token::DONE:
      feed_end_good ();
      return false;
    }
  }

  return false;
}

} // namespace import::gcode
