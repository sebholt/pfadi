/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <import/gcode/elem/code.hpp>
#include <import/gcode/elem/comment.hpp>
