/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#include "mapper.hpp"
#include <sev/assert.hpp>
#include <sev/lag/math.hpp>
#include <sev/string/utility.hpp>
#include <snc/mpath/speed_database.hpp>
#include <algorithm>
#include <type_traits>

namespace import::gcode
{

Mapper::Mapper ( const sev::logt::Reference & log_parent_n )
: _log ( log_parent_n, "Mapper" )
{
  init_codes ( _g_codes );
  init_codes ( _m_codes );

  // Checks
  DEBUG_ASSERT ( _g_codes.list[ 0 ].code == &std::get< 0 > ( _g_codes.tuple ) );
  DEBUG_ASSERT ( _g_codes.list[ 1 ].code == &std::get< 1 > ( _g_codes.tuple ) );

  DEBUG_ASSERT ( _m_codes.list[ 0 ].code == &std::get< 0 > ( _m_codes.tuple ) );
  DEBUG_ASSERT ( _m_codes.list[ 1 ].code == &std::get< 1 > ( _m_codes.tuple ) );
}

void
Mapper::begin ( const import::Config & config_n )
{
  reset ();

  _config = config_n;

  _active = true;
  _element_feed = true;
}

void
Mapper::reset ()
{
  if ( !is_mapping () ) {
    return;
  }

  _active = false;
  _element_feed = false;
  _error_message.clear ();
  _error_message.shrink_to_fit ();

  _mpath_rbuffer.clear ();

  _curve.reset ();

  _action_code.reset ();
}

Mapper::Token
Mapper::read ()
{
  DEBUG_ASSERT ( is_mapping () );

  if ( !_error_message.empty () ) {
    return Token::ERROR;
  }
  if ( !_mpath_rbuffer.is_empty () ) {
    return Token::ELEMENT;
  }
  if ( _element_feed ) {
    return Token::NONE;
  }
  return Token::DONE;
}

void
Mapper::feed ( const Element & elem_n )
{
  DEBUG_ASSERT ( is_mapping () );

  switch ( elem_n.type () ) {
  case Element_Type::INVALID:
    break;
  case Element_Type::COMMENT:
    feed_comment ( static_cast< const elem::Comment & > ( elem_n ) );
    break;
  case Element_Type::CODE:
    feed_code ( static_cast< const elem::Code & > ( elem_n ) );
    break;
  }
}

void
Mapper::feed_end ()
{
  DEBUG_ASSERT ( is_mapping () );

  // No more elements coming
  _element_feed = false;

  // Flush a pending action code
  flush_action_code ();

  // Finish with stream end element
  {
    snc::mpath::elem::Stream_End * elem;
    _mpath_rbuffer.emplace_back_not_full ( &elem );
  }
}

void
Mapper::feed_comment ( const elem::Comment & elem_n [[maybe_unused]] )
{
  //_log.cat (
  //    sev::logt::FL_DEBUG_0, "feed: Comment: ", elem_n.text () );
}

void
Mapper::feed_code ( const elem::Code & elem_n )
{
  switch ( elem_n.letter () ) {
  case 'G':
    feed_code_action ( elem_n, _g_codes );
    break;
  case 'M':
    feed_code_action ( elem_n, _m_codes );
    break;
  default:
    feed_code_parameter ( elem_n );
    break;
  }
}

template < typename CODES >
void
Mapper::feed_code_action ( const elem::Code & elem_n, CODES & codes_n )
{
  // if ( auto logo = _log.ostr ( sev::logt::FL_DEBUG_0 ) ) {
  //  logo << "feed: Action: " << elem_n.letter () << " " << elem_n.value ();
  //}

  // Flush previous code
  flush_action_code ();
  if ( error () ) {
    return;
  }

  // Find code
  for ( auto & chandle : codes_n.list ) {
    if ( chandle.number == elem_n.value () ) {
      _action_code = chandle;
      break;
    }
  }

  if ( _action_code.valid () ) {
    // Begin code
    _action_code.code->begin () ();
    if ( error () ) {
      _action_code.reset ();
    }
  } else {
    // Handle not found
    raise_error ( sev::string::cat (
        "Code ", elem_n.letter (), elem_n.value (), " is not supported." ) );
  }
}

void
Mapper::feed_code_parameter ( const elem::Code & elem_n )
{
  // if ( auto logo = _log.ostr ( sev::logt::FL_DEBUG_0 ) ) {
  //  logo << "feed: Parameter: " << elem_n.letter () << " " << elem_n.value ();
  //}

  if ( !_action_code.valid () ) {
    raise_error ( sev::string::cat (
        "No active code for parameter ", elem_n.letter (), elem_n.value () ) );
    return;
  }

  auto & acode = *_action_code.code;
  auto * par = acode.find_par ( elem_n.letter () );
  if ( par != nullptr ) {
    // Set parameter value
    par->set_value ( elem_n.value () );
    return;
  }

  // Unknown parameter
  raise_error ( sev::string::cat ( "Parameter ",
                                   elem_n.letter (),
                                   elem_n.value (),
                                   " for code ",
                                   acode.letter (),
                                   acode.number (),
                                   " not supported" ) );
}

void
Mapper::flush_action_code ()
{
  if ( !_action_code.valid () ) {
    return;
  }

  // if ( auto logo = _log.ostr ( sev::logt::FL_DEBUG_0 ) ) {
  //  logo << "flush action: " << _action_code.code->letter () << " "
  //       << _action_code.code->number ();
  //}

  // Call action code flush method
  _action_code.code->flush () ();
  _action_code.code->reset ();
  // Clear active action code
  _action_code.reset ();
}

void
Mapper::raise_error ( std::string_view err_n )
{
  // Fill error information
  if ( err_n.empty () ) {
    err_n = "Error";
  }
  if ( !_error_message.empty () ) {
    // Ensure new line
    constexpr char eol = '\n';
    if ( _error_message.back () != eol ) {
      _error_message.push_back ( eol );
    }
  }
  _error_message.append ( err_n );

  //_log.cat ( sev::logt::FL_DEBUG_0, err_n );
}

template < class T, typename... Args >
T &
Mapper::mpath_emplace ( Args &&... args_n )
{
  // Append curve end elements automatically
  bool is_curve = snc::mpath::d3::elem::type_is_curve ( T::class_elem_type ) ||
                  ( T::class_elem_type == snc::mpath::elem::Type::SPEED );
  if ( is_curve != _curve.inside ) {
    if ( is_curve ) {
      // A curve starts
      _curve.inside = true;
    } else {
      // A curve sequence ends
      _curve.inside = false;
      // Insert curve finish element
      snc::mpath::elem::Stream_Curve_Finish * elem;
      _mpath_rbuffer.emplace_back_not_full ( &elem );
    }
  }

  T * res = nullptr;
  _mpath_rbuffer.emplace_back_not_full ( &res,
                                         std::forward< Args > ( args_n )... );
  return *res;
}

void
Mapper::mpath_emplace_speed ( const snc::mpath::Speed & speed_n )
{
  if ( _curve.speed_emitted && ( _curve.speed == speed_n ) ) {
    // No speed change
    return;
  }
  _curve.speed_emitted = true;
  _curve.speed = speed_n;
  mpath_emplace< snc::mpath::elem::Speed > ( speed_n );
}

void
Mapper::mpath_emplace_speed ( const codes::Parameter & par_n,
                              snc::mpath::Speed_Regime speed_regime_n )
{
  snc::mpath::Speed speed;
  if ( par_n.valid () ) {
    speed.set_value ( get_speed ( par_n.value () ) );
  } else {
    speed.set_regime ( speed_regime_n );
  }
  mpath_emplace_speed ( speed );
}

void
Mapper::set_pos ( Vec_Reg & vreg_n,
                  std::size_t axis_n,
                  const codes::Parameter & par_n )
{
  if ( !par_n.valid () ) {
    return;
  }
  vreg_n.valid[ axis_n ] = true;
  // TODO: Mapping
  vreg_n.vec[ axis_n ] = par_n.value ();
}

double
Mapper::get_speed ( double value_n )
{
  // GCode speeds are in mm/min or inch/min
  return ( value_n / 60.0 );
}

template < class C >
void
Mapper::code_begin ()
{
}

template < class C >
void
Mapper::code_flush ()
{
}

void
Mapper::code_flush_linear ( const codes::G_Linear & gc_n,
                            snc::mpath::Speed_Regime speed_default_n )
{
  Vec_Reg target = _curve.pos;
  set_pos ( target, 0, gc_n.X () );
  set_pos ( target, 1, gc_n.Y () );
  set_pos ( target, 2, gc_n.Z () );
  _curve.pos = target;
  if ( !target.all_valid () ) {
    // Coordinate missing
    return;
  }
  // Speed
  mpath_emplace_speed ( gc_n.F (), speed_default_n );
  // Curve
  mpath_emplace< snc::mpath::d3::elem::Curve_Linear > ( target.vec );
}

template <>
void
Mapper::code_flush< codes::G00 > ()
{
  code_flush_linear ( _g_codes.get< codes::G00 > (),
                      snc::mpath::Speed_Regime::RAPID );
}

template <>
void
Mapper::code_flush< codes::G01 > ()
{
  code_flush_linear ( _g_codes.get< codes::G01 > (),
                      snc::mpath::Speed_Regime::MATERIAL );
}

void
Mapper::code_flush_arc ( const codes::G_Arc & gc_n, bool cw_n )
{
  if ( !_curve.pos.all_valid () ) {
    raise_error ( "Arc requires a valid position" );
    return;
  }

  // Target
  Vec_Reg target = _curve.pos;
  set_pos ( target, 0, gc_n.X () );
  set_pos ( target, 1, gc_n.Y () );
  set_pos ( target, 2, gc_n.Z () );
  if ( !target.all_valid () ) {
    raise_error ( "Incomplete arc target" );
    return;
  }
  if ( target.vec[ 2 ] != _curve.pos.vec[ 2 ] ) {
    raise_error ( "Arc target z missmatch" );
    return;
  }

  // Center
  if ( gc_n.R ().valid () ) {
    raise_error ( "Arc radius not supported" );
    return;
  }

  Vec_Reg center_delta;
  set_pos ( center_delta, 0, gc_n.I () );
  set_pos ( center_delta, 1, gc_n.J () );
  set_pos ( center_delta, 2, gc_n.K () );

  if ( center_delta.vec[ 2 ] != 0.0 ) {
    raise_error ( "Arc z missmatch" );
    return;
  }

  // Compute
  using V2d = sev::lag::Vector2d;
  const V2d v2_begin{ { _curve.pos.vec[ 0 ], _curve.pos.vec[ 1 ] } };
  const V2d v2_target{ { target.vec[ 0 ], target.vec[ 1 ] } };
  V2d v2_san_cd;
  V2d v2_center;
  double angle;
  {
    const V2d v2_cd{ { center_delta.vec[ 0 ], center_delta.vec[ 1 ] } };
    const V2d v2_delta = ( v2_target - v2_begin );
    const double dist = sev::lag::magnitude ( v2_delta );
    if ( dist < 1.0e-6 ) {
      // Full circle
      v2_center = v2_begin + v2_cd;
      angle = cw_n ? -sev::lag::two_pi_d : sev::lag::two_pi_d;
    } else {
      // Arc
      const V2d v2_dir = v2_delta / dist;
      const V2d v2_normal = sev::lag::rotated_90 ( v2_dir, 1 );
      // Distance of center from connection line
      const double cdist = sev::lag::dot_prod ( v2_cd, v2_normal );
      // Sanitized center (delta)
      const V2d v2_san_cd = ( v2_delta * 0.5 ) + v2_normal * cdist;
      // Compute center position
      v2_center = v2_begin + v2_san_cd;
      // Compute angle
      const double alpha = std::acos (
          sev::lag::dot_prod ( v2_dir, sev::lag::normalized ( v2_san_cd ) ) );
      if ( cw_n == ( cdist < 0.0 ) ) {
        // Small angle
        angle = sev::lag::pi_d - 2.0 * alpha;
      } else {
        // Large angle
        angle = sev::lag::pi_d + 2.0 * alpha;
      }
      if ( cw_n ) {
        angle = -angle;
      }
    }
  }

  // Speed
  mpath_emplace_speed ( gc_n.F (), snc::mpath::Speed_Regime::MATERIAL );
  // Curve
  {
    auto & melem = mpath_emplace< snc::mpath::d3::elem::Curve_Circle > ();
    melem.set_pos_center ( v2_center );
    melem.set_angle_radians ( angle );
  }

  _curve.pos = target;
}

template <>
void
Mapper::code_flush< codes::G02 > ()
{
  code_flush_arc ( _g_codes.get< codes::G02 > (), true );
}

template <>
void
Mapper::code_flush< codes::G03 > ()
{
  code_flush_arc ( _g_codes.get< codes::G03 > (), false );
}

template <>
void
Mapper::code_flush< codes::M03 > ()
{
  const auto & ccfg = config ().cutter ();
  if ( !ccfg.map_enabled ) {
    return;
  }

  // Set switch state
  {
    auto & melem = mpath_emplace< snc::mpath::elem::Fader > ();
    melem.set_fader_bits ( 1 );
    melem.set_fader_index ( ccfg.enable_switch_index );
    melem.fader_value_ref ().set_uint8 ( 0, !ccfg.enable_switch_inverted );
  }
  // Append warm up duration
  if ( ccfg.warm_up_duration.count () != 0 ) {
    mpath_emplace< snc::mpath::elem::Sleep > (
        std::chrono::ceil< std::chrono::microseconds > ( ccfg.warm_up_duration )
            .count () );
  }
}

template <>
void
Mapper::code_flush< codes::M05 > ()
{
  const auto & ccfg = config ().cutter ();
  if ( !ccfg.map_enabled ) {
    return;
  }

  // Set switch state
  {
    auto & melem = mpath_emplace< snc::mpath::elem::Fader > ();
    melem.set_fader_bits ( 1 );
    melem.set_fader_index ( ccfg.enable_switch_index );
    melem.fader_value_ref ().set_uint8 ( 0, ccfg.enable_switch_inverted );
  }
  // Append cool down duration
  if ( ccfg.cool_down_duration.count () != 0 ) {
    mpath_emplace< snc::mpath::elem::Sleep > (
        std::chrono::ceil< std::chrono::microseconds > (
            ccfg.cool_down_duration )
            .count () );
  }
}

template < class CODES >
void
Mapper::init_codes ( CODES & codes_n )
{
  // Write flush methods
  std::apply (
      [ this ] ( auto &&... code_n ) {
        auto flusher = [ this ] ( auto & item_n ) {
          using RT = decltype ( item_n );
          using CT = typename std::remove_reference< RT >::type;
          item_n.set_begin ( [ this ] () { this->code_begin< CT > (); } );
          item_n.set_flush ( [ this ] () { this->code_flush< CT > (); } );
        };
        ( flusher ( code_n ), ... );
      },
      codes_n.tuple );
}

} // namespace import::gcode
