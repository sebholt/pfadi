/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#include "parser.hpp"
#include <sev/assert.hpp>
#include <sev/lag/math.hpp>
#include <sev/string/utility.hpp>
#include <snc/mpath/d3/types.hpp>
#include <snc/mpath/speed_database.hpp>
#include <algorithm>
#include <cstdlib>
#include <string>
#include <utility>

namespace import::gcode
{

namespace
{

inline Parser::PRes
pres_consumed ( Parser::Token token_n = Parser::Token::NONE )
{
  return { true, token_n };
}

inline Parser::PRes
pres_not_consumed ( Parser::Token token_n = Parser::Token::NONE )
{
  return { false, token_n };
}
} // namespace

Parser::Parser ( const sev::logt::Reference & log_parent_n )
: _log ( log_parent_n, "Parser" )
{
}

void
Parser::begin ()
{
  reset ();

  _data_feed = true;
  _pfunc = &Parser::p_root;
}

void
Parser::reset ()
{
  if ( !is_parsing () ) {
    return;
  }

  _data_feed = false;
  _data_cur = nullptr;
  _data_end = nullptr;

  _pfunc = nullptr;
  _line = 0;
  _column = 0;

  _code_buffer.clear ();
  _comment_buffer.clear ();
  _error_message.clear ();

  _element = &_elem_base;
}

void
Parser::feed ( const char * data_n, std::size_t size_n )
{
  _data_cur = data_n;
  _data_end = data_n + size_n;
}

void
Parser::feed_end ()
{
  _data_feed = false;
}

Parser::Token
Parser::read ()
{
  return _data_feed ? parse_data () : parse_end ();
}

Parser::Token
Parser::parse_data ()
{
  // Stack local copies of pointers
  auto * dcur = _data_cur;
  auto * const dend = _data_end;

  while ( true ) {
    if ( dcur == dend ) {
      _data_cur = dcur;
      return Token::NONE;
    }
    PRes pres = _pfunc ( *this, *dcur );
    if ( pres.consumed ) {
      ++dcur;
    }
    if ( pres.token != Token::NONE ) {
      _data_cur = dcur;
      return pres.token;
    }
  }

  return Token::NONE;
}

Parser::Token
Parser::parse_end ()
{
  if ( _pfunc == &Parser::p_err ) {
    return Token::ERROR;
  } else if ( _pfunc == &Parser::p_comment_line ) {
    return pend_comment_line ( *this ).token;
  } else if ( _pfunc == &Parser::p_comment_parantheses ) {
    return pu_err ( *this, "Unexpected end of data while in comment" ).token;
  } else if ( _pfunc == &Parser::p_code_value ) {
    return pend_code_value ( *this ).token;
  }

  return Token::DONE;
}

inline void
Parser::pu_col ( Parser & p_n )
{
  ++p_n._column;
}

inline void
Parser::pu_cr ( Parser & p_n )
{
  p_n._column = 0;
}

inline void
Parser::pu_nl ( Parser & p_n )
{
  p_n._column = 0;
  ++p_n._line;
}

Parser::PRes
Parser::pu_err ( Parser & p_n, std::string message_n )
{
  p_n._pfunc = Parser::p_err;
  p_n._error_message = std::move ( message_n );
  return pres_not_consumed ( Token::ERROR );
}

Parser::PRes
Parser::p_err ( Parser & p_n [[maybe_unused]], char c_n [[maybe_unused]] )
{
  return pres_not_consumed ( Token::ERROR );
}

Parser::PRes
Parser::p_root ( Parser & p_n, char c_n )
{
  switch ( c_n ) {
  // Carriage return / new line
  case '\r':
    pu_cr ( p_n );
    return pres_consumed ();
  case '\n':
    pu_nl ( p_n );
    return pres_consumed ();

  // White space
  case '\t':
  case ' ':
    pu_col ( p_n );
    return pres_consumed ();

  // Start / End of program line
  case '%':
    pu_col ( p_n );
    p_n._pfunc = Parser::p_skip_line;
    return pres_consumed ();

  // Comment line
  case ';':
    pu_col ( p_n );
    p_n._pfunc = Parser::p_comment_line;
    return pres_consumed ();

  // Comment in parantheses
  case '(':
    pu_col ( p_n );
    p_n._pfunc = Parser::p_comment_parantheses;
    return pres_consumed ();

  // Code
  case 'A':
  case 'B':
  case 'C':
  case 'D':
  case 'E':
  case 'F':
  case 'G':
  case 'H':
  case 'I':
  case 'J':
  case 'K':
  case 'L':
  case 'M':
  case 'N':
  case 'O':
  case 'P':
  case 'Q':
  case 'R':
  case 'S':
  case 'T':
  case 'U':
  case 'V':
  case 'W':
  case 'X':
  case 'Y':
  case 'Z':
    pu_col ( p_n );
    p_n._elem_code.set_letter ( c_n );
    p_n._pfunc = Parser::p_code_value;
    return pres_consumed ();

  default:
    break;
  }
  return pu_err ( p_n, sev::string::cat ( "Unexpected character: ", c_n ) );
}

Parser::PRes
Parser::p_skip_line ( Parser & p_n, char c_n )
{
  switch ( c_n ) {
  case '\r':
    pu_cr ( p_n );
    return pres_consumed ();
  case '\n':
    pu_nl ( p_n );
    p_n._pfunc = Parser::p_root;
    return pres_consumed ();
  default:
    break;
  }

  // Consume by default
  pu_col ( p_n );
  return pres_consumed ();
}

Parser::PRes
Parser::p_comment_line ( Parser & p_n, char c_n )
{
  switch ( c_n ) {
  // End of line
  case '\r':
    pu_cr ( p_n );
    return pres_consumed ();
  case '\n':
    pu_nl ( p_n );
    return pend_comment_line ( p_n );

  default:
    break;
  }

  // Consume by default
  pu_col ( p_n );
  p_n._comment_buffer.push_back ( c_n );
  return pres_consumed ();
}

Parser::PRes
Parser::pend_comment_line ( Parser & p_n )
{
  p_n._elem_comment.set_text ( p_n._comment_buffer );
  p_n._comment_buffer.clear ();
  p_n._element = &p_n._elem_comment;
  p_n._pfunc = Parser::p_root;
  return pres_consumed ( Token::ELEMENT );
}

Parser::PRes
Parser::p_comment_parantheses ( Parser & p_n, char c_n )
{
  switch ( c_n ) {
  // End of line
  case '\r':
    return pu_err ( p_n, "Unexpected carriage return in comment" );
  case '\n':
    return pu_err ( p_n, "Unexpected end of line in comment" );

  // End of comment
  case ')':
    p_n._elem_comment.set_text ( p_n._comment_buffer );
    p_n._comment_buffer.clear ();
    p_n._element = &p_n._elem_comment;
    p_n._pfunc = Parser::p_root;
    return pres_consumed ( Token::ELEMENT );

  default:
    break;
  }

  // Consume by default
  pu_col ( p_n );
  p_n._comment_buffer.push_back ( c_n );
  return pres_consumed ();
}

Parser::PRes
Parser::p_code_value ( Parser & p_n, char c_n )
{
  switch ( c_n ) {
  case '+':
  case '-':
  case '0':
  case '1':
  case '2':
  case '3':
  case '4':
  case '5':
  case '6':
  case '7':
  case '8':
  case '9':
  case '.':
  case 'e':
  case 'E':
    pu_col ( p_n );
    p_n._code_buffer.push_back ( c_n );
    return pres_consumed ();

  // Allowed end of code value characters
  case ' ':
  case '\t':
  case '\r':
  case '\n':
    return pend_code_value ( p_n );

  default:
    break;
  }

  return pu_err (
      p_n,
      sev::string::cat ( "Unexpected character ", c_n, " in code value" ) );
}

Parser::PRes
Parser::pend_code_value ( Parser & p_n )
{
  // Evaluate
  if ( p_n._code_buffer.empty () ) {
    return pu_err ( p_n, "Empty code value" );
  }

  {
    double value = 0.0;
    if ( !sev::string::get_number ( value, p_n._code_buffer ) ) {
      auto res = pu_err (
          p_n,
          sev::string::cat ( "Can't convert code value string to double: ",
                             p_n._code_buffer ) );
      p_n._code_buffer.clear ();
      return res;
    }
    p_n._elem_code.set_value ( value );
  }

  p_n._code_buffer.clear ();
  p_n._element = &p_n._elem_code;
  p_n._pfunc = Parser::p_root;
  return pres_not_consumed ( Token::ELEMENT );
}

} // namespace import::gcode
