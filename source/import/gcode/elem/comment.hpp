/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <import/gcode/element.hpp>
#include <string>
#include <utility>

namespace import::gcode::elem
{

class Comment : public Element
{
  public:
  // -- Construction
  Comment ()
  : Element ( Element_Type::COMMENT )
  {
  }

  // -- Attributes

  const std::string &
  text () const
  {
    return _text;
  }

  void
  set_text ( std::string text_n )
  {
    _text = std::move ( text_n );
  }

  private:
  std::string _text;
};

} // namespace import::gcode::elem
