/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <import/gcode/element.hpp>

namespace import::gcode::elem
{

class Code : public Element
{
  public:
  // -- Construction
  Code ()
  : Element ( Element_Type::CODE )
  {
  }

  // -- Attributes

  char
  letter () const
  {
    return _letter;
  }

  void
  set_letter ( char c_n )
  {
    _letter = c_n;
  }

  double
  value () const
  {
    return _value;
  }

  void
  set_value ( double v_n )
  {
    _value = v_n;
  }

  private:
  char _letter = 0;
  double _value = 0.0;
};

} // namespace import::gcode::elem
