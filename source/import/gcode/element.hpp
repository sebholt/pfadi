/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <cstdint>

namespace import::gcode
{

enum class Element_Type
{
  INVALID,
  COMMENT,
  CODE
};

class Element
{
  public:
  // -- Construction

  Element ( Element_Type type_n = Element_Type::INVALID )
  : _type ( type_n )
  {
  }

  Element_Type
  type () const
  {
    return _type;
  }

  private:
  Element_Type _type = Element_Type::INVALID;
};

} // namespace import::gcode
