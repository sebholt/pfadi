/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#include "config.hpp"

namespace import::config
{

Cutter::Cutter () = default;

void
Cutter::reset ()
{
  map_enabled = false;
  enable_switch_inverted = false;
  enable_switch_index = 0;
  warm_up_duration = std::chrono::nanoseconds{};
  cool_down_duration = std::chrono::nanoseconds{};
}
} // namespace import::config

namespace import
{

Config::Config () {}

void
Config::reset ()
{
  _cutter.reset ();
}

} // namespace import
