/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <chrono>
#include <cstdint>

namespace import::config
{
/// @brief MPML to MPath mapping configuration for mpml cutter element
///
class Cutter
{
  public:
  // -- Construction and setup

  Cutter ();

  void
  reset ();

  public:
  // -- Attributes
  /// @brief To map or not to map cutter elements
  bool map_enabled = false;
  bool enable_switch_inverted = false;
  std::uint8_t enable_switch_index = 0;
  std::chrono::nanoseconds warm_up_duration = {};
  std::chrono::nanoseconds cool_down_duration = {};
};
} // namespace import::config

namespace import
{

/// @brief MPML to MPath mapping configuration
///
class Config
{
  public:
  // -- Construction and setup

  Config ();

  void
  reset ();

  // -- Accessors

  const config::Cutter &
  cutter () const
  {
    return _cutter;
  }

  config::Cutter &
  cutter_ref ()
  {
    return _cutter;
  }

  private:
  // -- Attributes
  config::Cutter _cutter;
};

} // namespace import
