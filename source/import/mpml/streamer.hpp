/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <import/data_streamer.hpp>
#include <import/mpml/mapper.hpp>
#include <import/mpml/parser.hpp>
#include <sev/logt/context.hpp>
#include <snc/mpath_stream/write.hpp>

namespace import::mpml
{

/// @brief Parses an mpml text stream and feeds an MPath stream
///
class Streamer : public import::Data_Streamer
{
  // -- Types
  private:
  using Super = import::Data_Streamer;

  public:
  using Stream = snc::mpath_stream::Write;

  // -- Construction

  Streamer ( const sev::logt::Reference & log_parent_n );

  // -- Interface

  void
  begin ( const import::Config & config_n );

  void
  feed ( const char * data_n, std::size_t size_n, Stream * stream_n ) override;

  void
  feed_end ( Stream * stream_n ) override;

  void
  end () override;

  private:
  // -- Utility

  bool
  parse ( Stream * stream_n );

  bool
  map ( Stream * stream_n );

  private:
  Parser _parser;
  Mapper _mapper;
};

} // namespace import::mpml
