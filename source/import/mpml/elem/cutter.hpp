/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <import/mpml/element.hpp>

namespace import::mpml::elem
{

class Cutter : public import::mpml::Element
{
  public:
  // -- Construction
  Cutter ()
  : import::mpml::Element ( import::mpml::Element_Type::CUTTER )
  , speed_type ( import::mpml::Cutter_Speed_Type::INVALID )
  {
  }

  public:
  // -- Attributes
  import::mpml::Cutter_Speed_Type speed_type;
};

} // namespace import::mpml::elem
