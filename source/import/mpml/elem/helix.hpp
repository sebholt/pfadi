/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <import/mpml/element.hpp>
#include <array>

namespace import::mpml::elem
{

class Helix : public import::mpml::Element
{
  public:
  // -- Construction
  Helix ()
  : import::mpml::Element ( import::mpml::Element_Type::HELIX )
  , pos_center_type{ { import::mpml::Coord_Type::INVALID } }
  , pos_center{ { 0.0 } }
  {
  }

  public:
  // -- Attributes
  std::array< import::mpml::Coord_Type, 3 > pos_center_type;
  std::array< double, 3 > pos_center;

  bool angle_rad_valid = false;
  double angle_rad = 0.0;

  bool length_valid = false;
  double length = 0.0;
};

} // namespace import::mpml::elem
