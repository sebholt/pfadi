/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <import/mpml/element.hpp>
#include <import/mpml/types.hpp>

namespace import::mpml::elem
{

class Speed : public import::mpml::Element
{
  public:
  // -- Construction
  Speed ()
  : import::mpml::Element ( import::mpml::Element_Type::SPEED )
  {
  }

  public:
  // -- Attributes
  Speed_Type speed_type = Speed_Type::INVALID;
  double speed_value = 0.0;
};

} // namespace import::mpml::elem
