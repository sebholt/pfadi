/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <import/mpml/element.hpp>
#include <sev/lag/vector3.hpp>
#include <array>

namespace import::mpml::elem
{

class Cubic : public import::mpml::Element
{
  public:
  // -- Construction
  Cubic ()
  : import::mpml::Element ( import::mpml::Element_Type::CUBIC )
  {
    for ( auto & item : pos_type ) {
      item = import::mpml::Coord_Type::INVALID;
    }
    for ( auto & item : pos ) {
      item.fill ( 0.0 );
    }
  }

  public:
  // -- Attributes
  std::array< import::mpml::Coord_Type, ( 3 * 3 ) > pos_type;
  std::array< sev::lag::Vector3d, 3 > pos;
};

} // namespace import::mpml::elem
