/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <import/mpml/element.hpp>
#include <array>

namespace import::mpml::elem
{

class Move : public import::mpml::Element
{
  public:
  // -- Construction
  Move ()
  : import::mpml::Element ( import::mpml::Element_Type::MOVE )
  , pos_target_type{ { import::mpml::Coord_Type::INVALID } }
  , pos_target{ { 0.0 } }
  {
  }

  public:
  // -- Attributes
  std::array< import::mpml::Coord_Type, 3 > pos_target_type;
  std::array< double, 3 > pos_target;
};

} // namespace import::mpml::elem
