/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <import/mpml/element.hpp>
#include <import/mpml/types.hpp>

namespace import::mpml::elem
{

class Speed_Db : public import::mpml::Element
{
  public:
  // -- Construction
  Speed_Db ()
  : import::mpml::Element ( import::mpml::Element_Type::SPEED_DB )
  {
  }

  public:
  // -- Attributes
  Speed_Db_Type speed_type = Speed_Db_Type::INVALID;
  double speed_value = 0.0;
};

} // namespace import::mpml::elem
