/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include "elements.hpp"
#include <array>
#include <cstdint>

namespace import::mpml
{

class Element_Buffer
{
  // Public types
  public:
  static constexpr std::size_t buffer_size = 128;

  // Public methods
  public:
  Element_Buffer () { invalidate (); }

  /// @brief Creates an invalid import::mpml::Element
  void
  invalidate ()
  {
    new ( _buffer.data () ) Element ();
  }

  template < class T, typename... Args >
  T &
  create_element ( Args &&... args_n )
  {
    static_assert ( sizeof ( T ) <= buffer_size, "Buffer size too small" );
    new ( _buffer.data () ) T ( std::forward< Args > ( args_n )... );
    return casted_element< T > ();
  }

  Element &
  element ()
  {
    return *( reinterpret_cast< Element * > ( _buffer.data () ) );
  }

  const Element &
  element () const
  {
    return *( reinterpret_cast< const Element * > ( _buffer.data () ) );
  }

  template < class T >
  T &
  casted_element ()
  {
    return static_cast< T & > ( element () );
  }

  template < class T >
  const T &
  casted_element () const
  {
    return static_cast< T & > ( element () );
  }

  private:
  std::array< std::uint8_t, buffer_size > _buffer;
};

} // namespace import::mpml
