/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#include "streamer.hpp"

namespace import::mpml
{

Streamer::Streamer ( const sev::logt::Reference & log_parent_n )
: Super ( { log_parent_n, "MPML-Streamer" } )
, _parser ( log () )
, _mapper ( log () )
{
}

void
Streamer::begin ( const import::Config & config_n )
{
  Super::begin ();
  _parser.begin ();
  _mapper.begin ( config_n );
}

void
Streamer::end ()
{
  _parser.reset ();
  _mapper.reset ();
  Super::end ();
}

void
Streamer::feed ( const char * data_n, std::size_t size_n, Stream * stream_n )
{
  _parser.feed ( data_n, size_n );
  while ( parse ( stream_n ) ) {
  };
}

void
Streamer::feed_end ( Stream * stream_n )
{
  while ( parse ( stream_n ) ) {
  };
}

bool
Streamer::parse ( Stream * stream_n )
{
  using RToken = Parser::Token;

  switch ( _parser.read () ) {
  case RToken::NONE:
    // Should not happen
    feed_end_error ( "Bad mpml reader state." );
    return false;

  case RToken::ERROR:
    // Register error
    feed_end_error (
        sev::string::cat ( "MPML parse error: ", _parser.error_message () ) );
    return false;

  case RToken::FEED:
    // Data from file needed
    return false;

  case RToken::BEGIN:
    // Ignore
    return true;

  case RToken::ELEMENT:
    // Feed mapper from reader
    _mapper.feed ( &_parser.element () );
    return map ( stream_n );

  case RToken::UNKNOWN_ELEMENT:
    // Ignore
    return true;

  case RToken::END:
    // Finish
    _mapper.feed_end ();
    return map ( stream_n );

  case RToken::DONE:
    return map ( stream_n );
  }

  return false;
}

bool
Streamer::map ( Stream * stream_n )
{
  while ( true ) {
    using MToken = Mapper::Token;
    switch ( _mapper.read () ) {
    case MToken::NONE:
      // Continue reading
      return true;

    case MToken::ERROR:
      feed_end_error ( sev::string::cat ( "MPML mapper error: ",
                                          _mapper.error_message () ) );
      return false;

    case MToken::ELEMENT:
      // Write element to stream and read again
      stream_n->write ( *_mapper.element () );
      _mapper.element_pop ();
      break;

    case MToken::DONE:
      feed_end_good ();
      return false;
    }
  }

  return false;
}

} // namespace import::mpml
