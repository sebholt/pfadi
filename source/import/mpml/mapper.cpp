/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#include "mapper.hpp"
#include <import/mpml/elements.hpp>
#include <sev/assert.hpp>
#include <sev/lag/math.hpp>
#include <sev/string/utility.hpp>
#include <snc/mpath/speed_database.hpp>
#include <algorithm>

namespace import::mpml
{

Mapper::Mapper ( const sev::logt::Reference & log_parent_n )
: _log ( log_parent_n, "Mapper" )
{
  _pos_current.fill ( 0.0 );
  _pos_current_valid.fill ( false );
}

void
Mapper::begin ( const import::Config & config_n )
{
  reset ();

  _config = config_n;

  _active = true;
  _element_feed = true;
}

void
Mapper::reset ()
{
  if ( !is_mapping () ) {
    return;
  }

  _active = false;
  _element_feed = false;

  _error_message.clear ();

  _mpath_rbuffer.clear ();

  _pos_current.fill ( 0.0 );
  _pos_current_valid.fill ( false );
  _inside_curve_stream = false;
}

void
Mapper::feed ( const Element * element_n )
{
  if ( !is_mapping () ) {
    return;
  }

  _mpml_element = element_n;
  compile_mpath_element ();
  _mpml_element = nullptr;
}

void
Mapper::feed_end ()
{
  if ( !is_mapping () ) {
    return;
  }

  // No more elements coming
  _element_feed = false;

  // Finish with stream end element
  mpath_emplace< snc::mpath::elem::Stream_End > ();
}

Mapper::Token
Mapper::read ()
{
  if ( !_error_message.empty () ) {
    return Token::ERROR;
  }
  if ( !_mpath_rbuffer.is_empty () ) {
    return Token::ELEMENT;
  }
  if ( _element_feed ) {
    return Token::NONE;
  }

  return Token::DONE;
}

void
Mapper::raise_error ( std::string_view err_n )
{
  // Fill error information
  if ( err_n.empty () ) {
    err_n = "Error";
  }
  if ( !_error_message.empty () ) {
    // Ensure new line
    constexpr char eol = '\n';
    if ( _error_message.back () != eol ) {
      _error_message.push_back ( eol );
    }
  }
  _error_message.append ( err_n );

  _log.cat ( sev::logt::FL_DEBUG_0, err_n );
}

template < class T, typename... Args >
T &
Mapper::mpath_emplace ( Args &&... args_n )
{
  // Insert curve end elements automatically
  bool is_curve = snc::mpath::d3::elem::type_is_curve ( T::class_elem_type ) ||
                  ( T::class_elem_type == snc::mpath::elem::Type::SPEED );
  if ( is_curve != _inside_curve_stream ) {
    if ( is_curve ) {
      // A curve starts
      _inside_curve_stream = true;
    } else {
      // A curve sequence ends
      _inside_curve_stream = false;
      // Insert curve finish element
      snc::mpath::elem::Stream_Curve_Finish * elem;
      _mpath_rbuffer.emplace_back_not_full ( &elem );
    }
  }

  T * res = nullptr;
  _mpath_rbuffer.emplace_back_not_full ( &res,
                                         std::forward< Args > ( args_n )... );

  return *res;
}

void
Mapper::acquire_axis_coord ( std::size_t axis_index_n,
                             double * val_res_n,
                             double val_input_n,
                             Coord_Type coord_type_n )
{
  double value = *val_res_n;
  bool value_valid = false;
  if ( coord_type_n != Coord_Type::INVALID ) {
    // Valid element value
    if ( coord_type_n == Coord_Type::RELATIVE ) {
      // Relative position
      if ( _pos_current_valid[ axis_index_n ] ) {
        value = _pos_current[ axis_index_n ] + val_input_n;
        value_valid = true;
      } else {
        raise_error ( sev::string::cat (
            "Axis ",
            axis_index_n,
            " relative position to unknown absolute position" ) );
      }
    } else {
      // Absolute position
      value = val_input_n;
      value_valid = true;
    }
  } else if ( _pos_current_valid[ axis_index_n ] ) {
    value = _pos_current[ axis_index_n ];
    value_valid = true;
  } else {
    raise_error (
        sev::string::cat ( "Axis ", axis_index_n, " without valid value" ) );
  }

  if ( value_valid ) {
    *val_res_n = value;
  }
}

void
Mapper::acquire_pos_current ( snc::mpath::elem::Curve_D< 3 > & curve_n )
{
  // Acquire new current position
  _eval_buffer.new_from_curve ( _pos_current, curve_n );
  _eval_buffer.evaluator ().calc_end_pos ( _pos_current );
  for ( auto & item : _pos_current_valid ) {
    item = true;
  }
}

void
Mapper::compile_mpath_element ()
{
  if ( ( _mpml_element == nullptr ) ||
       ( _mpml_element->elem_type () == Element_Type::INVALID ) ) {
    raise_error ( "Bad MPML element pointer" );
    return;
  }

  switch ( _mpml_element->elem_type () ) {
  case Element_Type::MOVE:
    compile_mpath_elem_move ();
    break;
  case Element_Type::CIRCLE:
    compile_mpath_elem_circle ();
    break;
  case Element_Type::HELIX:
    compile_mpath_elem_helix ();
    break;
  case Element_Type::CUBIC:
    compile_mpath_elem_cubic ();
    break;
  case Element_Type::SPEED_DB:
    compile_mpath_elem_speed_db ();
    break;
  case Element_Type::SPEED:
    compile_mpath_elem_speed ();
    break;
  case Element_Type::CUTTER:
    compile_mpath_elem_cutter ();
    break;
  default:
    raise_error ( "Unknown mpml element type" );
    break;
  }
}

void
Mapper::compile_mpath_elem_move ()
{
  auto & xelem = static_cast< const elem::Move & > ( *_mpml_element );

  bool valid_pos ( false );
  // Check if any of the given positions is valid
  for ( std::size_t ii = 0; ii != xelem.pos_target_type.size (); ++ii ) {
    if ( xelem.pos_target_type[ ii ] != Coord_Type::INVALID ) {
      valid_pos = true;
      break;
    }
  }
  if ( valid_pos ) {
    // Target position
    sev::lag::Vector3d pos_end;
    for ( std::size_t ii = 0; ii != 3; ++ii ) {
      acquire_axis_coord ( ii,
                           &( pos_end[ ii ] ),
                           xelem.pos_target[ ii ],
                           xelem.pos_target_type[ ii ] );
    }
    if ( !error () ) {
      auto & melem = mpath_emplace< snc::mpath::d3::elem::Curve_Linear > ();
      melem.set_pos_end ( pos_end );
      acquire_pos_current ( melem );
    }
  } else {
    raise_error ( "No valid move position" );
  }
}

void
Mapper::compile_mpath_elem_circle ()
{
  auto & xelem = static_cast< const elem::Circle & > ( *_mpml_element );

  bool valid_angle = xelem.angle_rad_valid;
  bool valid_pos = false;
  for ( std::size_t ii = 0; ii != xelem.pos_center_type.size (); ++ii ) {
    if ( xelem.pos_center_type[ ii ] != Coord_Type::INVALID ) {
      valid_pos = true;
      break;
    }
  }
  if ( valid_angle && valid_pos ) {
    // Center position
    sev::lag::Vector2d pos_center;
    for ( std::size_t ii = 0; ii != 2; ++ii ) {
      acquire_axis_coord ( ii,
                           &( pos_center[ ii ] ),
                           xelem.pos_center[ ii ],
                           xelem.pos_center_type[ ii ] );
    }
    if ( !error () ) {
      auto & melem = mpath_emplace< snc::mpath::d3::elem::Curve_Circle > ();
      melem.set_pos_center ( pos_center );
      melem.set_angle_radians ( xelem.angle_rad );
      acquire_pos_current ( melem );
    }
  } else {
    if ( !valid_angle ) {
      raise_error ( "No valid circle angle" );
    }
    if ( !valid_pos ) {
      raise_error ( "No valid circle center value" );
    }
  }
}

void
Mapper::compile_mpath_elem_helix ()
{
  auto & xelem = static_cast< const elem::Helix & > ( *_mpml_element );

  bool valid_angle = xelem.angle_rad_valid;
  bool valid_pos = false;
  for ( std::size_t ii = 0; ii < xelem.pos_center_type.size (); ++ii ) {
    if ( xelem.pos_center_type[ ii ] != Coord_Type::INVALID ) {
      valid_pos = true;
      break;
    }
  }
  bool valid_length = xelem.length_valid;
  if ( valid_angle && valid_pos && valid_length ) {
    // Center position
    sev::lag::Vector2d pos_center;
    for ( std::size_t ii = 0; ii != 2; ++ii ) {
      acquire_axis_coord ( ii,
                           &( pos_center[ ii ] ),
                           xelem.pos_center[ ii ],
                           xelem.pos_center_type[ ii ] );
    }
    if ( !error () ) {
      auto & melem = mpath_emplace< snc::mpath::d3::elem::Curve_Helix > ();
      melem.set_pos_center ( pos_center );
      melem.set_angle_radians ( xelem.angle_rad );
      melem.set_height ( xelem.length );
      acquire_pos_current ( melem );
    }
  } else {
    if ( !valid_angle ) {
      raise_error ( "No valid helix angle" );
    }
    if ( !valid_pos ) {
      raise_error ( "No valid helix center value" );
    }
    if ( !valid_length ) {
      raise_error ( "No valid helix length value" );
    }
  }
}

void
Mapper::compile_mpath_elem_cubic ()
{
  auto & xelem = static_cast< const elem::Cubic & > ( *_mpml_element );

  bool valid_pos = false;
  for ( std::size_t ii = 0; ii != xelem.pos_type.size (); ++ii ) {
    if ( xelem.pos_type[ ii ] != Coord_Type::INVALID ) {
      valid_pos = true;
      break;
    }
  }
  if ( !valid_pos ) {
    raise_error ( "Invalid position value" );
    return;
  }

  // Positions
  {
    sev::lag::Vector3d pos[ 3 ];
    for ( std::size_t ii = 0; ii != 3; ++ii ) {
      const sev::lag::Vector3d & mpos ( xelem.pos[ ii ] );
      const Coord_Type * mpos_type ( &( xelem.pos_type[ ii * 3 ] ) );
      for ( std::size_t jj = 0; jj != 3; ++jj ) {
        acquire_axis_coord (
            jj, &( pos[ ii ][ jj ] ), mpos[ jj ], mpos_type[ jj ] );
      }
    }
    if ( !error () ) {
      auto & melem = mpath_emplace< snc::mpath::d3::elem::Curve_Cubic > ();
      for ( std::size_t ii = 0; ii != 3; ++ii ) {
        melem.set_point ( ii, pos[ ii ] );
      }
      acquire_pos_current ( melem );
    }
  }
}

void
Mapper::compile_mpath_elem_speed_db ()
{
  auto & xelem = static_cast< const elem::Speed_Db & > ( *_mpml_element );

  auto speed_id = snc::mpath::Speed_Db_Id::LIST_END;
  switch ( xelem.speed_type ) {
  case Speed_Db_Type::RAPID:
    speed_id = snc::mpath::Speed_Db_Id::RAPID;
    break;
  case Speed_Db_Type::MATERIAL:
    speed_id = snc::mpath::Speed_Db_Id::MATERIAL;
    break;
  case Speed_Db_Type::MATERIAL_PLANAR:
    speed_id = snc::mpath::Speed_Db_Id::MATERIAL_PLANAR;
    break;
  case Speed_Db_Type::MATERIAL_NORMAL:
    speed_id = snc::mpath::Speed_Db_Id::MATERIAL_NORMAL;
    break;
  default:
    raise_error ( "Invalid speed db type" );
    return;
  }

  auto & melem = mpath_emplace< snc::mpath::elem::Speed_Db_Value > ();
  melem.set_speed_id ( speed_id );
  melem.set_speed_value ( xelem.speed_value );
}

void
Mapper::compile_mpath_elem_speed ()
{
  auto & xelem = static_cast< const elem::Speed & > ( *_mpml_element );

  auto speed_regime = snc::mpath::Speed_Regime::LIST_END;
  switch ( xelem.speed_type ) {
  case Speed_Type::RAPID:
    speed_regime = snc::mpath::Speed_Regime::RAPID;
    break;
  case Speed_Type::MATERIAL:
    speed_regime = snc::mpath::Speed_Regime::MATERIAL;
    break;
  default:
    raise_error ( "Invalid curve speed type" );
    return;
  }

  auto & melem = mpath_emplace< snc::mpath::elem::Speed > ();
  melem.speed ().set_regime ( speed_regime );
}

void
Mapper::compile_mpath_elem_cutter ()
{
  const auto & ccfg = config ().cutter ();
  if ( !ccfg.map_enabled ) {
    return;
  }
  auto & xelem = static_cast< const elem::Cutter & > ( *_mpml_element );

  // Cutter switch state
  bool cutter_enable = false;

  switch ( xelem.speed_type ) {
  case Cutter_Speed_Type::OFF:
    cutter_enable = false;
    break;
  case Cutter_Speed_Type::STAND_BY:
  case Cutter_Speed_Type::MATERIAL:
    cutter_enable = true;
    break;
  default:
    raise_error ( "Invalid cutter speed type" );
    return;
  }

  if ( cutter_enable ) {
    // Set switch state
    {
      auto & melem = mpath_emplace< snc::mpath::elem::Fader > ();
      melem.set_fader_bits ( 1 );
      melem.set_fader_index ( ccfg.enable_switch_index );
      melem.fader_value_ref ().set_uint8 ( 0, !ccfg.enable_switch_inverted );
      // Debug
      _log.cat ( sev::logt::FL_DEBUG_0,
                 "Cutter enable:",
                 " fader_bits() ",
                 std::uint_fast32_t ( melem.fader_bits () ),
                 " fader_index() ",
                 std::uint_fast32_t ( melem.fader_index () ),
                 " fader_value() ",
                 std::uint_fast32_t ( melem.fader_value ().get_uint8 ( 0 ) ) );
    }
    // Append warm up usecs
    if ( ccfg.warm_up_duration.count () != 0 ) {
      mpath_emplace< snc::mpath::elem::Sleep > (
          std::chrono::ceil< std::chrono::microseconds > (
              ccfg.warm_up_duration )
              .count () );
    }
  } else {
    // Set switch state
    {
      auto & melem = mpath_emplace< snc::mpath::elem::Fader > ();
      melem.set_fader_bits ( 1 );
      melem.set_fader_index ( ccfg.enable_switch_index );
      melem.fader_value_ref ().set_uint8 ( 0, ccfg.enable_switch_inverted );
      // Debug
      _log.cat ( sev::logt::FL_DEBUG_0,
                 "Cutter disable:",
                 " fader_bits() ",
                 std::uint_fast32_t ( melem.fader_bits () ),
                 " fader_index() ",
                 std::uint_fast32_t ( melem.fader_index () ),
                 " fader_value() ",
                 std::uint_fast32_t ( melem.fader_value ().get_uint8 ( 0 ) ) );
    }
    // Append cool down usecs
    if ( ccfg.cool_down_duration.count () != 0 ) {
      mpath_emplace< snc::mpath::elem::Sleep > (
          std::chrono::ceil< std::chrono::microseconds > (
              ccfg.cool_down_duration )
              .count () );
    }
  }
}

} // namespace import::mpml
