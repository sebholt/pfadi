/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#include "parser.hpp"
#include <sev/assert.hpp>
#include <sev/lag/math.hpp>
#include <sev/string/utility.hpp>
#include <snc/mpath/d3/types.hpp>
#include <snc/mpath/speed_database.hpp>
#include <algorithm>

namespace import::mpml
{

Parser::Parser ( const sev::logt::Reference & log_parent_n )
: _log ( log_parent_n, "Parser" )
{
}

void
Parser::begin ()
{
  reset ();

  // Reset buffer
  _mpml_elem_buffer.invalidate ();
  // Update parser state
  _pstate = PState::FIND_MPML;
  _xstate = XState::FEED;
}

void
Parser::reset ()
{
  if ( !is_reading () ) {
    return;
  }

  _xml_stream.clear ();

  _error_message.clear ();
  _unknown_element.clear ();
  _depth = 0;
  _depth_unknown = 0;
  _mpml_elem_string = nullptr;

  _token = Token::NONE;
  _pstate = PState::NONE;
  _xstate = XState::NONE;
}

void
Parser::feed ( const char * data_n, std::size_t size_n )
{
  if ( _xstate == XState::FEED ) {
    _xml_stream.addData ( QByteArray::fromRawData ( data_n, size_n ) );
    // Update xstate
    _xstate = XState::READ;
  }
}

Parser::Token
Parser::read ()
{
  _token = Token::NONE;
  switch ( _pstate ) {
  case PState::FIND_MPML:
  case PState::ELEMENTS:
    xml_process ();
    break;
  case PState::ERROR:
    _token = Token::ERROR;
    break;
  case PState::DONE:
    _token = Token::DONE;
    break;
  default:
    break;
  }
  // Return and clear token
  Token res ( _token );
  _token = Token::NONE;
  return res;
}

void
Parser::xml_process ()
{
  do {
    // Evaluate parser state
    switch ( _xstate ) {
    case XState::FEED:
      // Feed xml parser with raw data
      _token = Token::FEED;
      break;
    case XState::READ:
      // Read elements from satiated xml parser
      xml_read ();
      break;
    default:
      raise_error ( "Invalid parser xstate" );
      break;
    }
  } while ( _token == Token::NONE );
}

void
Parser::xml_read ()
{
  switch ( _xml_stream.readNext () ) {
  case QXmlStreamReader::NoToken:
    break;
  case QXmlStreamReader::Invalid:
    xml_read_error ();
    break;
  case QXmlStreamReader::StartDocument:
    cb_document_begin ();
    break;
  case QXmlStreamReader::EndDocument:
    cb_document_end ();
    break;
  case QXmlStreamReader::StartElement:
    cb_element_begin ();
    break;
  case QXmlStreamReader::EndElement:
    cb_element_end ();
    break;
  case QXmlStreamReader::Characters:
  case QXmlStreamReader::Comment:
  case QXmlStreamReader::DTD:
  case QXmlStreamReader::EntityReference:
  case QXmlStreamReader::ProcessingInstruction:
    break;
  }
}

void
Parser::xml_read_error ()
{
  // Check if there is already an error registered
  if ( _pstate == PState::ERROR ) {
    return;
  }

  // Evaluate XML parser error
  switch ( _xml_stream.error () ) {
  case QXmlStreamReader::CustomError:
    raise_xml_error ();
    break;
  case QXmlStreamReader::NotWellFormedError:
    raise_xml_error ();
    break;
  case QXmlStreamReader::PrematureEndOfDocumentError:
    // Out of data. Request new data feeding.
    _xstate = XState::FEED;
    break;
  case QXmlStreamReader::UnexpectedElementError:
    raise_xml_error ();
    break;
  default:
    break;
  }
}

void
Parser::raise_xml_error ()
{
  raise_error ( _xml_stream.errorString ().toStdString () );
}

void
Parser::raise_error ( std::string_view err_n )
{
  // Fill error information
  if ( !_error_message.empty () ) {
    // Ensure new line
    constexpr char eol = '\n';
    if ( _error_message.back () != eol ) {
      _error_message.push_back ( eol );
    }
  }
  _error_message.append ( err_n );

  // Update state
  _token = Token::ERROR;
  _pstate = PState::ERROR;
  _xstate = XState::NONE;

  // Debug
  _log.cat ( sev::logt::FL_DEBUG_0, err_n );
}

void
Parser::cb_document_begin ()
{
  // Debug
  _log.cat ( sev::logt::FL_DEBUG_1, "XML document begin" );
}

void
Parser::cb_document_end ()
{
  // Debug
  _log.cat ( sev::logt::FL_DEBUG_1, "XML document end" );
}

void
Parser::cb_element_begin ()
{
  const QStringRef & elem_name = _xml_stream.name ();

  ++_depth;
  // Limit (unknown) depth
  if ( _depth > 1024 ) {
    raise_error ( "Nesting too deep" );
  }
  if ( _pstate == PState::FIND_MPML ) {
    if ( _depth == 1 ) {
      if ( elem_name_match ( elem_name, String_Id::mpml ) ) {
        // MPML root element found
        // Update state
        _token = Token::BEGIN;
        _pstate = PState::ELEMENTS;
        // Debug
        _log.cat ( sev::logt::FL_DEBUG_1,
                   "MPML begin found: ",
                   elem_name.toString ().toStdString () );
      } else {
        raise_error ( "Expected mpml root element" );
      }
    } else {
      raise_error ( "mpml root element at wrong depth" );
    }
  } else if ( _pstate == PState::ELEMENTS ) {
    bool unknown = true;
    if ( _depth == 2 ) {
      // Check if we know this element type
      DEBUG_ASSERT ( _mpml_elem_string == nullptr );
      // Compare element string with known element names
      for ( std::size_t ii = 0; ii != num_element_types; ++ii ) {
        const Elem_String & cselem =
            _mpml_sdb.str_elem ( static_cast< Element_Type > ( ii ) );
        if ( elem_name_match ( elem_name, cselem.elem_name ) ) {
          _mpml_elem_string = &cselem;
          break;
        }
      }
      if ( _mpml_elem_string != nullptr ) {
        // Parse element
        parse_mpml_elem ();
        unknown = false;
      }
    }
    // Process unknown elements
    if ( unknown ) {
      if ( _depth_unknown == 0 ) {
        // New unknown element
        _unknown_element = elem_name.toString ().toStdString ();
      }
      // Increment unknown depth
      ++_depth_unknown;
    }
  } else {
    raise_error ( "Invalid parser pstate" );
  }
}

void
Parser::cb_element_end ()
{
  const QStringRef & elem_name = _xml_stream.name ();

  if ( _depth != 0 ) {
    --_depth;
  }
  if ( _depth_unknown != 0 ) {
    // Decrement unknown depth
    --_depth_unknown;
    if ( _depth_unknown == 0 ) {
      // Unknown element end
      _token = Token::UNKNOWN_ELEMENT;
    }
  } else if ( _mpml_elem_string != nullptr ) {
    // Process mpml element end
    if ( elem_name_match ( elem_name, _mpml_elem_string->elem_name ) ) {
      // Update public state: Element complete
      _token = Token::ELEMENT;
    } else {
      raise_error (
          sev::string::cat ( "MPML: Invalid element ",
                             _mpml_elem_string->elem_name.toStdString (),
                             " end: ",
                             elem_name.toString ().toStdString () ) );
    }
    // Clear elem string reference
    _mpml_elem_string = nullptr;
  } else if ( _pstate == PState::ELEMENTS ) {
    // Process mpml end
    if ( elem_name_match ( elem_name, String_Id::mpml ) ) {
    } else {
      raise_error (
          sev::string::cat ( "MPML: Invalid element ",
                             _mpml_sdb.str ( String_Id::mpml ).toStdString (),
                             " end: ",
                             elem_name.toString ().toStdString () ) );
    }

    // Update public state: Element complete
    _token = Token::END;
    // Update parser state: Done
    _pstate = PState::DONE;
    _xstate = XState::NONE;
    // Clear xml stream
    _xml_stream.clear ();
  } else if ( _depth == 0 ) {
    raise_error ( sev::string::cat ( "MPML: unexpected element end ",
                                     elem_name.toString ().toStdString () ) );
  }
}

bool
Parser::elem_name_match ( const QStringRef & name0_n,
                          const QString & name1_n ) const
{
  return ( QString::compare ( name1_n, name0_n, Qt::CaseInsensitive ) == 0 );
}

bool
Parser::elem_name_match ( const QStringRef & name0_n, String_Id id_n ) const
{
  return ( QString::compare (
               _mpml_sdb.str ( id_n ), name0_n, Qt::CaseInsensitive ) == 0 );
}

bool
Parser::elem_attr_match ( const QStringRef & name0_n, String_Id id_n ) const
{
  return ( QString::compare (
               _mpml_sdb.str ( id_n ), name0_n, Qt::CaseInsensitive ) == 0 );
}

bool
Parser::parse_mpml_position ( double * res_coord_n,
                              Coord_Type * res_coord_type_n,
                              const QStringRef & key_n,
                              const QStringRef & value_n )
{
  bool res = true;

  std::array< std::uint8_t, 3 > axes{ { 0 } };
  std::uint8_t num_axes ( 0 );
  Coord_Type coord_type = Coord_Type::ABSOLUTE;

  if ( elem_attr_match ( key_n, String_Id::x ) ) {
    // --- x value
    num_axes = 1;
    axes[ 0 ] = 0;
    coord_type = Coord_Type::ABSOLUTE;
  } else if ( elem_attr_match ( key_n, String_Id::dx ) ) {
    // --- x delta value
    num_axes = 1;
    axes[ 0 ] = 0;
    coord_type = Coord_Type::RELATIVE;
  } else if ( elem_attr_match ( key_n, String_Id::y ) ) {
    // --- y value
    num_axes = 1;
    axes[ 0 ] = 1;
    coord_type = Coord_Type::ABSOLUTE;
  } else if ( elem_attr_match ( key_n, String_Id::dy ) ) {
    // --- y delta value
    num_axes = 1;
    axes[ 0 ] = 1;
    coord_type = Coord_Type::RELATIVE;
  } else if ( elem_attr_match ( key_n, String_Id::z ) ) {
    // --- z value
    num_axes = 1;
    axes[ 0 ] = 2;
    coord_type = Coord_Type::ABSOLUTE;
  } else if ( elem_attr_match ( key_n, String_Id::dz ) ) {
    // --- z delta value
    num_axes = 1;
    axes[ 0 ] = 2;
    coord_type = Coord_Type::RELATIVE;
  } else if ( elem_attr_match ( key_n, String_Id::xy ) ) {
    // --- xy value
    num_axes = 2;
    axes[ 0 ] = 0;
    axes[ 1 ] = 1;
    coord_type = Coord_Type::ABSOLUTE;
  } else if ( elem_attr_match ( key_n, String_Id::dxy ) ) {
    // --- xy delta value
    num_axes = 2;
    axes[ 0 ] = 0;
    axes[ 1 ] = 1;
    coord_type = Coord_Type::RELATIVE;
  } else if ( elem_attr_match ( key_n, String_Id::yz ) ) {
    // --- yz value
    num_axes = 2;
    axes[ 0 ] = 1;
    axes[ 1 ] = 2;
    coord_type = Coord_Type::ABSOLUTE;
  } else if ( elem_attr_match ( key_n, String_Id::dyz ) ) {
    // --- yz delta value
    num_axes = 2;
    axes[ 0 ] = 1;
    axes[ 1 ] = 2;
    coord_type = Coord_Type::RELATIVE;
  } else if ( elem_attr_match ( key_n, String_Id::xyz ) ) {
    // --- xyz value
    num_axes = 3;
    axes[ 0 ] = 0;
    axes[ 1 ] = 1;
    axes[ 2 ] = 2;
    coord_type = Coord_Type::ABSOLUTE;
  } else if ( elem_attr_match ( key_n, String_Id::dxyz ) ) {
    // --- xyz delta value
    num_axes = 3;
    axes[ 0 ] = 0;
    axes[ 1 ] = 1;
    axes[ 2 ] = 2;
    coord_type = Coord_Type::RELATIVE;
  } else {
    res = false;
  }

  if ( res ) {
    parse_mpml_positions ( res_coord_n,
                           res_coord_type_n,
                           value_n,
                           axes.begin (),
                           num_axes,
                           coord_type );
  }

  return res;
}

bool
Parser::parse_mpml_positions ( double * res_coord_n,
                               Coord_Type * res_coord_type_n,
                               const QStringRef & value_n,
                               const std::uint8_t * axis_n,
                               std::uint8_t num_axes_n,
                               Coord_Type coord_type_n )
{
  bool res = true;
  DEBUG_ASSERT ( num_axes_n <= 3 );

  // Debug
  if ( auto logo = _log.ostr ( sev::logt::FL_DEBUG_1 ) ) {
    logo << "Reading axis {";
    for ( std::size_t ii = 0; ii != num_axes_n; ++ii ) {
      if ( ii != 0 ) {
        logo << ",";
      }
      logo << axis_n[ ii ];
    }
    logo << "} positions: " << value_n.toString ().toStdString ();
  }
  {
    std::array< double *, 3 > val_ptrs;
    for ( std::size_t ii = 0; ii != num_axes_n; ++ii ) {
      val_ptrs[ ii ] = &( res_coord_n[ axis_n[ ii ] ] );
    }
    parse_value_double_vector ( val_ptrs.begin (), &res, num_axes_n, value_n );
  }
  if ( res ) {
    for ( std::size_t ii = 0; ii != num_axes_n; ++ii ) {
      res_coord_type_n[ axis_n[ ii ] ] = coord_type_n;
    }
  }

  return res;
}

bool
Parser::parse_value_double ( double * value_n, const QStringRef & str_n )
{
  bool success = true;
  const double value = _number_locale.toDouble ( str_n, &success );
  if ( !success ) {
    raise_error ( sev::string::cat ( "Invalid float value: ",
                                     str_n.toString ().toStdString () ) );
    return false;
  }
  *value_n = value;
  return true;
}

void
Parser::parse_value_double_vector ( double ** values_n,
                                    bool * value_valid_n,
                                    std::size_t num_values_n,
                                    const QStringRef & str_n )
{
  bool all_good = true;
  const QChar csep ( ' ' );
  int icur = 0;
  for ( std::size_t ii = 0; ii != num_values_n; ++ii ) {
    // Skip white space
    while ( icur < str_n.size () ) {
      if ( !str_n.at ( icur ).isSpace () ) {
        break;
      }
      ++icur;
    }
    if ( icur < str_n.size () ) {
      int iend = icur + 1;
      // Count non white space
      while ( iend < str_n.size () ) {
        if ( str_n.at ( iend ).isSpace () ) {
          break;
        }
        ++iend;
      }
      // Read double value from substring
      {
        int len ( iend - icur );
        const QStringRef str_num ( str_n.mid ( icur, len ) );
        all_good = parse_value_double ( values_n[ ii ], str_num );
      }
      if ( !all_good ) {
        break;
      }
      icur = iend;
    } else {
      // Not enough values in string
      all_good = false;
      break;
    }
  }

  *value_valid_n = all_good;
}

void
Parser::parse_mpml_attr_cutter_speed ( Cutter_Speed_Type & speed_type_n,
                                       const QStringRef & speed_str_n )
{
  Cutter_Speed_Type speed_type = Cutter_Speed_Type::INVALID;
  if ( elem_attr_match ( speed_str_n, String_Id::spindle_speed_on ) ) {
    speed_type = Cutter_Speed_Type::MATERIAL;
  } else if ( elem_attr_match ( speed_str_n, String_Id::spindle_speed_off ) ) {
    speed_type = Cutter_Speed_Type::OFF;
  } else if ( elem_attr_match ( speed_str_n,
                                String_Id::spindle_speed_stand_by ) ) {
    speed_type = Cutter_Speed_Type::STAND_BY;
  } else if ( elem_attr_match ( speed_str_n,
                                String_Id::spindle_speed_material ) ) {
    speed_type = Cutter_Speed_Type::MATERIAL;
  }

  if ( speed_type == Cutter_Speed_Type::INVALID ) {
    raise_error ( sev::string::cat ( "Invalid cutter speed type: ",
                                     speed_str_n.toString ().toStdString () ) );
    return;
  }

  // Speed type is valid
  speed_type_n = speed_type;
  _log.cat ( sev::logt::FL_DEBUG_1,
             "Found cutter speed type: ",
             speed_str_n.toString ().toStdString () );
}

void
Parser::parse_mpml_elem ()
{
  switch ( _mpml_elem_string->elem_type ) {
  case Element_Type::MOVE:
    parse_mpml_elem_move ();
    break;
  case Element_Type::CIRCLE:
    parse_mpml_elem_circle ();
    break;
  case Element_Type::HELIX:
    parse_mpml_elem_helix ();
    break;
  case Element_Type::CUBIC:
    parse_mpml_elem_cubic ();
    break;
  case Element_Type::SPEED_DB:
    parse_mpml_elem_speed_db ();
    break;
  case Element_Type::SPEED:
    parse_mpml_elem_speed ();
    break;
  case Element_Type::CUTTER:
    parse_mpml_elem_cutter ();
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }
}

void
Parser::parse_mpml_elem_move ()
{
  auto & mpml_elem = _mpml_elem_buffer.create_element< elem::Move > ();

  for ( const auto & item : _xml_stream.attributes () ) {
    const QStringRef & key = item.name ();
    const QStringRef & value = item.value ();

    if ( parse_mpml_position ( &mpml_elem.pos_target[ 0 ],
                               &mpml_elem.pos_target_type[ 0 ],
                               key,
                               value ) ) {
      // No operation
    } else {
      if ( auto logo = _log.ostr ( sev::logt::FL_DEBUG_1 ) ) {
        logo << "Ignoring unknown attribute: " << key.toString ().toStdString ()
             << "=" << value.toString ().toStdString ();
      }
    }
  }
}

void
Parser::parse_mpml_elem_circle ()
{
  auto & mpml_elem = _mpml_elem_buffer.create_element< elem::Circle > ();

  for ( const auto & item : _xml_stream.attributes () ) {
    const QStringRef & key = item.name ();
    const QStringRef & value = item.value ();

    bool attr_used ( true );
    if ( elem_attr_match ( key, String_Id::angle_deg ) ) {
      // --- angle in degrees
      double bval ( 0.0 );
      if ( parse_value_double ( &bval, value ) ) {
        mpml_elem.angle_rad_valid = true;
        mpml_elem.angle_rad = sev::lag::degrees_to_radians ( bval );
      }
    } else if ( elem_attr_match ( key, String_Id::angle_rad ) ) {
      // --- angle in radians
      double bval ( 0.0 );
      if ( parse_value_double ( &bval, value ) ) {
        mpml_elem.angle_rad_valid = true;
        mpml_elem.angle_rad = bval;
      }
    } else {
      // --- center position
      const int pt_pref_size ( _mpml_sdb.str ( String_Id::center ).size () );
      if ( key.size () >= pt_pref_size ) {
        QStringRef key_end;
        {
          const QStringRef key_begin ( key.left ( pt_pref_size ) );
          if ( elem_attr_match ( key_begin, String_Id::center ) ) {
            // use it
          } else {
            attr_used = false;
          }
        }
        if ( key.size () > pt_pref_size ) {
          const int lmin ( pt_pref_size + 1 );
          if ( key.size () > lmin ) {
            if ( key.at ( pt_pref_size ) == QChar ( '_' ) ) {
              // Everything after the separator
              key_end = key.mid ( lmin );
            } else {
              // Invalid separator
              attr_used = false;
            }
          } else {
            // Invalid length
            attr_used = false;
          }
        } else {
          key_end = QStringRef ( &_mpml_sdb.str ( String_Id::xyz ) );
        }
        if ( attr_used ) {
          attr_used = parse_mpml_position ( &mpml_elem.pos_center[ 0 ],
                                            &mpml_elem.pos_center_type[ 0 ],
                                            key_end,
                                            value );
        }
      } else {
        attr_used = false;
      }
    }
    if ( !attr_used ) {
      if ( auto logo = _log.ostr ( sev::logt::FL_DEBUG_1 ) ) {
        logo << "Ignoring unknown attribute: " << key.toString ().toStdString ()
             << "=" << value.toString ().toStdString ();
      }
    }
  }
}

void
Parser::parse_mpml_elem_helix ()
{
  auto & mpml_elem = _mpml_elem_buffer.create_element< elem::Helix > ();

  for ( const auto & item : _xml_stream.attributes () ) {
    const QStringRef & key = item.name ();
    const QStringRef & value = item.value ();

    bool attr_used ( true );
    if ( elem_attr_match ( key, String_Id::angle_deg ) ) {
      // --- angle in degrees
      double bval ( 0.0 );
      if ( parse_value_double ( &bval, value ) ) {
        mpml_elem.angle_rad_valid = true;
        mpml_elem.angle_rad = sev::lag::degrees_to_radians ( bval );
      }
    } else if ( elem_attr_match ( key, String_Id::angle_rad ) ) {
      // --- angle in radians
      double bval ( 0.0 );
      if ( parse_value_double ( &bval, value ) ) {
        mpml_elem.angle_rad_valid = true;
        mpml_elem.angle_rad = bval;
      }
    } else if ( elem_attr_match ( key, String_Id::length ) ) {
      // --- helix length
      double bval ( 0.0 );
      if ( parse_value_double ( &bval, value ) ) {
        mpml_elem.length_valid = true;
        mpml_elem.length = bval;
      }
    } else {
      // --- center position
      const int pt_pref_size ( _mpml_sdb.str ( String_Id::center ).size () );
      if ( key.size () >= pt_pref_size ) {
        QStringRef key_end;
        {
          const QStringRef key_begin ( key.left ( pt_pref_size ) );
          if ( elem_attr_match ( key_begin, String_Id::center ) ) {
            // use it
          } else {
            attr_used = false;
          }
        }
        if ( key.size () > pt_pref_size ) {
          const int lmin ( pt_pref_size + 1 );
          if ( key.size () > lmin ) {
            if ( key.at ( pt_pref_size ) == QChar ( '_' ) ) {
              // Everything after the separator
              key_end = key.mid ( lmin );
            } else {
              // Invalid separator
              attr_used = false;
            }
          } else {
            // Invalid length
            attr_used = false;
          }
        } else {
          key_end = QStringRef ( &_mpml_sdb.str ( String_Id::xyz ) );
        }
        if ( attr_used ) {
          attr_used = parse_mpml_position ( &mpml_elem.pos_center[ 0 ],
                                            &mpml_elem.pos_center_type[ 0 ],
                                            key_end,
                                            value );
        }
      } else {
        attr_used = false;
      }
    }
    if ( !attr_used ) {
      if ( auto logo = _log.ostr ( sev::logt::FL_DEBUG_1 ) ) {
        logo << "Ignoring unknown attribute: " << key.toString ().toStdString ()
             << "=" << value.toString ().toStdString ();
      }
    }
  }
}

void
Parser::parse_mpml_elem_cubic ()
{
  auto & mpml_elem = _mpml_elem_buffer.create_element< elem::Cubic > ();

  for ( const auto & item : _xml_stream.attributes () ) {
    const QStringRef & key = item.name ();
    const QStringRef & value = item.value ();

    bool attr_used ( true );
    {
      const int pt_pref_size ( _mpml_sdb.str ( String_Id::pt1 ).size () );
      if ( key.size () >= pt_pref_size ) {
        std::size_t pt_index ( 0 );
        QStringRef key_end;
        {
          const QStringRef key_begin ( key.left ( pt_pref_size ) );
          if ( elem_attr_match ( key_begin, String_Id::pt1 ) ) {
            pt_index = 0;
          } else if ( elem_attr_match ( key_begin, String_Id::pt2 ) ) {
            pt_index = 1;
          } else if ( elem_attr_match ( key_begin, String_Id::pt3 ) ) {
            pt_index = 2;
          } else {
            attr_used = false;
          }
        }
        if ( key.size () > pt_pref_size ) {
          const int lmin ( pt_pref_size + 1 );
          if ( key.size () > lmin ) {
            if ( key.at ( pt_pref_size ) == QChar ( '_' ) ) {
              // Everything after the separator
              key_end = key.mid ( lmin );
            } else {
              // Invalid separator
              attr_used = false;
            }
          } else {
            // Invalid length
            attr_used = false;
          }
        } else {
          key_end = QStringRef ( &_mpml_sdb.str ( String_Id::xyz ) );
        }
        if ( attr_used ) {
          attr_used =
              parse_mpml_position ( &( mpml_elem.pos[ pt_index ][ 0 ] ),
                                    &( mpml_elem.pos_type[ pt_index * 3 ] ),
                                    key_end,
                                    value );
        }
      } else {
        attr_used = false;
      }
    }
    if ( !attr_used ) {
      if ( auto logo = _log.ostr ( sev::logt::FL_DEBUG_1 ) ) {
        logo << "Ignoring unknown attribute: " << key.toString ().toStdString ()
             << "=" << value.toString ().toStdString ();
      }
    }
  }
}

void
Parser::parse_mpml_elem_speed_db ()
{
  auto & mpml_elem = _mpml_elem_buffer.create_element< elem::Speed_Db > ();

  bool type_found = false;
  bool value_found = false;

  for ( const auto & item : _xml_stream.attributes () ) {
    const QStringRef & key = item.name ();
    const QStringRef & value = item.value ().trimmed ();

    if ( elem_attr_match ( key, String_Id::speed_type ) ) {

      // Identify speed type
      if ( elem_attr_match ( value, String_Id::speed_db_rapid ) ) {
        type_found = true;
        mpml_elem.speed_type = Speed_Db_Type::RAPID;
        _log.cat ( sev::logt::FL_DEBUG_1, "Speed db. rapid speed found" );
      } else if ( elem_attr_match ( value, String_Id::speed_db_material ) ) {
        type_found = true;
        mpml_elem.speed_type = Speed_Db_Type::MATERIAL;
        _log.cat ( sev::logt::FL_DEBUG_1, "Speed db. material speed found" );
      } else if ( elem_attr_match ( value,
                                    String_Id::speed_db_material_planar ) ) {
        type_found = true;
        mpml_elem.speed_type = Speed_Db_Type::MATERIAL_PLANAR;
        _log.cat ( sev::logt::FL_DEBUG_1,
                   "Speed db. material planar speed found" );
      } else if ( elem_attr_match ( value,
                                    String_Id::speed_db_material_normal ) ) {
        type_found = true;
        mpml_elem.speed_type = Speed_Db_Type::MATERIAL_NORMAL;
        _log.cat ( sev::logt::FL_DEBUG_1,
                   "Speed db. material normal speed found" );
      } else {
        raise_error ( sev::string::cat ( "Speed db. Invalid speed typed type: ",
                                         value.toString ().toStdString () ) );
      }
    } else if ( elem_attr_match ( key, String_Id::speed_value ) ) {
      double bval = 0.0;
      if ( parse_value_double ( &bval, value ) ) {
        value_found = true;
        mpml_elem.speed_value = bval;
      } else {
        raise_error ( sev::string::cat ( "Speed db. Bad value: ",
                                         value.toString ().toStdString () ) );
      }
    } else {
      if ( auto logo = _log.ostr ( sev::logt::FL_DEBUG_1 ) ) {
        logo << "Ignoring unknown attribute: " << key.toString ().toStdString ()
             << "=" << value.toString ().toStdString ();
      }
    }
  }

  if ( !type_found ) {
    raise_error ( "Speed db: No type found." );
  }
  if ( !value_found ) {
    raise_error ( "Speed db: No value found." );
  }
}

void
Parser::parse_mpml_elem_speed ()
{
  auto & mpml_elem = _mpml_elem_buffer.create_element< elem::Speed > ();

  for ( const auto & item : _xml_stream.attributes () ) {
    const QStringRef & key = item.name ();
    const QStringRef & value = item.value ().trimmed ();

    if ( elem_attr_match ( key, String_Id::speed_type ) ) {

      // Identify speed type
      if ( elem_attr_match ( value, String_Id::curve_speed_rapid ) ) {
        mpml_elem.speed_type = Speed_Type::RAPID;
        // Debug
        _log.cat ( sev::logt::FL_DEBUG_1, "Rapid curve speed found\n" );
      } else if ( elem_attr_match ( value, String_Id::curve_speed_material ) ) {
        mpml_elem.speed_type = Speed_Type::MATERIAL;
        // Debug
        _log.cat ( sev::logt::FL_DEBUG_1, "Material curve speed found\n" );
      } else {
        raise_error ( sev::string::cat ( "Invalid curve speed type: ",
                                         value.toString ().toStdString () ) );
      }

    } else {
      if ( auto logo = _log.ostr ( sev::logt::FL_DEBUG_1 ) ) {
        logo << "Ignoring unknown attribute: " << key.toString ().toStdString ()
             << "=" << value.toString ().toStdString ();
      }
    }
  }
}

void
Parser::parse_mpml_elem_cutter ()
{
  auto & mpml_elem = _mpml_elem_buffer.create_element< elem::Cutter > ();

  for ( const auto & item : _xml_stream.attributes () ) {
    const QStringRef & key = item.name ();
    const QStringRef & value = item.value ().trimmed ();

    if ( elem_attr_match ( key, String_Id::spindle_speed ) ) {
      // --- Speed value
      parse_mpml_attr_cutter_speed ( mpml_elem.speed_type, value );
    } else {
      if ( auto logo = _log.ostr ( sev::logt::FL_DEBUG_1 ) ) {
        logo << "Ignoring unknown attribute: " << key.toString ().toStdString ()
             << "=" << value.toString ().toStdString ();
      }
    }
  }
}

} // namespace import::mpml
