/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <import/mpml/types.hpp>
#include <sev/assert.hpp>
#include <QString>
#include <array>
#include <cstdint>

namespace import::mpml
{

enum class String_Id
{
  mpml,

  speed_db_rapid,
  speed_db_material,
  speed_db_material_planar,
  speed_db_material_normal,

  curve_speed_rapid,
  curve_speed_material,

  spindle_speed,
  spindle_speed_on,
  spindle_speed_off,
  spindle_speed_stand_by,
  spindle_speed_material,

  x,
  y,
  z,
  xy,
  yz,
  xyz,

  dx,
  dy,
  dz,
  dxy,
  dyz,
  dxyz,

  axis,
  center,
  angle_deg,
  angle_rad,
  length,

  pt1,
  pt2,
  pt3,

  speed_type,
  speed_value,

  ID_LIST_END
};

const std::size_t num_string_ids =
    static_cast< std::size_t > ( String_Id::ID_LIST_END );

class Elem_String
{
  public:
  // -- Construction

  Elem_String ()
  : elem_type ( Element_Type::INVALID )
  {
  }

  Elem_String ( const QString & elem_name_n, Element_Type elem_type_n )
  : elem_name ( elem_name_n )
  , elem_type ( elem_type_n )
  {
  }

  public:
  // -- Attributes
  QString elem_name;
  Element_Type elem_type;
};

class String_Database
{
  public:
  // -- Construction

  String_Database ();

  // -- Interface

  const QString &
  str ( String_Id id_n ) const
  {
    std::size_t index = static_cast< std::size_t > ( id_n );
    DEBUG_ASSERT ( index < import::mpml::num_string_ids );
    return _strings[ index ];
  }

  const Elem_String &
  str_elem ( Element_Type type_n ) const
  {
    std::size_t type_index = static_cast< std::size_t > ( type_n );
    DEBUG_ASSERT ( type_index < import::mpml::num_element_types );
    return _strings_elem[ type_index ];
  }

  private:
  // -- Utility

  void
  set_str ( String_Id id_n, const char * str_n );

  void
  set_str_elem ( Element_Type type_n, const char * str_n );

  private:
  // -- Attributes
  std::array< QString, import::mpml::num_string_ids > _strings;
  std::array< Elem_String, import::mpml::num_element_types > _strings_elem;
};

} // namespace import::mpml
