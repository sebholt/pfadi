/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <import/mpml/element_buffer.hpp>
#include <import/mpml/elements.hpp>
#include <import/mpml/string_database.hpp>
#include <sev/lag/vector3.hpp>
#include <sev/logt/context.hpp>
#include <QByteArray>
#include <QLocale>
#include <QXmlStreamReader>
#include <cstdint>
#include <string>

namespace import::mpml
{

/// @brief Reads a MPML file
///
class Parser
{
  public:
  // -- Types

  /// @brief Public state token
  enum class Token
  {
    NONE,  /// @brief Not reading
    ERROR, /// @brief Error locked
    FEED,  /// @brief Feed new data

    BEGIN,           /// @brief MPML begin found
    ELEMENT,         /// @brief MPML element found
    UNKNOWN_ELEMENT, /// @brief Unknown XML element found
    END,             /// @brief MPML end reached
    DONE             /// @brief Reading complete. Call clear().
  };

  /// @brief Parser state
  enum class PState
  {
    NONE,
    ERROR,
    FIND_MPML,
    ELEMENTS,
    DONE
  };

  /// @brief XML parser state
  enum class XState
  {
    NONE,
    FEED,
    READ
  };

  // -- Construction

  Parser ( const sev::logt::Reference & log_parent_n );

  // -- Parser state

  bool
  is_reading () const
  {
    return ( _pstate != PState::NONE );
  }

  const Element &
  element () const
  {
    return _mpml_elem_buffer.element ();
  }

  /// @brief Name of the last found unknown XML element
  const std::string &
  unknown_element () const;

  /// @brief Line number of the last event
  std::int64_t
  line () const
  {
    return _xml_stream.lineNumber ();
  }

  /// @brief Column number of the last event
  std::int64_t
  columnt () const
  {
    return _xml_stream.columnNumber ();
  }

  /// @brief Signalizes that there was an error
  bool
  error () const
  {
    return ( _pstate == PState::ERROR );
  }

  /// @brief Error text
  const std::string &
  error_message () const
  {
    return _error_message;
  }

  // -- Parser control

  void
  begin ();

  void
  reset ();

  void
  feed ( const char * data_n, std::size_t size_n );

  Token
  read ();

  private:
  // -- Utility

  void
  xml_process ();

  void
  xml_read ();

  void
  xml_read_error ();

  void
  raise_xml_error ();

  void
  raise_error ( std::string_view error_n );

  void
  cb_document_begin ();

  void
  cb_document_end ();

  void
  cb_element_begin ();

  void
  cb_element_end ();

  bool
  elem_name_match ( const QStringRef & name0_n, const QString & name1_n ) const;

  bool
  elem_name_match ( const QStringRef & name0_n, String_Id id_n ) const;

  bool
  elem_attr_match ( const QStringRef & name0_n, String_Id id_n ) const;

  bool
  parse_mpml_position ( double * res_coord_n,
                        Coord_Type * res_coord_type_n,
                        const QStringRef & key_n,
                        const QStringRef & value_n );

  bool
  parse_mpml_positions ( double * res_coord_n,
                         Coord_Type * res_coord_type_n,
                         const QStringRef & value_n,
                         const std::uint8_t * axis_n,
                         std::uint8_t num_axes_n,
                         Coord_Type coord_type_n );

  bool
  parse_value_double ( double * value_n, const QStringRef & str_n );

  void
  parse_value_double_vector ( double ** values_n,
                              bool * values_valid_n,
                              std::size_t num_values_n,
                              const QStringRef & str_n );

  void
  parse_mpml_attr_cutter_speed ( Cutter_Speed_Type & speed_type_n,
                                 const QStringRef & speed_str_n );

  void
  parse_mpml_elem ();

  /// @brief Move element
  void
  parse_mpml_elem_move ();

  /// @brief Circle element
  void
  parse_mpml_elem_circle ();

  /// @brief Helix element
  void
  parse_mpml_elem_helix ();

  /// @brief Cubic element
  void
  parse_mpml_elem_cubic ();

  /// @brief Speed Database element
  void
  parse_mpml_elem_speed_db ();

  /// @brief Speed element
  void
  parse_mpml_elem_speed ();

  /// @brief Cutter spindle element
  void
  parse_mpml_elem_cutter ();

  private:
  // -- Attributes
  /// @brief Logging context
  sev::logt::Context _log;

  Token _token = Token::NONE;
  PState _pstate = PState::NONE;
  XState _xstate = XState::NONE;
  std::size_t _depth = 0;
  std::size_t _depth_unknown = 0;

  std::string _error_message;
  std::string _unknown_element;

  /// @brief Current mpml/xml element
  const Elem_String * _mpml_elem_string = nullptr;
  /// @brief Anonymous construction space for Element instances
  Element_Buffer _mpml_elem_buffer;
  /// @brief locale to convert strings into numbers
  QLocale _number_locale = QLocale::C;
  /// @brief Database of strings that appear in an MPML file
  String_Database _mpml_sdb;
  /// @brief XML parser
  QXmlStreamReader _xml_stream;
};

} // namespace import::mpml
