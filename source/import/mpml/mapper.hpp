/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <import/config.hpp>
#include <import/mpml/element.hpp>
#include <sev/lag/vector3.hpp>
#include <sev/logt/context.hpp>
#include <sev/mem/ring_static.hpp>
#include <snc/mpath/curve_evaluator_buffer_d.hpp>
#include <snc/mpath/d3/types.hpp>
#include <snc/mpath/element_buffer_d.hpp>
#include <string>

namespace import::mpml
{

/// @brief Maps mpml elements into mpath elements
///
class Mapper
{
  public:
  // -- Types

  /// @brief Public state token
  enum class Token
  {
    NONE,    /// @brief Feed and mpml element
    ERROR,   /// @brief Error locked
    ELEMENT, /// @brief MPath element ready
    DONE     /// @brief No more elements
  };

  // -- Construction

  Mapper ( const sev::logt::Reference & log_parent_n );

  // -- MPath configuration

  const import::Config &
  config () const
  {
    return _config;
  }

  // -- Mapping state

  const snc::mpath::Element *
  element () const
  {
    return &( _mpath_rbuffer.front ().elem () );
  }

  bool
  error () const
  {
    return !_error_message.empty ();
  }

  const std::string &
  error_message () const
  {
    return _error_message;
  }

  bool
  is_mapping () const
  {
    return _active;
  }

  // -- Mapping control

  void
  begin ( const import::Config & config_n );

  void
  reset ();

  void
  feed ( const Element * element_n );

  /// @brief Call when no more elements are available
  void
  feed_end ();

  Token
  read ();

  void
  element_pop ()
  {
    _mpath_rbuffer.pop_front_not_empty ();
  }

  private:
  // -- Utility

  void
  raise_error ( std::string_view error_n );

  template < class T, typename... Args >
  T &
  mpath_emplace ( Args &&... args_n );

  void
  acquire_axis_coord ( std::size_t axis_index_n,
                       double * res_n,
                       double val_input_n,
                       Coord_Type coord_type_n );

  void
  acquire_pos_current ( snc::mpath::elem::Curve_D< 3 > & curve_n );

  void
  compile_mpath_element ();

  void
  compile_mpath_elem_move ();

  void
  compile_mpath_elem_circle ();

  void
  compile_mpath_elem_helix ();

  void
  compile_mpath_elem_cubic ();

  void
  compile_mpath_elem_speed_db ();

  void
  compile_mpath_elem_speed ();

  void
  compile_mpath_elem_cutter ();

  private:
  sev::logt::Context _log;

  bool _active = false;
  bool _element_feed = false;
  import::Config _config;
  std::string _error_message;

  /// @brief Latest mpml element
  const Element * _mpml_element = nullptr;

  /// @brief Current position tracker
  sev::lag::Vector3d _pos_current;
  /// @brief Current position validity tracker
  std::array< bool, 3 > _pos_current_valid;
  // @brief Flags if the last element was a curve element
  bool _inside_curve_stream = false;

  /// @brief mpath element construction buffer
  sev::mem::Ring_Static< snc::mpath::Element_Buffer_D< 3 >, 4 > _mpath_rbuffer;
  /// @brief MPath curve evaluator buffer
  snc::mpath::Curve_Evaluator_Buffer_D3 _eval_buffer;
};

} // namespace import::mpml
