/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <import/mpml/types.hpp>
#include <cstdint>

namespace import::mpml
{

class Element
{
  public:
  Element () = default;

  import::mpml::Element_Type
  elem_type () const
  {
    return _elem_type;
  }

  protected:
  Element ( import::mpml::Element_Type elem_type_n )
  : _elem_type ( elem_type_n )
  {
  }

  private:
  import::mpml::Element_Type _elem_type = import::mpml::Element_Type::INVALID;
};

} // namespace import::mpml
