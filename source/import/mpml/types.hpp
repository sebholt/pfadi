/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <QList>
#include <QString>
#include <array>
#include <cstdint>

namespace import::mpml
{

enum class Element_Type
{
  INVALID,

  MOVE,
  HELIX,
  CIRCLE,
  CUBIC,
  SPEED_DB,
  SPEED,

  CUTTER,

  TYPE_LIST_END
};

const std::size_t num_element_types =
    static_cast< std::size_t > ( Element_Type::TYPE_LIST_END );

enum class Coord_Type
{
  INVALID,
  ABSOLUTE,
  RELATIVE
};

enum class Speed_Db_Type
{
  INVALID,
  RAPID,
  MATERIAL,
  MATERIAL_PLANAR,
  MATERIAL_NORMAL
};

enum class Speed_Type
{
  INVALID,
  RAPID,
  MATERIAL
};

enum class Cutter_Speed_Type
{
  INVALID,
  OFF,
  STAND_BY,
  MATERIAL
};

} // namespace import::mpml
