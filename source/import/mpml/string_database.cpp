/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#include "string_database.hpp"
#include <import/mpml/types.hpp>

namespace import::mpml
{

String_Database::String_Database ()
{
  set_str ( String_Id::mpml, "mpml" );

  set_str ( String_Id::speed_db_rapid, "rapid" );
  set_str ( String_Id::speed_db_material, "material" );
  set_str ( String_Id::speed_db_material_planar, "material_planar" );
  set_str ( String_Id::speed_db_material_normal, "material_normal" );

  set_str ( String_Id::curve_speed_rapid, "rapid" );
  set_str ( String_Id::curve_speed_material, "material" );

  set_str ( String_Id::spindle_speed, "speed" );
  set_str ( String_Id::spindle_speed_on, "on" );
  set_str ( String_Id::spindle_speed_off, "off" );
  set_str ( String_Id::spindle_speed_stand_by, "stand_by" );
  set_str ( String_Id::spindle_speed_material, "material" );

  set_str ( String_Id::x, "x" );
  set_str ( String_Id::y, "y" );
  set_str ( String_Id::z, "z" );
  set_str ( String_Id::xy, "xy" );
  set_str ( String_Id::yz, "yz" );
  set_str ( String_Id::xyz, "xyz" );

  set_str ( String_Id::dx, "dx" );
  set_str ( String_Id::dy, "dy" );
  set_str ( String_Id::dz, "dz" );
  set_str ( String_Id::dxy, "dxy" );
  set_str ( String_Id::dyz, "dyz" );
  set_str ( String_Id::dxyz, "dxyz" );

  set_str ( String_Id::axis, "axis" );
  set_str ( String_Id::center, "center" );
  set_str ( String_Id::angle_deg, "angle_deg" );
  set_str ( String_Id::angle_rad, "angle_rad" );
  set_str ( String_Id::length, "length" );
  set_str ( String_Id::pt1, "pt1" );
  set_str ( String_Id::pt2, "pt2" );
  set_str ( String_Id::pt3, "pt3" );

  set_str ( String_Id::speed_type, "type" );
  set_str ( String_Id::speed_value, "value" );

  set_str_elem ( Element_Type::MOVE, "move" );
  set_str_elem ( Element_Type::HELIX, "helix" );
  set_str_elem ( Element_Type::CIRCLE, "circle" );
  set_str_elem ( Element_Type::CUBIC, "cubic" );
  set_str_elem ( Element_Type::SPEED_DB, "speed_db" );
  set_str_elem ( Element_Type::SPEED, "speed" );
  set_str_elem ( Element_Type::CUTTER, "cutter" );
}

void
String_Database::set_str ( String_Id id_n, const char * str_n )
{
  std::size_t index = static_cast< std::size_t > ( id_n );
  _strings.at ( index ) = QString::fromUtf8 ( str_n );
}

void
String_Database::set_str_elem ( Element_Type type_n, const char * str_n )
{
  std::size_t type_index = static_cast< std::size_t > ( type_n );
  auto & entry = _strings_elem.at ( type_index );
  entry.elem_name = QString::fromUtf8 ( str_n );
  entry.elem_type = type_n;
}

} // namespace import::mpml
