/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#include "data_streamer.hpp"

namespace import
{

Data_Streamer::Data_Streamer ( sev::logt::Context log_n )
: _log ( std::move ( log_n ) )
{
}

Data_Streamer::~Data_Streamer () {}

void
Data_Streamer::begin ()
{
  _good = true;
}

void
Data_Streamer::end ()
{
  _good = false;
}

void
Data_Streamer::feed_end_good ()
{
  log ().cat ( sev::logt::FL_DEBUG, "feed_end_good" );
  _good = false;
}

void
Data_Streamer::feed_end_error ( std::string error_n )
{
  log ().cat ( sev::logt::FL_DEBUG, "feed_end_error: ", error_n );
  _error_message = std::move ( error_n );
  _good = false;
}

} // namespace import
