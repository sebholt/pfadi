/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#include "Dash.hpp"
#include <sev/utility.hpp>
#include <snc/emb/config/config.hpp>
#include <snc/math/stepping.hpp>
#include <snc/usb/device_query.hpp>
#include <QFile>

namespace dash
{

AxisCommon::AxisCommon ( snc::svp::dash::Dash & dash_n,
                         int index_n,
                         snc::device::qt::Info * info_n )
: QObject ( &dash_n )
, _index ( index_n )
, _machineInfo ( info_n )
{
  updateAxisInfo ();
}

void
AxisCommon::updateAxisInfo ()
{
  if ( sev::change ( _info,
                     _machineInfo->axes ()->getOrCreateShared ( _index ) ) ) {
    emit infoChanged ();
  }
}

} // namespace dash

Dash::Dash ( const sev::logt::Reference & log_parent_n,
             sev::thread::Tracker * thread_tracker_n,
             QObject * qparent_n )
: Super ( { log_parent_n, "Dash" }, thread_tracker_n, qparent_n )
, _lookout ( *this )
, _autostart ( *this )
, _serial_device ( *this )
, _tracker ( *this )
, _pilot_machine_startup ( *this )
, _pilot_machine_res ( *this )
, _pilot_controls ( *this )
, _pilot_mpath ( *this )
, _controls ( *this )
, _axis_x ( *this, 0, _tracker.info () )
, _axis_y ( *this, 1, _tracker.info () )
, _axis_z ( *this, 2, _tracker.info () )
, _align_multi ( *this )
, _mpath_loader ( *this )
, _mpath_painter ( *this )
, _mpath_transform ( *this )
, _program ( *this )
{
}

Dash::~Dash () = default;

void
Dash::session_begin ()
{
  Super::session_begin ();

  // Setup device configurations
  {
    QFile jsonFile ( ":/devices/Haase_CUT2500S_emb.json" );
    DEBUG_ASSERT ( jsonFile.exists () );
    if ( jsonFile.open ( QIODevice::ReadOnly ) ) {
      qint64 maxSize = 1024 * 1024 * 10;
      QByteArray data = jsonFile.read ( maxSize );
      DEBUG_ASSERT ( !data.isEmpty () );
      _autostart.set_default_device_statics_json ( data.toStdString () );
    } else {
      DEBUG_ASSERT ( false );
    }
  }
  {
    QFile jsonFile ( ":/devices/Haase_CUT2500S_est.json" );
    DEBUG_ASSERT ( jsonFile.exists () );
    if ( jsonFile.open ( QIODevice::ReadOnly ) ) {
      qint64 maxSize = 1024 * 1024 * 10;
      QByteArray data = jsonFile.read ( maxSize );
      DEBUG_ASSERT ( !data.isEmpty () );
      _autostart.add_device_statics_json ( data.toStdString () );
    } else {
      DEBUG_ASSERT ( false );
    }
  }
}
