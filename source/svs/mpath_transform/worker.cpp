/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#include "worker.hpp"
#include <sev/assert.hpp>
#include <snc/mpath/d3/types.hpp>
#include <snc/svs/frame/async_proc/worker_t.ipp>
#include <svs/mpath_transform/types.hpp>

// -- Instantiation
namespace snc::svs::frame::async_proc
{
template class Worker_T< ::svs::mpath_transform::Types >;
} // namespace snc::svs::frame::async_proc

namespace svs::mpath_transform
{

Worker::Worker ( const sev::logt::Reference & log_parent_n,
                 const sev::event::queue_io::Link_2 & link_n,
                 std::shared_ptr< const Request_Base > request_n )
: Super ( { log_parent_n, "Worker" }, link_n, std::move ( request_n ) )
{
}

Worker::~Worker () = default;

void
Worker::feed_begin ()
{
  _translation = request ().translation;

  _mpath_stream_read.set_buffer ( request ().mpath.get () );
  _mpath_stream_read.open ();

  _mpath_transformed = std::make_shared< snc::mpath_stream::Buffer > ();
  _mpath_stream_write_buffer.set_buffer ( _mpath_transformed.get () );

  _mpath_stream_tee.set_ostream ( 0, &_mpath_stream_write_stats );
  _mpath_stream_tee.set_ostream ( 1, &_mpath_stream_write_buffer );
  _mpath_stream_tee.open ();
}

void
Worker::feed_end ( Result::End end_n, std::string_view error_n )
{
  _mpath_stream_read.close ();
  _mpath_stream_read.set_buffer ( nullptr );

  _mpath_stream_tee.close ();
  _mpath_stream_write_buffer.set_buffer ( nullptr );

  // Update result
  result ().end = end_n;
  result ().error_message.assign ( error_n );
  if ( result ().good () ) {
    result ().mpath = _mpath_transformed;
  }
  Super::feed_end ();
}

void
Worker::feed_abort ()
{
  log ().cat ( sev::logt::FL_DEBUG_0, "Incoming abort event." );
  feed_end ( Result::End::ABORTED );
}

void
Worker::feed ()
{
  std::size_t num_remain = 1024;
  while ( _mpath_stream_read.is_good () && ( num_remain != 0 ) ) {
    --num_remain;

    snc::mpath::Element & elem = _ebuffer.copy ( *_mpath_stream_read.read () );
    if ( snc::mpath::d3::elem::type_is_curve ( elem.elem_type () ) ) {
      auto & curve = static_cast< snc::mpath::elem::Curve_D< 3 > & > ( elem );
      snc::mpath::elem::curve_translate_d ( curve, _translation );
    }
    _mpath_stream_tee.write ( elem );
  }

  if ( _mpath_stream_read.is_good () ) {
    progress ().mpath_stats = _mpath_stream_write_stats.stats ();
  } else {
    result ().mpath_stats = _mpath_stream_write_stats.stats ();
    feed_end ( Result::End::GOOD );
  }
}

} // namespace svs::mpath_transform
