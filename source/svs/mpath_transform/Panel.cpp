/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#include "Panel.hpp"
#include <sev/utility.hpp>
#include <snc/svs/frame/async_proc/clerk_t.ipp>
#include <snc/svs/frame/async_proc/service_t.ipp>
#include <svs/mpath_transform/worker.hpp>

// -- Instantiation
namespace snc::svs::frame::async_proc
{
template class Service_T< ::svs::mpath_transform::Worker >;
template class Clerk_T< Service_T< ::svs::mpath_transform::Worker > >;
} // namespace snc::svs::frame::async_proc

namespace svs::mpath_transform
{

Panel::Panel ( const Panel_Init & init_n )
: Super ( init_n, "MPath-Transform" )
{
}

Panel::~Panel () = default;

Panel::Clerk_Factory_Handle
Panel::office_session_begin_factory ()
{
  return make_clerk_factory<
      snc::svs::frame::async_proc::Clerk_T<
          snc::svs::frame::async_proc::Service_T< Worker > >,
      std::string > ( log ().context_name () );
}

} // namespace svs::mpath_transform
