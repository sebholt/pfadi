/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <sev/lag/vector3.hpp>
#include <snc/mpath_stream/buffer.hpp>
#include <snc/mpath_stream/statistics_d.hpp>
#include <snc/svs/frame/async_proc/types.hpp>
#include <memory>
#include <string>

namespace svs::mpath_transform
{

class Request : public snc::svs::frame::async_proc::Request_Base
{
  public:
  std::shared_ptr< const snc::mpath_stream::Buffer > mpath;
  sev::lag::Vector3d translation;
};

class Progress : public snc::svs::frame::async_proc::Progress_Base
{
  public:
  snc::mpath_stream::Statistics_D3 mpath_stats;
};

class Result : public snc::svs::frame::async_proc::Result_Base
{
  public:
  std::shared_ptr< snc::mpath_stream::Buffer > mpath;
  snc::mpath_stream::Statistics_D3 mpath_stats;
};

struct Types
{
  using Request = svs::mpath_transform::Request;
  using Progress = svs::mpath_transform::Progress;
  using Result = svs::mpath_transform::Result;
};

} // namespace svs::mpath_transform
