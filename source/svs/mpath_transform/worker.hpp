/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <sev/lag/vector3.hpp>
#include <snc/mpath_stream/read/buffer.hpp>
#include <snc/mpath_stream/write/buffer.hpp>
#include <snc/mpath_stream/write/statistics_d.hpp>
#include <snc/mpath_stream/write/tee.hpp>
#include <snc/svs/frame/async_proc/worker_t.hpp>
#include <svs/mpath_transform/types.hpp>
#include <string_view>

namespace svs::mpath_transform
{

class Worker : public snc::svs::frame::async_proc::Worker_T< Types >
{
  // -- Types
  private:
  using Super = snc::svs::frame::async_proc::Worker_T< Types >;

  public:
  // -- Construction

  Worker ( const sev::logt::Reference & log_parent_n,
           const sev::event::queue_io::Link_2 & link_n,
           std::shared_ptr< const Request_Base > request_n );

  ~Worker ();

  private:
  // -- Interface implementations

  void
  feed_begin () override;

  void
  feed_end ( Result::End end_n, std::string_view error_n = {} );

  void
  feed_abort () override;

  void
  feed () override;

  private:
  sev::lag::Vector3d _translation;
  std::shared_ptr< snc::mpath_stream::Buffer > _mpath_transformed;
  snc::mpath_stream::read::Buffer _mpath_stream_read;
  snc::mpath_stream::write::Buffer _mpath_stream_write_buffer;
  snc::mpath_stream::write::Statistics_D3 _mpath_stream_write_stats;
  snc::mpath_stream::write::Tee2 _mpath_stream_tee;
  snc::mpath::Element_Buffer_D< 3 > _ebuffer;
};

} // namespace svs::mpath_transform
