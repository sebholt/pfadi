/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <mpath_stream/read/go_to.hpp>
#include <mpath_stream/read/speed_insert.hpp>
#include <sev/event/pool.hpp>
#include <sev/event/timer_queue/chrono/client.hpp>
#include <sev/mem/flags.hpp>
#include <snc/device/handle.hpp>
#include <snc/mpath_stream/buffer.hpp>
#include <snc/mpath_stream/read/buffer.hpp>
#include <snc/svp/office/clerk.hpp>
#include <snc/svs/ctl/actor/office/client_actor.hpp>
#include <snc/svs/fac/mpath/office/client_n.hpp>
#include <svs/mpath_loader/client.hpp>
#include <svs/mpath_painter/client.hpp>
#include <svs/mpath_transform/client.hpp>
#include <svs/program/dash/events.hpp>
#include <svs/program/office/state.hpp>
#include <memory>

namespace svs::program::office
{

class Clerk : public snc::svp::office::Clerk
{
  // -- Types
  private:
  using Super = snc::svp::office::Clerk;

  public:
  struct Dash_Message
  {
    static constexpr std::uint8_t STATE = ( 1 << 0 );
  };

  struct File_Request
  {
    static constexpr std::uint8_t LOAD = ( 1 << 0 );
    static constexpr std::uint8_t PAINT = ( 1 << 1 );
  };

  struct Transform_Request
  {
    static constexpr std::uint8_t TRANSFORM = ( 1 << 0 );
  };

  struct Route_Request
  {
    static constexpr std::uint8_t BEGIN = ( 1 << 0 );
    static constexpr std::uint8_t BREAK = ( 1 << 1 );
  };

  using Stream_Select = State::Stream_Select;
  using Run_State = MPath_Job_State::Run_State;
  using Run_Modifier = MPath_Job_State::Run_Modifier;

  // -- Construction

  Clerk ( const Clerk_Init & init_n );

  ~Clerk ();

  // -- Session interface

  void
  cell_session_begin () override;

  void
  cell_session_abort () override;

  bool
  cell_session_is_ready_to_end () const override;

  // -- Dash event processing

  void
  dash_event ( Dash_Event & event_n ) override;

  void
  dash_event_path_file ( Dash_Event & event_n );

  void
  dash_event_transform ( Dash_Event & event_n );

  void
  dash_event_transform_axis ( Dash_Event & event_n );

  void
  dash_event_speeds ( Dash_Event & event_n );

  void
  dash_event_speed ( Dash_Event & event_n );

  void
  dash_event_route_begin ( Dash_Event & event_n );

  void
  dash_event_route_abort ( Dash_Event & event_n );

  void
  dash_notify () override;

  // -- Central processing

  void
  process_async () override;

  void
  process () override;

  protected:
  // -- Utility

  void
  sanitize_speeds ();

  void
  request_front_message_state ();

  void
  state_update ();

  void
  route_begin ();

  void
  route_break ();

  void
  route_stream_start ();

  void
  route_stream_finish ();

  private:
  // State
  snc::svs::ctl::actor::office::Client_Actor _actor;
  State _state;

  // Requests
  sev::mem::Flags_Fast8 _file_requests;
  sev::mem::Flags_Fast8 _transform_requests;
  sev::mem::Flags_Fast8 _route_requests;

  // MPath
  std::shared_ptr< snc::mpath_stream::Buffer > _mpath;
  std::shared_ptr< snc::mpath_stream::Buffer > _mpath_transformed;

  // MPath approach / retreat
  mpath_stream::read::Go_To _mpath_stream_go_to;
  snc::mpath::Speed_Database _speed_db_approach;
  sev::lag::Vector3d _retreat_pos;

  // Sanitizer stream
  snc::mpath_stream::read::Buffer _mpath_transformed_stream;
  std::shared_ptr< mpath_stream::read::Speed_Insert3 >
      _mpath_stream_speed_insert;

  // Loading
  svs::mpath_loader::Client _mpath_loader;
  svs::mpath_loader::Client::Control _mpath_loader_control;
  svs::mpath_painter::Client _mpath_painter;
  svs::mpath_painter::Client::Control _mpath_painter_control;
  svs::mpath_transform::Client _mpath_transform;
  svs::mpath_transform::Client::Control _mpath_transform_control;

  // -- Transforming
  Transform _requested_transform;

  // -- Remote controller
  snc::svs::fac::mpath::office::Client_N< 3 > _mpath_pilot;
  snc::svs::fac::mpath::office::Client_N< 3 >::Control _mpath_pilot_control;

  // -- State update timer
  sev::event::timer_queue::chrono::Client_Steady _state_update_timer;

  // -- Dash event io
  sev::event::Pool< dash::event::in::State > _event_pool_program_state;
};

} // namespace svs::program::office
