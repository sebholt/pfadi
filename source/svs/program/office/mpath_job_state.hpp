/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <sev/chrono/steady_system.hpp>
#include <sev/mem/flags.hpp>
#include <array>
#include <chrono>
#include <cstdint>

namespace svs::program::office
{

class MPath_Job_State
{
  public:
  // -- Types

  using Time_Point = sev::chrono::Steady_System;

  enum class Run_State : std::uint8_t
  {
    NONE,
    SCHEDULED,
    PREPARING,
    RUNNING,
    FINISHED
  };

  struct Run_Modifier
  {
    static constexpr std::uint8_t NONE = ( 1 << 0 );
    static constexpr std::uint8_t SKIP = ( 1 << 1 );
    static constexpr std::uint8_t ABORT = ( 1 << 2 );
  };

  // -- Construction and setup

  MPath_Job_State () = default;

  ~MPath_Job_State () = default;

  void
  reset ();

  // -- Run state

  Run_State
  run_state () const
  {
    return _run_state;
  }

  void
  set_run_state ( Run_State state_n )
  {
    _run_state = state_n;
  }

  // -- Run modifiers

  sev::mem::Flags_Fast8 &
  run_modifiers ()
  {
    return _run_modifiers;
  }

  const sev::mem::Flags_Fast8 &
  run_modifiers () const
  {
    return _run_modifiers;
  }

  // -- Time: Begin

  const Time_Point &
  time_begin () const
  {
    return _time_begin;
  }

  void
  time_begin_now ();

  // -- Time: Finish

  const Time_Point &
  time_finish () const
  {
    return _time_finish;
  }

  void
  time_finish_now ();

  // -- Time: Update

  /// @brief Time point when this state was updated
  Time_Point &
  time_update ()
  {
    return _time_update;
  }

  /// @brief Time point when this state was updated
  const Time_Point &
  time_update () const
  {
    return _time_update;
  }

  // -- Duration

  const std::chrono::milliseconds &
  duration () const
  {
    return _duration;
  }

  void
  duration_update ();

  public:
  // -- Attributes
  Run_State _run_state = Run_State::NONE;
  sev::mem::Flags_Fast8 _run_modifiers;
  Time_Point _time_begin;
  Time_Point _time_finish;
  Time_Point _time_update;
  std::chrono::milliseconds _duration;
};

} // namespace svs::program::office
