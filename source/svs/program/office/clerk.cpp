/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#include "clerk.hpp"
#include <sev/assert.hpp>
#include <snc/device/state/state.hpp>
#include <snc/device/statics/axis.hpp>
#include <snc/device/statics/control/i1.hpp>
#include <cstdint>

namespace svs::program::office
{

Clerk::Clerk ( const Clerk_Init & init_n )
: Super ( init_n, "Program" )
, _actor ( this )
, _mpath_loader ( this )
, _mpath_painter ( this )
, _mpath_transform ( this )
, _mpath_pilot ( this )
, _event_pool_program_state ( dash_io ().epool_tracker () )
{
  dash_io ().allocate ( _event_pool_program_state, 2 );

  // -- Front event generation
  _state_update_timer.set_duration ( std::chrono::milliseconds ( 100 ) );
  _state_update_timer.set_callback ( [ this ] () { this->state_update (); } );
  _state_update_timer.connect ( cell_context ().timer_queue () );

  // Setup actor interface
  _actor.set_break_hard_func ( [ this ] () { route_break (); } );

  _mpath_pilot.session ().set_begin_callback ( [ this ] () {
    // Setup speeds
    _state.speed_db_requested.set_speed ( snc::mpath::Speed_Db_Id::RAPID,
                                          1000.0 );
    sanitize_speeds ();

    request_front_message_state ();
  } );
  _mpath_pilot.session ().set_end_callback (
      [ this ] () { _mpath_pilot_control.release (); } );
}

Clerk::~Clerk () = default;

void
Clerk::cell_session_begin ()
{
  Super::cell_session_begin ();

  _actor.connect_required ();
  _mpath_pilot.connect_required ();
  _mpath_loader.connect_required ();
  _mpath_painter.connect_required ();
  _mpath_transform.connect_required ();

  _mpath_stream_speed_insert =
      std::make_shared< mpath_stream::read::Speed_Insert3 > ();

  _speed_db_approach.set_speed ( snc::mpath::Speed_Db_Id::RAPID, 10000 );

  _requested_transform.reset ();
  request_front_message_state ();
}

void
Clerk::cell_session_abort ()
{
  Super::cell_session_abort ();
  route_break ();
}

bool
Clerk::cell_session_is_ready_to_end () const
{
  return Super::cell_session_is_ready_to_end () && //
         !_mpath_loader_control &&                 //
         !_mpath_painter_control &&                //
         !_mpath_transform_control &&              //
         !_state.routing_in_progress ();
}

inline void
Clerk::request_front_message_state ()
{
  dash_io ().notification_set ( Dash_Message::STATE );
}

void
Clerk::dash_event ( Dash_Event & event_n )
{
  switch ( event_n.type () ) {
  case dash::event::out::Type::PATH_FILE:
    dash_event_path_file ( event_n );
    break;
  case dash::event::out::Type::TRANSFORM:
    dash_event_transform ( event_n );
    break;
  case dash::event::out::Type::TRANSFORM_AXIS:
    dash_event_transform_axis ( event_n );
    break;
  case dash::event::out::Type::SPEEDS:
    dash_event_speeds ( event_n );
    break;
  case dash::event::out::Type::SPEED:
    dash_event_speed ( event_n );
    break;
  case dash::event::out::Type::ROUTE_BEGIN:
    dash_event_route_begin ( event_n );
    break;
  case dash::event::out::Type::ROUTE_ABORT:
    dash_event_route_abort ( event_n );
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }

  request_front_message_state ();
  request_processing ();
}

void
Clerk::dash_event_path_file ( Dash_Event & event_n )
{
  if ( _state.routing_in_progress () ) {
    return;
  }

  auto & cevent =
      static_cast< const dash::event::out::Path_File & > ( event_n );
  log ().cat ( sev::logt::FL_DEBUG_0, "File request: ", cevent.file_name () );

  // Reset route state / requests
  _state.reset_job_states ();

  _file_requests.clear ();
  _transform_requests.clear ();
  _route_requests.clear ();
  _requested_transform.reset ();

  _mpath_loader_control.release ();
  _mpath_painter_control.release ();
  _mpath_transform_control.release ();

  _mpath.reset ();
  _mpath_transformed.reset ();

  // Register file request
  _state.file_state = State::File_State::SCHEDULED;
  _state.file_name = cevent.file_name ();
  _file_requests.set ( File_Request::LOAD );

  request_processing ();
}

void
Clerk::dash_event_transform ( Dash_Event & event_n )
{
  if ( _state.routing_in_progress () ) {
    return;
  }

  auto & cevent =
      static_cast< const dash::event::out::Transform & > ( event_n );
  log ().cat ( sev::logt::FL_DEBUG_0, "Transform request." );

  // Stop transformer
  _mpath_transform_control.release ();
  _mpath_transformed.reset ();

  // Reset route state / requests
  _state.reset_job_states ();
  _route_requests.clear ();

  // Register transform request
  _requested_transform = cevent.transform ();
}

void
Clerk::dash_event_transform_axis ( Dash_Event & event_n )
{
  if ( _state.routing_in_progress () ) {
    return;
  }

  auto & cevent =
      static_cast< const dash::event::out::Transform_Axis & > ( event_n );
  if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "Transform axis request.\n  Axis: " << cevent.axis_index ();
    logo << "\n  Type: " << static_cast< int > ( cevent.offset_type () );
    logo << "\n  Offset: " << cevent.offset ();
  }

  // Stop transformer
  _mpath_transform_control.release ();
  _mpath_transformed.reset ();

  // Reset route state / requests
  _state.reset_job_states ();
  _route_requests.clear ();

  // Register transform request
  auto aindex = cevent.axis_index ();
  if ( aindex < 3 ) {
    _requested_transform.offset_type[ aindex ] = cevent.offset_type ();
    _requested_transform.offset[ aindex ] = cevent.offset ();
  }
}

void
Clerk::dash_event_speeds ( Dash_Event & event_n )
{
  if ( _state.routing_in_progress () ) {
    return;
  }

  auto & cevent = static_cast< const dash::event::out::Speeds & > ( event_n );
  if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "Speeds request: ";
    for ( auto & item : cevent.speed_db ().speeds () ) {
      logo << item << ", ";
    }
  };

  // Reset route state / requests
  _state.reset_job_states ();
  _route_requests.clear ();

  _state.speed_db_requested = cevent.speed_db ();
  sanitize_speeds ();
}

void
Clerk::dash_event_speed ( Dash_Event & event_n )
{
  if ( _state.routing_in_progress () ) {
    return;
  }

  auto & cevent = static_cast< const dash::event::out::Speed & > ( event_n );
  log ().cat ( sev::logt::FL_DEBUG_0,
               "Speed request.\n  type: ",
               static_cast< int > ( cevent.speed_type () ),
               "\n  value: ",
               cevent.value () );

  // Reset route state / requests
  _state.reset_job_states ();
  _route_requests.clear ();

  _state.speed_db_requested.set_speed ( cevent.speed_type (), cevent.value () );
  sanitize_speeds ();
}

void
Clerk::sanitize_speeds ()
{
  if ( _mpath_pilot.session_is_good () ) {
    auto & statics = _mpath_pilot.device_statics ();
    // Compute sanitized speeds
    _state.speed_db_sanitized = _state.speed_db_requested;
    _state.speed_db_sanitized.sanitized_maximum_speeds ();
    _state.speed_db_sanitized.crop_speeds ( 0.0, statics.speed_max () );
    _state.speed_db_sanitized.crop_planar_speeds (
        0.0, statics.speed_planar_max () );
    _state.speed_db_sanitized.crop_normal_speeds (
        0.0, statics.speed_normal_max () );
  }
}

void
Clerk::dash_event_route_begin ( Dash_Event & event_n [[maybe_unused]] )
{
  log ().cat ( sev::logt::FL_DEBUG_0, "Route begin request." );
  if ( !_state.routing_in_progress () ) {
    _route_requests.set ( Route_Request::BEGIN );
    request_processing ();
  }
}

void
Clerk::dash_event_route_abort ( Dash_Event & event_n [[maybe_unused]] )
{
  log ().cat ( sev::logt::FL_DEBUG_0, "Route abort request." );
  if ( _route_requests.test_any ( Route_Request::BEGIN ) ) {
    _route_requests.unset ( Route_Request::BEGIN );
  }
  if ( _state.routing_in_progress () ) {
    if ( !_route_requests.test_any ( Route_Request::BREAK ) ) {
      _route_requests.set ( Route_Request::BREAK );
      request_processing ();
    }
  }
}

void
Clerk::dash_notify ()
{
  if ( auto sender =
           dash_notifier ( Dash_Message::STATE, _event_pool_program_state ) ) {
    sender.event->set_dash_event_count ( dash_io ().stats_in_fetch_clear () );
    sender.event->set_state ( _state );
  }
}

void
Clerk::process_async ()
{
  request_processing ();
}

void
Clerk::process ()
{
  using File_State = State::File_State;
  using Transformer_State = State::Transformer_State;

  // State update
  if ( sev::change ( _state.controller_available,
                     _mpath_pilot.control_available () ) ) {
    request_front_message_state ();
  }

  // File loading: Start
  if ( _file_requests.test_any ( File_Request::LOAD ) ) {
    if ( _mpath_loader_control = _mpath_loader.control () ) {
      _file_requests.unset ( File_Request::LOAD );
      if ( !_state.file_name.empty () ) {
        {
          auto request = _mpath_loader_control.make_request ();
          request->file_name = _state.file_name;
          if ( auto ctl_bool =
                   _mpath_pilot.device_statics ().controls_i1 ().get_by_key (
                       "cutter" ) ) {
            auto & cref = request->import_config.cutter_ref ();
            cref.map_enabled = true;
            cref.enable_switch_index = ctl_bool->emb ().control_index ();
            cref.enable_switch_inverted = ctl_bool->emb ().inverted ();
            cref.warm_up_duration = ctl_bool->warm_up_duration ();
            cref.cool_down_duration = ctl_bool->cool_down_duration ();
          }
          _mpath_loader_control.start ( request );
        }
        log ().cat ( sev::logt::FL_DEBUG_0, "MPath file loader started." );
        _state.file_state = File_State::READING;
      } else {
        _state.file_state = File_State::CLOSED;
      }
      request_front_message_state ();
    }
  }
  // File loading: Progress
  if ( _mpath_loader_control.poll_updates () ) {
    if ( !_mpath_loader_control.result ().finished () ) {
      // Progress
      _state.file_bytes = _mpath_loader_control.progress ().file_bytes;
    } else {
      // Finished
      log ().cat ( sev::logt::FL_DEBUG_0, "MPath file loader finished." );
      // Finished
      const auto & res = _mpath_loader_control.result ();
      if ( res.good () ) {
        // Success
        _mpath = res.mpath;
        _state.mpath_stats = res.mpath_stats;
        _state.file_bytes = res.file_bytes;
        _state.file_state = File_State::LOADED_SUCCESS;

        // Continue with painting and transform
        _file_requests.set ( File_Request::PAINT );
        _transform_requests.set ( Transform_Request::TRANSFORM );
      } else {
        // Fail
        _state.file_state = File_State::LOADED_ERROR;
        _state.mpath_stats.reset ();
      }
      _mpath_loader_control.release ();
    }
    request_front_message_state ();
    request_processing ();
  }

  // MPath painting: Start
  if ( _file_requests.test_any ( File_Request::PAINT ) ) {
    if ( _mpath_painter_control = _mpath_painter.control () ) {
      _file_requests.unset ( File_Request::PAINT );
      {
        auto request = _mpath_painter_control.make_request ();
        request->mpath = _mpath;
        request->mpath_stats = _state.mpath_stats;
        _mpath_painter_control.start ( request );
      }
      log ().cat ( sev::logt::FL_DEBUG_0, "MPath painter started." );
    }
  }
  // MPath painting: Progress
  if ( _mpath_painter_control.poll_updates () ) {
    if ( _mpath_painter_control.result ().finished () ) {
      // Finished
      log ().cat ( sev::logt::FL_DEBUG_0, "MPath painter finished." );
      _mpath_painter_control.release ();
    }
  }

  if ( !_mpath_pilot.session_is_good () ) {
    _transform_requests.clear ();
    _requested_transform.reset ();
    _route_requests.clear ();
    return;
  }

  // MPath transform: Start
  if ( _mpath &&
       _transform_requests.test_any ( Transform_Request::TRANSFORM ) ) {
    if ( _mpath_transform_control = _mpath_transform.control () ) {
      _transform_requests.unset ( Transform_Request::TRANSFORM );
      {
        auto request = _mpath_transform_control.make_request ();
        request->mpath = _mpath;
        request->translation = _state.mpath_translation;
        _mpath_transform_control.start ( request );
      }
      _state.transformer_state = Transformer_State::RUNNING;
      log ().cat ( sev::logt::FL_DEBUG_0, "MPath transformer started." );
    }
  }
  // MPath transform: Progress
  if ( _mpath_transform_control.poll_updates () ) {
    // -- Utility
    auto update_contours =
        [ this ] ( const snc::mpath_stream::Statistics_D3 & mpath_stats ) {
          _state.mpath_stats_transformed = mpath_stats;
          for ( std::size_t ii = 0; ii != 3; ++ii ) {
            const auto & saxis =
                *_mpath_pilot.device_statics ().axes ().get ( ii );

            _state.mpath_edge_distance_bottom[ ii ] =
                _state.mpath_stats_transformed.curve_bbox[ 0 ][ ii ];
            _state.mpath_edge_distance_top[ ii ] =
                ( saxis.geo ().length () -
                  _state.mpath_stats_transformed.curve_bbox[ 1 ][ ii ] );
          }
        };

    if ( !_mpath_transform_control.result ().finished () ) {
      // Progress
      update_contours ( _mpath_transform_control.progress ().mpath_stats );
      request_front_message_state ();
    } else {
      // Finished
      log ().cat ( sev::logt::FL_DEBUG_0, "MPath transformer finished." );
      update_contours ( _mpath_transform_control.result ().mpath_stats );
      _mpath_transformed = _mpath_transform_control.result ().mpath;
      _state.transformer_state = Transformer_State::DONE;
      _mpath_transform_control.release ();
      request_front_message_state ();
      request_processing ();
    }
  }

  if ( _requested_transform.is_valid () ) {
    if ( _mpath && _transform_requests.is_empty () ) {

      const auto & bbox_min = _state.mpath_stats.curve_bbox[ 0 ];
      const auto & bbox_max = _state.mpath_stats.curve_bbox[ 1 ];
      for ( std::size_t ii = 0; ii != 3; ++ii ) {
        double axis_pos = 0.0;
        if ( ii < _mpath_pilot.device_state ().axes ().size () ) {
          axis_pos = _mpath_pilot.device_state ()
                         .axes ()[ ii ]
                         .kinematics_latest ()
                         .position ();
        }
        double pdelta = 0.0;
        bool do_set = true;
        switch ( _requested_transform.offset_type[ ii ] ) {
        case Transform::Offset::INVALID:
          do_set = false;
          break;
        case Transform::Offset::BASE:
          pdelta = axis_pos;
          break;
        case Transform::Offset::MIN:
          pdelta = axis_pos - bbox_min[ ii ];
          break;
        case Transform::Offset::MIDDLE:
          pdelta = axis_pos - ( ( bbox_min[ ii ] + bbox_max[ ii ] ) / 2.0 );
          break;
        case Transform::Offset::MAX:
          pdelta = axis_pos - bbox_max[ ii ];
          break;
        default:
          do_set = false;
          DEBUG_ASSERT ( false );
          break;
        }
        if ( do_set ) {
          double & apos = _state.mpath_translation[ ii ];
          apos = _requested_transform.offset[ ii ];
          apos += pdelta;
        }
      }
      _requested_transform.reset ();
      _transform_requests.set ( Transform_Request::TRANSFORM );
      request_processing ();
    }
  }

  if ( !_route_requests.is_empty () ) {
    // Routing begin
    if ( _route_requests.test_any ( Route_Request::BEGIN ) ) {
      if ( _file_requests.is_empty () && _transform_requests.is_empty () &&
           !_requested_transform.is_valid () && !_mpath_transform_control &&
           _mpath_transformed ) {
        _route_requests.unset ( Route_Request::BEGIN );
        route_begin ();
      }
    }
    // Routing break / abort
    if ( _route_requests.test_any_unset ( Route_Request::BREAK ) ) {
      route_break ();
    }
  }

  // Route start / finish
  route_stream_start ();
  route_stream_finish ();
}

void
Clerk::state_update ()
{
  if ( _state.stream_current == Stream_Select::NONE ) {
    _state_update_timer.stop ();
    return;
  }

  _state.current_job_state ()->duration_update ();
  request_front_message_state ();
}

void
Clerk::route_begin ()
{
  bool accepted = true;

  if ( !_mpath_pilot.device_state ().axes ()[ 0 ].is_aligned () ||
       !_mpath_pilot.device_state ().axes ()[ 1 ].is_aligned () ||
       !_mpath_pilot.device_state ().axes ()[ 2 ].is_aligned () ) {
    accepted = false;
    log ().cat ( sev::logt::FL_DEBUG_0,
                 "Route begin denied: Not all axes aligned." );
  }
  if ( !_state.startable () ) {
    accepted = false;
    log ().cat ( sev::logt::FL_DEBUG_0, "Route begin denied: Not startable." );
  }
  if ( _state.routing_in_progress () ) {
    accepted = false;
    log ().cat ( sev::logt::FL_DEBUG_0,
                 "Route begin denied: Routing in progress." );
  }

  if ( accepted ) {
    _mpath_pilot_control = _mpath_pilot.control ();
    if ( !_mpath_pilot_control ) {
      accepted = false;
      log ().cat ( sev::logt::FL_DEBUG_0,
                   "Route begin denied: MPath controller not available." );
    }
  }

  if ( accepted ) {
    log ().cat ( sev::logt::FL_DEBUG_0, "Route begin accepted." );

    _actor.go_active ();

    // Update controller state
    _state.reset_job_states ();
    _state.stream_current = Stream_Select::APPROACH;
    _state.job_state ( Stream_Select::APPROACH )
        .set_run_state ( Run_State::PREPARING );
    _state.job_state ( Stream_Select::MAIN )
        .set_run_state ( Run_State::SCHEDULED );
    _state.job_state ( Stream_Select::RETREAT )
        .set_run_state ( Run_State::SCHEDULED );

    _state_update_timer.start_periodical_now ();
  }

  request_front_message_state ();
  request_processing ();
}

void
Clerk::route_break ()
{
  if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "route_break";
  }

  if ( !_state.routing_in_progress () ) {
    return;
  }

  auto & job_state_approach = _state.job_state ( Stream_Select::APPROACH );
  auto & job_state_main = _state.job_state ( Stream_Select::MAIN );
  auto & job_state_retreat = _state.job_state ( Stream_Select::RETREAT );

  // Abort selective
  switch ( _state.stream_current ) {

  case Stream_Select::APPROACH:
    // Abort runnning approach and main but do a retreat
    job_state_approach.run_modifiers ().set ( Run_Modifier::ABORT );
    job_state_main.run_modifiers ().set ( Run_Modifier::ABORT );
    // Abort routing
    if ( job_state_approach.run_state () == Run_State::RUNNING ) {
      _mpath_pilot_control.break_hard ();
      _actor.go_breaking ();
    }
    break;

  case Stream_Select::MAIN:
    // Abort main but do a retreat
    job_state_main.run_modifiers ().set ( Run_Modifier::ABORT );
    // Abort routing
    if ( job_state_main.run_state () == Run_State::RUNNING ) {
      _mpath_pilot_control.break_hard ();
      _actor.go_breaking ();
    }
    break;

  case Stream_Select::RETREAT:
    // Abort the retreat
    job_state_retreat.run_modifiers ().set ( Run_Modifier::ABORT );
    // Abort routing
    if ( job_state_retreat.run_state () == Run_State::RUNNING ) {
      _mpath_pilot_control.break_hard ();
      _actor.go_breaking ();
    }
    break;

  default:
    DEBUG_ASSERT ( false );
    break;
  }

  request_front_message_state ();
  request_processing ();
}

void
Clerk::route_stream_start ()
{
  if ( _state.stream_current == Stream_Select::NONE ) {
    // Not preparing
    return;
  }
  if ( _state.current_job_state ()->run_state () != Run_State::PREPARING ) {
    // Not preparing
    return;
  }

  switch ( _state.stream_current ) {
  case Stream_Select::APPROACH: {
    // -- Approach mpath
    auto & job_state = *_state.current_job_state ();
    bool mpath_valid = false;
    if ( job_state.run_modifiers ().test_any ( Run_Modifier::ABORT ) ) {
      mpath_valid = false;
    }
    if ( _state.mpath_stats.num_elements_curve != 0 ) {
      // Start at current machine position and go to the
      // machine path begin position
      sev::lag::Vector3d pos_begin;
      sev::lag::Vector3d pos_end;
      _mpath_pilot.device_state ().acquire_position_current ( pos_begin );
      pos_end = _state.mpath_stats_transformed.curve_pos_begin;
      if ( pos_begin != pos_end ) {
        _mpath_stream_go_to.setup ( pos_begin, pos_end );
        _retreat_pos = pos_begin;
        mpath_valid = true;
      }
    }
    if ( mpath_valid ) {
      log ().cat ( sev::logt::FL_DEBUG_0, "Starting approach MPath." );
      // Setup streams
      {
        sev::lag::Vector3d pos_begin;
        _mpath_pilot.device_state ().acquire_position_current ( pos_begin );
        _mpath_stream_speed_insert->setup ( &_mpath_stream_go_to,
                                            pos_begin,
                                            _mpath_pilot.device_statics (),
                                            _speed_db_approach );
      }
      job_state.set_run_state ( Run_State::RUNNING );
      job_state.run_modifiers ().clear ();
      job_state.time_begin_now ();

      _mpath_pilot_control.route_begin ( _mpath_stream_speed_insert,
                                         "Approach" );
    } else {
      // Skip the approach
      log ().cat ( sev::logt::FL_DEBUG_0, "Skipping approach MPath." );
      job_state.set_run_state ( Run_State::FINISHED );
      job_state.run_modifiers ().assign ( Run_Modifier::SKIP );
      _state.stream_current = Stream_Select::MAIN;
      _state.current_job_state ()->set_run_state ( Run_State::PREPARING );
    }
  } break;

  case Stream_Select::MAIN: {
    // -- Main mpath
    auto & job_state = *_state.current_job_state ();
    bool mpath_valid = true;
    if ( job_state.run_modifiers ().test_any ( Run_Modifier::ABORT ) ) {
      mpath_valid = false;
    }
    if ( _state.mpath_stats_transformed.num_elements == 0 ) {
      mpath_valid = false;
    }
    if ( mpath_valid ) {
      log ().cat ( sev::logt::FL_DEBUG_0, "Starting main MPath." );
      // Setup streams
      {
        _mpath_transformed_stream.set_buffer ( _mpath_transformed.get () );
        sev::lag::Vector3d pos_begin;
        _mpath_pilot.device_state ().acquire_position_current ( pos_begin );
        _mpath_stream_speed_insert->setup ( &_mpath_transformed_stream,
                                            pos_begin,
                                            _mpath_pilot.device_statics (),
                                            _state.speed_db_sanitized );
      }
      job_state.set_run_state ( Run_State::RUNNING );
      job_state.run_modifiers ().clear ();
      job_state.time_begin_now ();

      _mpath_pilot_control.route_begin ( _mpath_stream_speed_insert, "Main" );
    } else {
      // Invalid mpath, skip the rest
      log ().cat ( sev::logt::FL_DEBUG_0, "Skipping main MPath." );
      job_state.set_run_state ( Run_State::FINISHED );
      job_state.run_modifiers ().assign ( Run_Modifier::SKIP );
      _state.stream_current = Stream_Select::RETREAT;
      _state.current_job_state ()->set_run_state ( Run_State::PREPARING );
    }
  } break;

  case Stream_Select::RETREAT: {
    // -- Retreat mpath
    auto & job_state = *_state.current_job_state ();
    bool mpath_valid = false;
    if ( job_state.run_modifiers ().test_any ( Run_Modifier::ABORT ) ) {
      mpath_valid = false;
    }
    if ( _state.mpath_stats.num_elements_curve != 0 ) {
      // Start at current machine position and
      // go to the retreat position
      sev::lag::Vector3d pos_cur;
      _mpath_pilot.device_state ().acquire_position_current ( pos_cur );
      if ( pos_cur != _retreat_pos ) {
        _mpath_stream_go_to.setup ( pos_cur, _retreat_pos );
        mpath_valid = true;
      }
    }
    if ( mpath_valid ) {
      log ().cat ( sev::logt::FL_DEBUG_0, "Starting retreat MPath." );
      // Setup streams
      {
        sev::lag::Vector3d pos_begin;
        _mpath_pilot.device_state ().acquire_position_current ( pos_begin );
        _mpath_stream_speed_insert->setup ( &_mpath_stream_go_to,
                                            pos_begin,
                                            _mpath_pilot.device_statics (),
                                            _speed_db_approach );
      }
      job_state.set_run_state ( Run_State::RUNNING );
      job_state.run_modifiers ().clear ();
      job_state.time_begin_now ();

      _mpath_pilot_control.route_begin ( _mpath_stream_speed_insert,
                                         "Retreat" );
    } else {
      // Skip the retreat
      log ().cat ( sev::logt::FL_DEBUG_0, "Skipping retreat MPath." );
      job_state.set_run_state ( Run_State::FINISHED );
      job_state.run_modifiers ().assign ( Run_Modifier::SKIP );
      _state.stream_current = Stream_Select::NONE;
    }
  } break;

  default:
    break;
  }

  request_front_message_state ();
  request_processing ();
}

void
Clerk::route_stream_finish ()
{
  // Process finished streams
  if ( _state.stream_current == Stream_Select::NONE ) {
    // Not routing
    return;
  }
  if ( _state.current_job_state ()->run_state () != Run_State::RUNNING ) {
    // Not routing
    return;
  }
  if ( _mpath_pilot.state ().is_running () ) {
    // Not finished, yet
    return;
  }

  {
    auto & job_state = *_state.current_job_state ();
    job_state.set_run_state ( Run_State::FINISHED );
    job_state.time_finish_now ();
  }

  switch ( _state.stream_current ) {
  case Stream_Select::APPROACH:
    log ().cat ( sev::logt::FL_DEBUG_0, "Approach finished." );
    _state.stream_current = Stream_Select::MAIN;
    _state.current_job_state ()->set_run_state ( Run_State::PREPARING );
    break;
  case Stream_Select::MAIN:
    log ().cat ( sev::logt::FL_DEBUG_0, "Main path finished." );
    _state.stream_current = Stream_Select::RETREAT;
    _state.current_job_state ()->set_run_state ( Run_State::PREPARING );
    break;
  case Stream_Select::RETREAT:
    log ().cat ( sev::logt::FL_DEBUG_0, "Retreat finished." );
    _state.stream_current = Stream_Select::NONE;
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }

  // All finished
  if ( _state.stream_current == Stream_Select::NONE ) {
    _mpath_pilot_control.release ();
    _actor.go_idle ();
  }

  request_front_message_state ();
  request_processing ();
}

} // namespace svs::program::office
