/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <sev/assert.hpp>
#include <sev/lag/vector3.hpp>
#include <snc/mpath/speed_database.hpp>
#include <snc/mpath_stream/statistics_d.hpp>
#include <svs/program/office/mpath_job_state.hpp>
#include <array>
#include <cstdint>
#include <string>

namespace svs::program::office
{

class State
{
  public:
  // -- Types

  enum class File_State : std::uint8_t
  {
    CLOSED,

    SCHEDULED,
    OPENING,
    READING,

    LOADED_SUCCESS,
    LOADED_ERROR
  };

  enum class Transformer_State : std::uint8_t
  {
    DONE,
    ABORTING,
    RUNNING
  };

  enum class Stream_Select : std::uint8_t
  {
    NONE,
    APPROACH,
    MAIN,
    RETREAT
  };

  enum class Stream_State : std::uint8_t
  {
    NONE,
    PREPARE,
    RUNNING,
    FINISHED,
  };

  // -- Construction

  State ();

  ~State ();

  void
  reset ();

  void
  reset_job_states ();

  // -- File loading state

  bool
  file_in_process () const
  {
    return ( file_state == File_State::SCHEDULED ) ||
           ( file_state == File_State::OPENING ) ||
           ( file_state == File_State::READING );
  }

  bool
  file_valid () const
  {
    return ( file_state == File_State::LOADED_SUCCESS );
  }

  // -- Path transformer state

  bool
  transformer_in_process () const;

  bool
  loaded_and_transformed () const;

  bool
  edge_distances_valid () const;

  bool
  startable () const;

  // -- Routing state

  bool
  routing_in_progress () const
  {
    return ( stream_current != Stream_Select::NONE );
  }

  bool
  current_job_state_valid () const
  {
    return ( stream_current != Stream_Select::NONE );
  }

  MPath_Job_State *
  current_job_state ()
  {
    return current_job_state_valid () ? &job_state ( stream_current ) : nullptr;
  }

  const MPath_Job_State *
  current_job_state () const
  {
    return current_job_state_valid () ? &job_state ( stream_current ) : nullptr;
  }

  MPath_Job_State &
  job_state ( Stream_Select select_n )
  {
    DEBUG_ASSERT ( select_n != Stream_Select::NONE );
    return stream_job_states[ static_cast< std::size_t > ( select_n ) - 1 ];
  }

  const MPath_Job_State &
  job_state ( Stream_Select select_n ) const
  {
    DEBUG_ASSERT ( select_n != Stream_Select::NONE );
    return stream_job_states[ static_cast< std::size_t > ( select_n ) - 1 ];
  }

  public:
  // -- Attributes
  // Route preparation state
  File_State file_state;
  Transformer_State transformer_state;
  bool controller_available = false;

  // Routing runtime state
  Stream_Select stream_current;
  std::array< MPath_Job_State, 3 > stream_job_states;

  /// @brief Machine path statistics
  snc::mpath_stream::Statistics_D3 mpath_stats;
  /// @brief Machine path transform
  sev::lag::Vector3d mpath_translation;
  snc::mpath_stream::Statistics_D3 mpath_stats_transformed;
  sev::lag::Vector3d mpath_edge_distance_bottom;
  sev::lag::Vector3d mpath_edge_distance_top;

  // Machine path source file
  std::string file_name;
  std::uint64_t file_bytes;

  // Speeds
  snc::mpath::Speed_Database speed_db_requested;
  snc::mpath::Speed_Database speed_db_sanitized;
};

} // namespace svs::program::office
