/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#include "mpath_job_state.hpp"

namespace svs::program::office
{

void
MPath_Job_State::reset ()
{
  _run_state = Run_State::NONE;
  _run_modifiers.clear ();
  _time_begin = Time_Point ();
  _time_finish = Time_Point ();
  _time_update = Time_Point ();
  _duration = decltype ( _duration ) ();
}

void
MPath_Job_State::time_begin_now ()
{
  _time_begin.now ();
  duration_update ();
}

void
MPath_Job_State::time_finish_now ()
{
  _time_finish.now ();
  duration_update ();
}

void
MPath_Job_State::duration_update ()
{
  if ( !_time_begin.is_valid () ) {
    _duration = decltype ( _duration ) ();
  }
  Time_Point tend = _time_finish;
  if ( !tend.is_valid () ) {
    tend.now ();
  }
  _duration = std::chrono::duration_cast< decltype ( _duration ) > (
      tend.steady () - _time_begin.steady () );
}

} // namespace svs::program::office
