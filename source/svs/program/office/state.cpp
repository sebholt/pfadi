/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#include "state.hpp"

namespace svs::program::office
{

State::State ()
{
  reset ();
}

State::~State () = default;

void
State::reset ()
{
  file_state = File_State::CLOSED;
  transformer_state = Transformer_State::DONE;
  controller_available = false;

  stream_current = Stream_Select::NONE;
  for ( auto & item : stream_job_states ) {
    item.reset ();
  }

  file_name.clear ();
  file_bytes = 0;

  mpath_stats.reset ();
  mpath_translation.fill ( 0.0 );
  mpath_stats_transformed.reset ();

  mpath_edge_distance_bottom.fill ( 0.0 );
  mpath_edge_distance_top.fill ( 0.0 );
}

void
State::reset_job_states ()
{
  stream_current = Stream_Select::NONE;
  for ( auto & item : stream_job_states ) {
    item.reset ();
  }
}

bool
State::transformer_in_process () const
{
  return file_in_process () ||
         ( transformer_state == Transformer_State::ABORTING ) ||
         ( transformer_state == Transformer_State::RUNNING );
}

bool
State::loaded_and_transformed () const
{
  return ( file_state == File_State::LOADED_SUCCESS ) &&
         ( transformer_state == Transformer_State::DONE );
}

bool
State::edge_distances_valid () const
{
  for ( auto & dist : mpath_edge_distance_bottom ) {
    if ( dist < 0.0 ) {
      return false;
    }
  }
  for ( auto & dist : mpath_edge_distance_top ) {
    if ( dist < 0.0 ) {
      return false;
    }
  }
  return true;
}

bool
State::startable () const
{
  return ( loaded_and_transformed () && edge_distances_valid () &&
           controller_available );
}

} // namespace svs::program::office
