/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <sev/lag/vector3.hpp>
#include <array>
#include <cstdint>

namespace svs::program::office
{

class Transform
{
  public:
  // -- Types

  enum class Offset : std::uint8_t
  {
    INVALID,

    BASE,
    MIN,
    MIDDLE,
    MAX
  };

  // -- Construction

  Transform () { reset (); }

  void
  reset ()
  {
    offset_type.fill ( Offset::INVALID );
    offset.fill ( 0.0 );
  }

  bool
  is_valid () const
  {
    for ( Offset offset : offset_type ) {
      if ( offset != Offset::INVALID ) {
        return true;
      }
    }
    return false;
  }

  public:
  // -- Attributes
  std::array< Offset, 3 > offset_type;
  /// @brief Base position
  sev::lag::Vector3d offset;
};

} // namespace svs::program::office
