/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <snc/mpath/speed_database.hpp>
#include <snc/svp/dash/events.hpp>
#include <svs/program/office/state.hpp>
#include <svs/program/office/transform.hpp>

namespace svs::program::dash::event::in
{

struct Type
{
  static constexpr std::uint_fast32_t STATE = 0;
};

using namespace snc::svp::dash::event::in;

using State = State_T< Type::STATE, svs::program::office::State >;

} // namespace svs::program::dash::event::in

namespace svs::program::dash::event::out
{

struct Type
{
  static constexpr std::uint_fast32_t PATH_FILE = 0;
  static constexpr std::uint_fast32_t TRANSFORM = 1;
  static constexpr std::uint_fast32_t TRANSFORM_AXIS = 2;
  static constexpr std::uint_fast32_t SPEEDS = 3;
  static constexpr std::uint_fast32_t SPEED = 4;
  static constexpr std::uint_fast32_t ROUTE_BEGIN = 5;
  static constexpr std::uint_fast32_t ROUTE_ABORT = 6;
};

using Out = snc::svp::dash::event::Out;

class Path_File : public Out
{
  public:
  static constexpr auto etype = Type::PATH_FILE;

  Path_File ( Pair pair_n )
  : Out ( etype, pair_n )
  {
  }

  void
  reset ()
  {
    Out::reset ();
    _file_name.clear ();
  }

  const std::string &
  file_name () const
  {
    return _file_name;
  }

  void
  set_file_name ( std::string_view name_n )
  {
    _file_name = name_n;
  }

  private:
  std::string _file_name;
};

class Transform : public Out
{
  public:
  static constexpr auto etype = Type::TRANSFORM;

  Transform ( Pair pair_n )
  : Out ( etype, pair_n )
  {
  }

  void
  reset ()
  {
    Out::reset ();
    _transform.reset ();
  }

  svs::program::office::Transform &
  transform ()
  {
    return _transform;
  }

  const svs::program::office::Transform &
  transform () const
  {
    return _transform;
  }

  private:
  svs::program::office::Transform _transform;
};

class Transform_Axis : public Out
{
  public:
  static constexpr auto etype = Type::TRANSFORM_AXIS;
  using Offset = svs::program::office::Transform::Offset;

  Transform_Axis ( Pair pair_n )
  : Out ( etype, pair_n )
  {
  }

  void
  reset ()
  {
    Out::reset ();
    _axis_index = 0;
    _offset = 0.0;
    _offset_type = Offset::INVALID;
  }

  std::size_t
  axis_index () const
  {
    return _axis_index;
  }

  void
  set_axis_index ( std::size_t index_n )
  {
    _axis_index = index_n;
  }

  double
  offset () const
  {
    return _offset;
  }

  void
  set_offset ( double value_n )
  {
    _offset = value_n;
  }

  Offset
  offset_type () const
  {
    return _offset_type;
  }

  void
  set_offset_type ( Offset value_n )
  {
    _offset_type = value_n;
  }

  private:
  std::size_t _axis_index = 0;
  double _offset = 0.0;
  Offset _offset_type = Offset::INVALID;
};

/// @brief Routing speeds setting
///
class Speeds : public Out
{
  public:
  static constexpr auto etype = Type::SPEEDS;

  Speeds ( Pair pair_n )
  : Out ( etype, pair_n )
  {
  }

  void
  reset ()
  {
    Out::reset ();
    _speed_db.reset ();
  }

  snc::mpath::Speed_Database &
  speed_db ()
  {
    return _speed_db;
  }

  const snc::mpath::Speed_Database &
  speed_db () const
  {
    return _speed_db;
  }

  private:
  snc::mpath::Speed_Database _speed_db;
};

/// @brief Routing speed setting
///
class Speed : public Out
{
  public:
  static constexpr auto etype = Type::SPEED;
  using Speed_Type = snc::mpath::Speed_Db_Id;

  Speed ( Pair pair_n )
  : Out ( etype, pair_n )
  {
  }

  void
  reset ()
  {
    Out::reset ();
    _speed_type = Speed_Type::LIST_END;
    _value = 0.0;
  }

  Speed_Type
  speed_type () const
  {
    return _speed_type;
  }

  void
  set_speed_type ( Speed_Type type_n )
  {
    _speed_type = type_n;
  }

  double
  value () const
  {
    return _value;
  }

  void
  set_value ( double value_n )
  {
    _value = value_n;
  }

  private:
  Speed_Type _speed_type = Speed_Type::LIST_END;
  double _value = 0.0;
};

/// @brief Routing begin request
///
class Route_Begin : public Out
{
  public:
  static constexpr auto etype = Type::ROUTE_BEGIN;

  Route_Begin ( Pair pair_n )
  : Out ( etype, pair_n )
  {
  }
};

/// @brief Routing abort request
///
class Route_Abort : public Out
{
  public:
  static constexpr auto etype = Type::ROUTE_ABORT;

  Route_Abort ( Pair pair_n )
  : Out ( etype, pair_n )
  {
  }
};

} // namespace svs::program::dash::event::out
