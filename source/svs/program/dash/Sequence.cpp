/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#include "Sequence.hpp"

namespace svs::program::dash
{

Sequence::Sequence ( QObject * qparent_n )
: QAbstractListModel ( qparent_n )
{
}

Sequence::~Sequence () = default;

void
Sequence::clear ()
{
  if ( _sections.empty () ) {
    return;
  }

  beginRemoveRows ( QModelIndex (), 0, _sections.size () - 1 );
  _sections.clear ();
  endRemoveRows ();
}

SequenceSection *
Sequence::get ( int index_n )
{
  if ( index_n < 0 ) {
    return nullptr;
  }
  std::size_t idx = static_cast< std::size_t > ( index_n );
  if ( idx >= _sections.size () ) {
    return nullptr;
  }
  return _sections[ idx ].get ();
}

void
Sequence::push ( std::shared_ptr< SequenceSection > section_n )
{
  beginInsertRows ( QModelIndex (), _sections.size (), _sections.size () );
  _sections.emplace_back ( std::move ( section_n ) );
  endInsertRows ();
}

std::shared_ptr< SequenceSection >
Sequence::emplaced ()
{
  auto res = std::make_shared< SequenceSection > ( this );
  push ( res );
  return res;
}

QHash< int, QByteArray >
Sequence::roleNames () const
{
  auto res = QAbstractListModel::roleNames ();
  res.insert ( ExtraRole::sectionObject, "sectionObject" );
  return res;
}

int
Sequence::rowCount ( const QModelIndex & parent_n ) const
{
  if ( parent_n.isValid () ) {
    return 0;
  }
  return _sections.size ();
}

QVariant
Sequence::data ( const QModelIndex & index_n, int role_n ) const
{
  if ( index_n.parent ().isValid () || ( index_n.column () != 0 ) ||
       ( index_n.row () < 0 ) ||
       ( static_cast< std::size_t > ( index_n.row () ) >=
         _sections.size () ) ) {
    return QVariant ();
  }

  switch ( role_n ) {
  case ExtraRole::sectionObject:
    return QVariant::fromValue (
        static_cast< QObject * > ( _sections[ index_n.row () ].get () ) );
  default:
    break;
  }

  return QVariant ();
}

} // namespace svs::program::dash
