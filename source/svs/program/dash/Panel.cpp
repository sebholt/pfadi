/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#include "Panel.hpp"
#include <sev/utility.hpp>
#include <svs/program/office/clerk.hpp>

namespace svs::program::dash
{

Panel::Panel ( const Panel_Init & init_n )
: Super ( init_n, "Machine-Program" )
, _event_pool_path_file ( office_io ().epool_tracker () )
, _event_pool_transform ( office_io ().epool_tracker () )
, _event_pool_transform_axis ( office_io ().epool_tracker () )
, _event_pool_speeds ( office_io ().epool_tracker () )
, _event_pool_speed ( office_io ().epool_tracker () )
, _event_pool_route_begin ( office_io ().epool_tracker () )
, _event_pool_route_abort ( office_io ().epool_tracker () )
, _pathStats ( this )
, _pathStatsTransformed ( this )
, _transformBase ( this )
, _speedsRequested ( this )
, _speedsAccepted ( this )
, _seq_approach ( _sequence.emplaced () )
, _seq_main ( _sequence.emplaced () )
, _seq_retreat ( _sequence.emplaced () )
{
  _seq_approach->setName ( "Approach" );
  _seq_main->setName ( "Main" );
  _seq_retreat->setName ( "Retreat" );

  connect ( &_speedsRequested,
            &QAbstractItemModel::dataChanged,
            this,
            &Panel::submitSpeedsRequested );
}

Panel::~Panel () = default;

Panel::Clerk_Factory_Handle
Panel::office_session_begin_factory ()
{
  return make_clerk_factory< svs::program::office::Clerk > ();
}

void
Panel::office_event ( Office_Event & event_n )
{
  switch ( event_n.type () ) {
  case dash::event::in::Type::STATE:
    office_event_state_t< dash::event::in::State > ( event_n, _state );
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }
}

void
Panel::state_changed ()
{
  Super::state_changed ();

  bool no_pending_events = !office_io ().events_unacknowledged ();
  bool c_startable = no_pending_events && state ().startable ();
  bool c_pathFileSelected = !state ().file_name.empty ();
  bool c_pathFileOpened = no_pending_events && c_pathFileSelected &&
                          !state ().transformer_in_process ();

  if ( sev::change ( _startable, c_startable ) ) {
    emit startableChanged ();
  }
  if ( sev::change ( _routing, state ().routing_in_progress () ) ) {
    emit routingChanged ();
  }

  // Path file
  if ( sev::change ( _pathFileSelected, c_pathFileSelected ) ) {
    emit pathFileSelectedChanged ();
  }
  if ( sev::change ( _pathFileOpening, state ().file_in_process () ) ) {
    emit pathFileOpeningChanged ();
  }
  if ( sev::change ( _pathFileOpened, c_pathFileOpened ) ) {
    emit pathFileOpenedChanged ();
  }
  if ( sev::change ( _pathFileValid, state ().file_valid () ) ) {
    emit pathFileValidChanged ();
  }
  if ( sev::change ( _pathFileSize, state ().file_bytes ) ) {
    emit pathFileSizeChanged ();
  }

  // Path stats
  _pathStats.update ( state ().mpath_stats );
  _pathStatsTransformed.update ( state ().mpath_stats_transformed );

  _transformBase.update ( state ().mpath_translation );
  _edgeDistBottom.update ( state ().mpath_edge_distance_bottom );
  _edgeDistTop.update ( state ().mpath_edge_distance_top );
  if ( sev::change ( _pathFitsWorkspace, state ().edge_distances_valid () ) ) {
    emit pathFitsWorkspaceChanged ();
  }
  if ( sev::change ( _controllerAvailable, state ().controller_available ) ) {
    emit controllerAvailableChanged ();
  }

  // Speeds
  _speedsRequested.update ( state ().speed_db_requested );
  _speedsAccepted.update ( state ().speed_db_sanitized );

  // Sequence
  {
    using Stream_Select = svs::program::office::State::Stream_Select;
    update_sequence_state ( state ().job_state ( Stream_Select::APPROACH ),
                            *_seq_approach );
    update_sequence_state ( state ().job_state ( Stream_Select::MAIN ),
                            *_seq_main );
    update_sequence_state ( state ().job_state ( Stream_Select::RETREAT ),
                            *_seq_retreat );
  }

  // Signal
  emit stateChanged ();
}

void
Panel::update_sequence_state (
    const svs::program::office::MPath_Job_State & state_n,
    SequenceSection & section_n )
{
  // Status
  {
    using Status = SequenceSection::Status;
    using Run_State = svs::program::office::MPath_Job_State::Run_State;
    using Run_Modifier = svs::program::office::MPath_Job_State::Run_Modifier;

    Status status = Status::NONE;
    if ( state_n.run_modifiers ().test_any ( Run_Modifier::SKIP ) ) {
      status = Status::SKIPPED;
    } else if ( state_n.run_modifiers ().test_any ( Run_Modifier::ABORT ) ) {
      switch ( state_n.run_state () ) {
      case Run_State::PREPARING:
      case Run_State::RUNNING:
        status = Status::ABORTING;
        break;
      default:
        status = Status::ABORTED;
        break;
      }
    } else {
      switch ( state_n.run_state () ) {
      case Run_State::NONE:
        status = Status::NONE;
        break;
      case Run_State::SCHEDULED:
        status = Status::SCHEDULED;
        break;
      case Run_State::PREPARING:
        status = Status::PREPARING;
        break;
      case Run_State::RUNNING:
        status = Status::RUNNING;
        break;
      case Run_State::FINISHED:
        status = Status::FINISHED;
        break;
      }
    }
    section_n.setStatus ( status );
  }

  // Time
  {
    auto makeDateTime = [] ( const sev::chrono::Steady_System & time_n ) {
      if ( !time_n.is_valid () ) {
        return QDateTime ();
      }
      auto ms = std::chrono::duration_cast< std::chrono::milliseconds > (
          time_n.system ().time_since_epoch () );
      return QDateTime::fromMSecsSinceEpoch ( ms.count () );
    };

    section_n.setBeginTime ( makeDateTime ( state_n.time_begin () ) );
    section_n.setEndTime ( makeDateTime ( state_n.time_finish () ) );
  }
  {
    QTime time ( 0, 0, 0, 0 );
    section_n.setDuration ( time.addMSecs ( state_n.duration ().count () ) );
  }
}

void
Panel::openPathFile ( const QUrl & file_n )
{
  if ( !office_session_is_good () ) {
    return;
  }
  if ( file_n.isEmpty () ) {
    return;
  }
  if ( !file_n.isLocalFile () ) {
    return;
  }
  {
    auto * event = office_io ().acquire ( _event_pool_path_file );
    event->set_file_name ( file_n.path ().toStdString () );
    office_io ().submit ( event );
  }
  if ( sev::change ( _pathFile, file_n ) ) {
    emit pathFileChanged ();
  }
}

void
Panel::transform ( const svs::program::office::Transform & transform_n )
{
  if ( !office_session_is_good () ) {
    return;
  }
  if ( !transform_n.is_valid () ) {
    return;
  }
  {
    auto * event = office_io ().acquire ( _event_pool_transform );
    event->transform () = transform_n;
    office_io ().submit ( event );
  }
}

void
Panel::transform_axis ( std::size_t axis_index_n,
                        double offset_n,
                        svs::program::office::Transform::Offset offset_type_n )
{
  if ( !office_session_is_good () ) {
    return;
  }
  {
    auto * event = office_io ().acquire ( _event_pool_transform_axis );
    event->set_axis_index ( axis_index_n );
    event->set_offset ( offset_n );
    event->set_offset_type ( offset_type_n );
    office_io ().submit ( event );
  }
}

void
Panel::setTransformBase ( int axis_index_n, double offset_n )
{
  transform_axis (
      axis_index_n, offset_n, svs::program::office::Transform::Offset::BASE );
}

void
Panel::setTransformMin ( int axis_index_n, double offset_n )
{
  transform_axis (
      axis_index_n, offset_n, svs::program::office::Transform::Offset::MIN );
}

void
Panel::setTransformMid ( int axis_index_n, double offset_n )
{
  transform_axis (
      axis_index_n, offset_n, svs::program::office::Transform::Offset::MIDDLE );
}

void
Panel::setTransformMax ( int axis_index_n, double offset_n )
{
  transform_axis (
      axis_index_n, offset_n, svs::program::office::Transform::Offset::MAX );
}

void
Panel::submitSpeedsRequested ()
{
  if ( !office_session_is_good () ) {
    return;
  }
  {
    auto * event = office_io ().acquire ( _event_pool_speeds );
    event->speed_db () = _speedsRequested.speeds ();
    office_io ().submit ( event );
  }
}

void
Panel::routeBegin ()
{
  if ( !office_session_is_good () ) {
    return;
  }
  office_io ().submit ( office_io ().acquire ( _event_pool_route_begin ) );
}

void
Panel::routeAbort ()
{
  if ( !office_session_is_good () ) {
    return;
  }
  office_io ().submit ( office_io ().acquire ( _event_pool_route_abort ) );
}

} // namespace svs::program::dash
