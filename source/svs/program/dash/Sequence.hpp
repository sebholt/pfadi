/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <svs/program/dash/SequenceSection.hpp>
#include <QAbstractListModel>
#include <QDateTime>
#include <memory>
#include <vector>

namespace svs::program::dash
{

class Sequence : public QAbstractListModel
{
  Q_OBJECT

  // -- Properties

  Q_PROPERTY ( int count READ count NOTIFY countChanged )
  Q_PROPERTY ( QDateTime begin READ begin NOTIFY beginChanged )
  Q_PROPERTY ( QDateTime end READ end NOTIFY endChanged )

  // -- Types

  enum ExtraRole
  {
    sectionObject = Qt::UserRole
  };

  public:
  // -- Construction

  Sequence ( QObject * qparent_n = nullptr );

  ~Sequence ();

  // -- Begin

  const QDateTime &
  begin () const
  {
    return _begin;
  }

  Q_SIGNAL
  void
  beginChanged ();

  // -- End

  const QDateTime &
  end () const
  {
    return _end;
  }

  Q_SIGNAL
  void
  endChanged ();

  // -- Count

  int
  count () const
  {
    return _sections.size ();
  }

  Q_SIGNAL void
  countChanged ();

  // -- Sections

  void
  clear ();

  const std::vector< std::shared_ptr< SequenceSection > > &
  sections () const
  {
    return _sections;
  }

  const std::shared_ptr< SequenceSection > &
  section ( std::size_t index_n )
  {
    return _sections[ index_n ];
  }

  svs::program::dash::SequenceSection *
  get ( int index_n );

  void
  push ( std::shared_ptr< SequenceSection > section_n );

  std::shared_ptr< SequenceSection >
  emplaced ();

  // -- Model interface

  QHash< int, QByteArray >
  roleNames () const override;

  int
  rowCount ( const QModelIndex & parent_n = QModelIndex () ) const override;

  QVariant
  data ( const QModelIndex & index_n, int role_n ) const override;

  private:
  // -- Attributes
  std::vector< std::shared_ptr< SequenceSection > > _sections;
  QDateTime _begin;
  QDateTime _end;
};

} // namespace svs::program::dash
