/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#include "SequenceSection.hpp"

namespace svs::program::dash
{

SequenceSection::SequenceSection ( QObject * qparent_n )
: QObject ( qparent_n )
{
}

SequenceSection::~SequenceSection () = default;

void
SequenceSection::setName ( const QString & name_n )
{
  if ( _name == name_n ) {
    return;
  }
  _name = name_n;
  emit nameChanged ();
}

void
SequenceSection::setStatus ( Status status_n )
{
  if ( _status == status_n ) {
    return;
  }
  _status = status_n;
  emit statusChanged ();
}

void
SequenceSection::setBeginTime ( const QDateTime & time_n )
{
  if ( _beginTime == time_n ) {
    return;
  }
  _beginTime = time_n;
  emit beginTimeChanged ();
  update_started ();
}

void
SequenceSection::setEndTime ( const QDateTime & time_n )
{
  if ( _endTime == time_n ) {
    return;
  }
  _endTime = time_n;
  emit endTimeChanged ();
  update_finished ();
}

void
SequenceSection::setDuration ( const QTime & time_n )
{
  if ( _duration == time_n ) {
    return;
  }
  _duration = time_n;
  emit durationChanged ();
}

void
SequenceSection::update_started ()
{
  bool c_started = _beginTime.isValid ();
  if ( _started == c_started ) {
    return;
  }
  _started = c_started;
  emit startedChanged ();
  update_running ();
}

void
SequenceSection::update_finished ()
{
  bool c_finished = _endTime.isValid ();
  if ( _finished == c_finished ) {
    return;
  }
  _finished = c_finished;
  emit finishedChanged ();
  update_running ();
}

void
SequenceSection::update_running ()
{
  bool c_running = ( _started && !_finished );
  if ( _running == c_running ) {
    return;
  }
  _running = c_running;
  emit runningChanged ();
}

} // namespace svs::program::dash
