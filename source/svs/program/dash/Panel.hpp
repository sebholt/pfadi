/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <sev/event/pool.hpp>
#include <snc/mpath/qt/SpeedDatabase.hpp>
#include <snc/mpath_stream/qt/Statistics.hpp>
#include <snc/qt/VectorModel.hpp>
#include <snc/svp/dash/Dash.hpp>
#include <snc/svp/dash/Panel.hpp>
#include <svs/program/dash/Sequence.hpp>
#include <svs/program/dash/SequenceSection.hpp>
#include <svs/program/dash/events.hpp>
#include <svs/program/office/state.hpp>
#include <QUrl>

namespace svs::program::dash
{

class Panel : public snc::svp::dash::Panel
{
  Q_OBJECT

  // -- Types
  private:
  using Super = snc::svp::dash::Panel;

  public:
  // -- Properties
  Q_PROPERTY ( bool startable READ startable NOTIFY startableChanged )
  Q_PROPERTY ( bool routing READ routing NOTIFY routingChanged )

  Q_PROPERTY (
      QUrl pathFile READ pathFile WRITE openPathFile NOTIFY pathFileChanged )
  Q_PROPERTY ( QString pathFilePath READ pathFilePath NOTIFY pathFileChanged )
  Q_PROPERTY ( bool pathFileSelected READ pathFileSelected NOTIFY
                   pathFileSelectedChanged )
  Q_PROPERTY (
      bool pathFileOpening READ pathFileOpening NOTIFY pathFileOpeningChanged )
  Q_PROPERTY (
      bool pathFileOpened READ pathFileOpened NOTIFY pathFileOpenedChanged )
  Q_PROPERTY (
      bool pathFileValid READ pathFileValid NOTIFY pathFileValidChanged )
  Q_PROPERTY (
      double pathFileSize READ pathFileSize NOTIFY pathFileSizeChanged )

  Q_PROPERTY ( QObject * pathStats READ pathStats NOTIFY pathStatsChanged )
  Q_PROPERTY ( QObject * pathStatsTransformed READ pathStatsTransformed NOTIFY
                   pathTransformedStatsChanged )
  Q_PROPERTY (
      QObject * transformBase READ transformBase NOTIFY transformBaseChanged )

  Q_PROPERTY ( bool pathFitsWorkspace READ pathFitsWorkspace NOTIFY
                   pathFitsWorkspaceChanged )
  Q_PROPERTY ( QObject * edgeDistBottom READ edgeDistBottom NOTIFY
                   edgeDistBottomChanged )
  Q_PROPERTY (
      QObject * edgeDistTop READ edgeDistTop NOTIFY edgeDistTopChanged )
  Q_PROPERTY ( bool controllerAvailable READ controllerAvailable NOTIFY
                   controllerAvailableChanged )

  Q_PROPERTY ( QObject * speedsRequested READ speedsRequested NOTIFY
                   speedsRequestedChanged )
  Q_PROPERTY ( QObject * speedsAccepted READ speedsAccepted NOTIFY
                   speedsAcceptedChanged )

  Q_PROPERTY ( Sequence * sequence READ sequence NOTIFY sequenceChanged )

  // -- Construction

  Panel ( const Panel_Init & init_n );

  ~Panel ();

  // -- Office session interface

  Clerk_Factory_Handle
  office_session_begin_factory () override;

  // -- Office event processing

  void
  office_event ( Office_Event & event_n ) override;

  // -- State interface

  const svs::program::office::State &
  state () const
  {
    return _state;
  }

  void
  state_changed () override;

  Q_SIGNAL
  void
  stateChanged ();

  // -- Property: Startable

  bool
  startable () const
  {
    return _startable;
  }

  Q_SIGNAL
  void
  startableChanged ();

  // -- Property: Routing

  bool
  routing () const
  {
    return _routing;
  }

  Q_SIGNAL
  void
  routingChanged ();

  // -- Property: Path file

  const QUrl &
  pathFile () const
  {
    return _pathFile;
  }

  QString
  pathFilePath () const
  {
    return _pathFile.path ();
  }

  void
  openPathFile ( const QUrl & file_n );

  Q_SIGNAL
  void
  pathFileChanged ();

  // -- Property: Path file selected

  bool
  pathFileSelected () const
  {
    return _pathFileSelected;
  }

  Q_SIGNAL
  void
  pathFileSelectedChanged ();

  // -- Property: Path file opening

  bool
  pathFileOpening () const
  {
    return _pathFileOpening;
  }

  Q_SIGNAL
  void
  pathFileOpeningChanged ();

  // -- Property: Path file opened

  bool
  pathFileOpened () const
  {
    return _pathFileOpened;
  }

  Q_SIGNAL
  void
  pathFileOpenedChanged ();

  // -- Property: Path file valid

  bool
  pathFileValid () const
  {
    return _pathFileValid;
  }

  Q_SIGNAL
  void
  pathFileValidChanged ();

  // -- Property: Path file size

  double
  pathFileSize () const
  {
    return _pathFileSize;
  }

  Q_SIGNAL
  void
  pathFileSizeChanged ();

  // -- Property: Path statistics

  snc::mpath_stream::qt::Statistics *
  pathStats ()
  {
    return &_pathStats;
  }

  Q_SIGNAL
  void
  pathStatsChanged ();

  // -- Property: Path transformed statistics

  snc::mpath_stream::qt::Statistics *
  pathStatsTransformed ()
  {
    return &_pathStatsTransformed;
  }

  Q_SIGNAL
  void
  pathTransformedStatsChanged ();

  // -- Property: Transform base

  snc::qt::VectorModel *
  transformBase ()
  {
    return &_transformBase;
  }

  Q_SIGNAL
  void
  transformBaseChanged ();

  // -- Property: Edge distances bottom

  snc::qt::VectorModel *
  edgeDistBottom ()
  {
    return &_edgeDistBottom;
  }

  Q_SIGNAL
  void
  edgeDistBottomChanged ();

  // -- Property: Edge distances top

  snc::qt::VectorModel *
  edgeDistTop ()
  {
    return &_edgeDistTop;
  }

  Q_SIGNAL
  void
  edgeDistTopChanged ();

  // -- Property: Path fits workspace

  bool
  pathFitsWorkspace () const
  {
    return _pathFitsWorkspace;
  }

  Q_SIGNAL
  void
  pathFitsWorkspaceChanged ();

  // -- Property: controller available

  bool
  controllerAvailable () const
  {
    return _controllerAvailable;
  }

  Q_SIGNAL
  void
  controllerAvailableChanged ();

  // -- Property: Speeds requested

  snc::mpath::qt::SpeedDatabase *
  speedsRequested ()
  {
    return &_speedsRequested;
  }

  Q_SIGNAL
  void
  speedsRequestedChanged ();

  // -- Property: Speeds accepted

  snc::mpath::qt::SpeedDatabase *
  speedsAccepted ()
  {
    return &_speedsAccepted;
  }

  Q_SIGNAL
  void
  speedsAcceptedChanged ();

  // -- Sequence

  Sequence *
  sequence ()
  {
    return &_sequence;
  }

  Q_SIGNAL
  void
  sequenceChanged ();

  // -- Request interface: Transform

  void
  transform ( const svs::program::office::Transform & transform_n );

  void
  transform_axis ( std::size_t axis_index_n,
                   double offset_n,
                   svs::program::office::Transform::Offset offset_type_n );

  Q_INVOKABLE
  void
  setTransformBase ( int axis_index_n, double offset_n );

  Q_INVOKABLE
  void
  setTransformMin ( int axis_index_n, double offset_n );

  Q_INVOKABLE
  void
  setTransformMid ( int axis_index_n, double offset_n );

  Q_INVOKABLE
  void
  setTransformMax ( int axis_index_n, double offset_n );

  // -- Routing

  Q_INVOKABLE
  void
  routeBegin ();

  Q_INVOKABLE
  void
  routeAbort ();

  private:
  // -- Utility

  Q_SLOT
  void
  submitSpeedsRequested ();

  void
  update_sequence_state ( const svs::program::office::MPath_Job_State & state_n,
                          SequenceSection & section_n );

  private:
  // -- Attributes
  svs::program::office::State _state;

  // -- Office event io
  sev::event::Pool< dash::event::out::Path_File > _event_pool_path_file;
  sev::event::Pool< dash::event::out::Transform > _event_pool_transform;
  sev::event::Pool< dash::event::out::Transform_Axis >
      _event_pool_transform_axis;
  sev::event::Pool< dash::event::out::Speeds > _event_pool_speeds;
  sev::event::Pool< dash::event::out::Speed > _event_pool_speed;
  sev::event::Pool< dash::event::out::Route_Begin > _event_pool_route_begin;
  sev::event::Pool< dash::event::out::Route_Abort > _event_pool_route_abort;

  // -- Attributes
  bool _startable = false;
  bool _routing = false;

  QUrl _pathFile;
  bool _pathFileSelected = false;
  bool _pathFileOpening = false;
  bool _pathFileOpened = false;
  bool _pathFileValid = false;
  double _pathFileSize = 0.0;

  snc::mpath_stream::qt::Statistics _pathStats;
  snc::mpath_stream::qt::Statistics _pathStatsTransformed;

  snc::qt::VectorModel _transformBase;
  snc::qt::VectorModel _edgeDistBottom;
  snc::qt::VectorModel _edgeDistTop;
  bool _pathFitsWorkspace = false;
  bool _controllerAvailable = false;

  snc::mpath::qt::SpeedDatabase _speedsRequested;
  snc::mpath::qt::SpeedDatabase _speedsAccepted;

  // -- Sequence
  Sequence _sequence;
  std::shared_ptr< SequenceSection > _seq_approach;
  std::shared_ptr< SequenceSection > _seq_main;
  std::shared_ptr< SequenceSection > _seq_retreat;
};

} // namespace svs::program::dash
