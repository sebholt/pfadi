/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <QDateTime>
#include <QObject>

namespace svs::program::dash
{

class SequenceSection : public QObject
{
  Q_OBJECT

  public:
  // -- Types

  enum Status
  {
    NONE,

    SCHEDULED,
    SKIPPED,

    PREPARING,
    RUNNING,
    FINISHED,

    ABORTING,
    ABORTED,
  };

  // -- Properties

  Q_ENUMS ( Status )
  Q_PROPERTY ( QString name READ name NOTIFY nameChanged )
  Q_PROPERTY ( svs::program::dash::SequenceSection::Status status READ status
                   NOTIFY statusChanged )
  Q_PROPERTY ( QDateTime beginTime READ beginTime NOTIFY beginTimeChanged )
  Q_PROPERTY ( QDateTime endTime READ endTime NOTIFY endTimeChanged )
  Q_PROPERTY ( QTime duration READ duration NOTIFY durationChanged )
  Q_PROPERTY ( bool started READ started NOTIFY startedChanged )
  Q_PROPERTY ( bool finished READ finished NOTIFY finishedChanged )
  Q_PROPERTY ( bool running READ running NOTIFY runningChanged )

  public:
  // -- Construction

  SequenceSection ( QObject * qparent_n = nullptr );

  ~SequenceSection ();

  // -- Name

  const QString &
  name () const
  {
    return _name;
  }

  Q_SIGNAL
  void
  nameChanged ();

  void
  setName ( const QString & name_n );

  // -- Status

  Status
  status () const
  {
    return _status;
  }

  Q_SIGNAL
  void
  statusChanged ();

  void
  setStatus ( Status status_n );

  // -- Begin

  const QDateTime &
  beginTime () const
  {
    return _beginTime;
  }

  Q_SIGNAL
  void
  beginTimeChanged ();

  void
  setBeginTime ( const QDateTime & time_n );

  // -- End

  const QDateTime &
  endTime () const
  {
    return _endTime;
  }

  Q_SIGNAL
  void
  endTimeChanged ();

  void
  setEndTime ( const QDateTime & time_n );

  // -- Duration

  const QTime &
  duration () const
  {
    return _duration;
  }

  Q_SIGNAL
  void
  durationChanged ();

  void
  setDuration ( const QTime & time_n );

  // -- Started

  bool
  started () const
  {
    return _started;
  }

  Q_SIGNAL
  void
  startedChanged ();

  // -- Running

  bool
  running () const
  {
    return _running;
  }

  Q_SIGNAL
  void
  runningChanged ();

  // -- Finished

  bool
  finished () const
  {
    return _finished;
  }

  Q_SIGNAL
  void
  finishedChanged ();

  private:
  // -- Utility

  void
  update_started ();

  void
  update_finished ();

  void
  update_running ();

  private:
  // -- Attributes
  QString _name;
  Status _status = Status::NONE;
  QDateTime _beginTime;
  QDateTime _endTime;
  QTime _duration;
  bool _started = false;
  bool _finished = false;
  bool _running = false;
};

} // namespace svs::program::dash
