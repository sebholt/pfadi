/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <mpath_stream/write/svg.hpp>
#include <snc/mpath_stream/read/buffer.hpp>
#include <snc/svs/frame/async_proc/worker_t.hpp>
#include <svs/mpath_painter/types.hpp>
#include <string_view>

namespace svs::mpath_painter
{

class Worker : public snc::svs::frame::async_proc::Worker_T< Types >
{
  // -- Types
  private:
  using Super = snc::svs::frame::async_proc::Worker_T< Types >;

  public:
  // -- Construction

  Worker ( const sev::logt::Reference & log_parent_n,
           const sev::event::queue_io::Link_2 & link_n,
           std::shared_ptr< const Request_Base > request_n );

  ~Worker ();

  private:
  // -- Interface implementations

  void
  feed_begin () override;

  void
  feed () override;

  void
  feed_end ( Result::End end_n, std::string_view error_n = {} );

  void
  feed_abort () override;

  private:
  snc::mpath_stream::read::Buffer _istream_buffer;
  mpath_stream::write::SVG _ostream_svg;
};

} // namespace svs::mpath_painter
