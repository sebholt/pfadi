/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#include "client.hpp"
#include <snc/svs/frame/async_proc/client_t.ipp>
#include <svs/mpath_painter/types.hpp>

namespace snc::svs::frame::async_proc
{
// -- Instantiation
template class Client_T< ::svs::mpath_painter::Types >;

} // namespace snc::svs::frame::async_proc
