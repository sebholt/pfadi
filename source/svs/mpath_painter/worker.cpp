/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#include "worker.hpp"
#include <sev/assert.hpp>
#include <snc/svs/frame/async_proc/worker_t.ipp>
#include <QDir>
#include <QStandardPaths>

// -- Instantiation
namespace snc::svs::frame::async_proc
{
template class Worker_T< ::svs::mpath_painter::Types >;
} // namespace snc::svs::frame::async_proc

namespace svs::mpath_painter
{

Worker::Worker ( const sev::logt::Reference & log_parent_n,
                 const sev::event::queue_io::Link_2 & link_n,
                 std::shared_ptr< const Request_Base > request_n )
: Super ( { log_parent_n, "Worker" }, link_n, std::move ( request_n ) )
{
}

Worker::~Worker () = default;

void
Worker::feed_begin ()
{
  // Prepare streams
  auto file_name = [] () {
    QDir cache_dir (
        QStandardPaths::writableLocation ( QStandardPaths::CacheLocation ) );
    return cache_dir.filePath ( "machine_path.svg" ).toStdString ();
  };
  _ostream_svg.set_file_name ( file_name () );
  _ostream_svg.set_stats ( request ().mpath_stats );
  _ostream_svg.open ();

  _istream_buffer.set_buffer ( request ().mpath.get () );
}

void
Worker::feed ()
{
  if ( _istream_buffer.open () ) {
    while ( _istream_buffer.is_good () ) {
      _ostream_svg.write ( *_istream_buffer.read () );
    }
    _istream_buffer.close ();
  }

  feed_end ( Result::End::GOOD );
}

void
Worker::feed_end ( Result::End end_n, std::string_view error_n )
{
  // Close streams
  _istream_buffer.set_buffer ( nullptr );
  _ostream_svg.close ();

  // Update result
  result ().end = end_n;
  result ().error_message.assign ( error_n );
  Super::feed_end ();
}

void
Worker::feed_abort ()
{
  log ().cat ( sev::logt::FL_DEBUG_0, "Abort requested." );
  feed_end ( Result::End::ABORTED );
}

} // namespace svs::mpath_painter
