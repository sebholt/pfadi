/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <snc/mpath_stream/buffer.hpp>
#include <snc/mpath_stream/statistics_d.hpp>
#include <snc/svs/frame/async_proc/types.hpp>
#include <memory>
#include <string>

namespace svs::mpath_painter
{

class Request : public snc::svs::frame::async_proc::Request_Base
{
  public:
  std::shared_ptr< const snc::mpath_stream::Buffer > mpath;
  snc::mpath_stream::Statistics_D3 mpath_stats;
};

class Progress : public snc::svs::frame::async_proc::Progress_Base
{
  public:
};

class Result : public snc::svs::frame::async_proc::Result_Base
{
};

struct Types
{
  using Request = svs::mpath_painter::Request;
  using Progress = svs::mpath_painter::Progress;
  using Result = svs::mpath_painter::Result;
};

} // namespace svs::mpath_painter
