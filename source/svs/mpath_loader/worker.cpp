/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#include "worker.hpp"
#include <sev/assert.hpp>
#include <snc/svs/frame/async_proc/worker_t.ipp>
#include <svs/mpath_loader/types.hpp>

// -- Instantiation
namespace snc::svs::frame::async_proc
{
template class Worker_T< ::svs::mpath_loader::Types >;
} // namespace snc::svs::frame::async_proc

namespace svs::mpath_loader
{

Worker::Worker ( const sev::logt::Reference & log_parent_n,
                 const sev::event::queue_io::Link_2 & link_n,
                 std::shared_ptr< const Request_Base > request_n )
: Super ( { log_parent_n, "Worker" }, link_n, std::move ( request_n ) )
, _mpml_streamer ( log () )
, _gcode_streamer ( log () )
{
}

Worker::~Worker () = default;

void
Worker::feed_begin ()
{
  // Allocate
  result ().mpath = std::make_shared< snc::mpath_stream::Buffer > ();
  _buffer.resize ( 4096 );

  // Setup streams
  _ostream_buffer.set_buffer ( result ().mpath.get () );
  _ostream_tee.set_ostream ( 0, &_ostream_stats );
  _ostream_tee.set_ostream ( 1, &_ostream_buffer );
  _ostream_tee.open ();

  // Debug
  if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
    const auto & cref = request ().import_config.cutter ();
    logo << "Reading file " << request ().file_name << "\n";
    logo << "Cutter mapping"
         << "\n  switch_index " << cref.enable_switch_index
         << "\n  switch_inverted " << cref.enable_switch_inverted
         << "\n  warm_up_duration " << cref.warm_up_duration.count ()
         << "\n  cool_down_duration " << cref.cool_down_duration.count ();
  }

  // Select streamer
  _data_streamer = nullptr;
  {
    auto ext = [ &fname = request ().file_name ] ( std::string_view part_n ) {
      return ( fname.rfind ( part_n ) == ( fname.size () - part_n.size () ) );
    };
    if ( ext ( ".mpml" ) || ext ( ".MPML" ) ) {
      log ().cat ( sev::logt::FL_DEBUG_0, "Found MPML file extension." );
      _mpml_streamer.begin ( request ().import_config );
      _data_streamer = &_mpml_streamer;
    } else if ( ext ( ".gcode" ) || ext ( ".GCODE" ) ) {
      log ().cat ( sev::logt::FL_DEBUG_0, "Found GCODE file extension." );
      _gcode_streamer.begin ( request ().import_config );
      _data_streamer = &_gcode_streamer;
    } else {
      feed_finish_auto ( "Unknown file extension." );
    }
  }

  // Open file
  if ( _data_streamer != nullptr ) {
    _ifstream.open ( request ().file_name, std::ios::binary );
    if ( _ifstream.is_open () ) {
      log ().cat ( sev::logt::FL_DEBUG_0, "File opened." );
    } else {
      feed_finish_auto ( "Could not open file." );
    }
  }
}

void
Worker::feed_abort ()
{
  log ().cat ( sev::logt::FL_DEBUG_0, "Incoming abort event." );
  feed_finish ( Result::End::ABORTED );
}

void
Worker::feed_finish ( Result::End end_n, std::string_view error_n )
{
  // Close streams
  _ifstream.close ();
  if ( _data_streamer != nullptr ) {
    _data_streamer->end ();
  }
  _ostream_tee.close ();
  _ostream_buffer.set_buffer ( nullptr );

  // Update result
  result ().end = end_n;
  result ().error_message.assign ( error_n );
  if ( !error_n.empty () ) {
    log ().cat ( sev::logt::FL_DEBUG_0, "Error: ", error_n );
  }

  Super::feed_end ();
}

void
Worker::feed_finish_auto ( std::string_view error_n )
{
  result ().mpath_stats = _ostream_stats.stats ();
  result ().file_bytes = progress ().file_bytes;
  feed_finish ( error_n.empty () ? Result::End::GOOD : Result::End::ERROR,
                error_n );
}

void
Worker::feed ()
{
  if ( _ifstream.fail () ) {
    // No more reading from data stream
    if ( _ifstream.bad () ) {
      // Data stream has gone bad
      feed_finish_auto ( "Data stream failed." );
    } else {
      // Regular end of stream reached
      log ().cat ( sev::logt::FL_DEBUG_0, "End of data stream reached." );
      // Feed end on streamer
      _data_streamer->feed_end ( &_ostream_tee );
      // Check streamer state afterwards
      feed_finish_auto ( _data_streamer->error_message () );
    }
  } else {
    // Read some bytes from data stream
    _ifstream.read ( _buffer.data (), _buffer.size () );
    const auto byte_count = _ifstream.gcount ();
    if ( byte_count > 0 ) {
      // Update byte count
      progress ().file_bytes += byte_count;
      // Feed bytes to streamer
      _data_streamer->feed ( _buffer.data (), byte_count, &_ostream_tee );
      // Check streamer state afterwards
      if ( !_data_streamer->good () ) {
        feed_finish_auto ( _data_streamer->error_message () );
      }
    }
  }
}

} // namespace svs::mpath_loader
