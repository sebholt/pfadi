/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#include "client.hpp"
#include <snc/svs/frame/async_proc/clerk_t.hpp>
#include <snc/svs/frame/async_proc/client_t.ipp>

namespace snc::svs::frame::async_proc
{
// -- Instantiation
template class Client_T< ::svs::mpath_loader::Types >;

} // namespace snc::svs::frame::async_proc
