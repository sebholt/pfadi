/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <import/config.hpp>
#include <snc/mpath_stream/buffer.hpp>
#include <snc/mpath_stream/statistics_d.hpp>
#include <snc/svs/frame/async_proc/types.hpp>
#include <cstdint>
#include <memory>
#include <string>

namespace svs::mpath_loader
{

class Request : public snc::svs::frame::async_proc::Request_Base
{
  public:
  std::string file_name;
  import::Config import_config;
};

class Progress : public snc::svs::frame::async_proc::Progress_Base
{
  public:
  std::uint_fast64_t file_bytes = 0;
};

class Result : public snc::svs::frame::async_proc::Result_Base
{
  public:
  std::uint_fast64_t file_bytes = 0;
  snc::mpath_stream::Statistics_D3 mpath_stats;
  std::shared_ptr< snc::mpath_stream::Buffer > mpath;
};

struct Types
{
  using Request = svs::mpath_loader::Request;
  using Progress = svs::mpath_loader::Progress;
  using Result = svs::mpath_loader::Result;
};

} // namespace svs::mpath_loader
