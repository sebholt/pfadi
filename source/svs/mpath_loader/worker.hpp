/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <import/gcode/streamer.hpp>
#include <import/mpml/streamer.hpp>
#include <snc/mpath_stream/write.hpp>
#include <snc/mpath_stream/write/buffer.hpp>
#include <snc/mpath_stream/write/statistics_d.hpp>
#include <snc/mpath_stream/write/tee.hpp>
#include <snc/svs/frame/async_proc/worker_t.hpp>
#include <svs/mpath_loader/types.hpp>
#include <QByteArray>
#include <fstream>
#include <string_view>

namespace svs::mpath_loader
{

class Worker : public snc::svs::frame::async_proc::Worker_T< Types >
{
  // -- Types
  private:
  using Super = snc::svs::frame::async_proc::Worker_T< Types >;

  public:
  // -- Construction

  Worker ( const sev::logt::Reference & log_parent_n,
           const sev::event::queue_io::Link_2 & link_n,
           std::shared_ptr< const Request_Base > request_n );

  ~Worker ();

  private:
  // -- Interface implementations

  void
  feed_begin () override;

  void
  feed_abort () override;

  void
  feed_finish ( Result::End end_n, std::string_view error_n = {} );

  void
  feed_finish_auto ( std::string_view error_n = {} );

  void
  feed () override;

  private:
  // -- File reading
  std::ifstream _ifstream;
  std::vector< char > _buffer;

  import::mpml::Streamer _mpml_streamer;
  import::gcode::Streamer _gcode_streamer;
  import::Data_Streamer * _data_streamer = nullptr;

  snc::mpath_stream::write::Buffer _ostream_buffer;
  snc::mpath_stream::write::Statistics_D3 _ostream_stats;
  snc::mpath_stream::write::Tee2 _ostream_tee;
};

} // namespace svs::mpath_loader
