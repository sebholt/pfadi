/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <snc/svs/frame/async_proc/client_t.hpp>
#include <svs/mpath_loader/types.hpp>

namespace svs::mpath_loader
{

using Client = snc::svs::frame::async_proc::Client_T< Types >;

} // namespace svs::mpath_loader
