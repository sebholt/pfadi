
#ifndef __inc_application_config_hpp__
#define __inc_application_config_hpp__

#define PACKAGE_NAME "${PACKAGE_NAME}"
#define PROGRAM_NAME "${PROGRAM_NAME}"
#define PROGRAM_TITLE "${PROGRAM_TITLE}"
#define DAEMON_NAME "${DAEMON_NAME}"
#define VERSION "${VERSION}"
#define L10N_PREFIX "${L10N_PREFIX}"
#define LOG_MASK_DEFAULT ${LOG_MASK_DEFAULT}

#define SINGLE_APPLICATION_KEY "${PROGRAM_NAME}-${VERSION}"

#endif
