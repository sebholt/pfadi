
all: debug release

debug:
	cd ~/build/rmill-control/debug && make -j8

release:
	cd ~/build/rmill-control/release && make -j8

