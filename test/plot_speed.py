#!/usr/bin/python

import re
import os
import sys
import numpy as np
import matplotlib.pyplot as plt

if ( len ( sys.argv ) < 2 ):
	exit ( -1 )

nmstr = "([\d+-e.]+)"
vmstr = "\s*" + nmstr + "\s*" + nmstr + "\s*" + nmstr
re_pos = re.compile ( "pos: " + vmstr )
re_tangent = re.compile ( "tangent: " + vmstr )
re_speed = re.compile ( "speed: " + vmstr )
poss = ([],[],[])
tangents = ([],[],[])
speeds = ([],[],[])

filename = sys.argv[1]
print ( "Reading file " + filename )
fl = open ( filename, 'r' )
for line in fl:
	match_pos = re_pos.search ( line );
	match_tangent = re_tangent.search ( line );
	match_speed = re_speed.search ( line );
	if ( match_pos and match_tangent and match_speed ):
		for ii in range ( 0, 3 ):
			poss[ii].append ( float ( match_pos.group ( ii+1 ) ) )
			tangents[ii].append ( float ( match_tangent.group ( ii+1 ) ) )
			speeds[ii].append ( float ( match_speed.group ( ii+1 ) ) )

print ( "Creating arrays" )
arr_poss = (
	np.array ( poss[0] ),
	np.array ( poss[1] ),
	np.array ( poss[2] ) )
arr_tangent = (
	np.array ( tangents[0] ),
	np.array ( tangents[1] ),
	np.array ( tangents[2] ) )
arr_speeds = (
	np.array ( speeds[0] ),
	np.array ( speeds[1] ),
	np.array ( speeds[2] ) )
poss = ([],[],[])
tangents = ([],[],[])
speeds = ([],[],[])

xvals = np.linspace( 0.0, len ( arr_poss[0] ), len ( arr_poss[0] ), endpoint=True )

fig = plt.figure()
num_plots = 3
ax_pos = fig.add_subplot ( num_plots, 1, 1 )
ax_tangent = fig.add_subplot ( num_plots, 1, 2 )
ax_speed = fig.add_subplot ( num_plots, 1, 3 )

ax_pos.set_ylabel ( 'position' )
ax_pos.plot ( xvals, arr_poss[0], color="red", linewidth=1.0, linestyle="-" )
ax_pos.plot ( xvals, arr_poss[1], color="green", linewidth=1.0, linestyle="-" )
ax_pos.plot ( xvals, arr_poss[2], color="blue", linewidth=1.0, linestyle="-" )

ax_tangent.set_ylabel ( 'tangent' )
ax_tangent.plot ( xvals, arr_tangent[0], color="red", linewidth=1.0, linestyle="-" )
ax_tangent.plot ( xvals, arr_tangent[1], color="green", linewidth=1.0, linestyle="-" )
ax_tangent.plot ( xvals, arr_tangent[2], color="blue", linewidth=1.0, linestyle="-" )

ax_speed.set_ylabel ( 'speed' )
ax_speed.plot ( xvals, arr_speeds[0], color="red", linewidth=1.0, linestyle="-" )
ax_speed.plot ( xvals, arr_speeds[1], color="green", linewidth=1.0, linestyle="-" )
ax_speed.plot ( xvals, arr_speeds[2], color="blue", linewidth=1.0, linestyle="-" )

plt.show()
